#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 15:28:02 2019

@author: bornat
"""


# this is only to access the module in the ../ dir
import sys
sys.path.append("../")


import TWIST_control as TC

TWIST = TC.TWIST_instance(debug=True, no_ver_warn=True)

#TWIST.set_chan_param(0, freq=1, PWMbits=0, combined=False)
TWIST.set_dbgLED(0x55)
for i in range(min(8, TWIST.TWIST_SINES)):
    TWIST.set_sine_freq(i,1000*(i+1))
    TWIST.set_sine_ampl(i, 0.99)
    TWIST.set_sine_options(i, enable=True)
    TWIST.connect_sine_to_channel(i, i)
TWIST.set_dbgLED(0x00)
#TWIST.close()
print("Job's done")
