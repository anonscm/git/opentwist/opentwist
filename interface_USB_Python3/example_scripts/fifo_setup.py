#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 12:06:52 2019

@author: bornat
"""

# this is only to access the modue in the ../ dir
import sys
sys.path.append("../")


import TWIST_control as TC

TWIST = TC.TWIST_instance(debug=True)

TWIST.setup_FIFO(0, 0, 100000, 0, enable=False, disable_when_empty=True)
free = TWIST.send_fifo_data(0, [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07]*10)
TWIST.set_pattern_trigger(0, 0)

TWIST.close()
