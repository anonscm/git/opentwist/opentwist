----------------------------------------------------------------------------------

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package Twist_parameters is
    
    constant TWIST_PARAMS_PATTERNS              : natural := 8;                 -- number of fixed pattern sources on the system
    constant TWIST_PARAMS_SINES                 : natural := 32;                -- number of sine generator sources on the system
    constant TWIST_PARAMS_FIFOS                 : natural := 8;                 -- number of FIFO data sources on the system

    constant TWIST_PARAMS_PATTERN_MAXLENGTH        : natural := 2047;                 -- maximal number of samples for fixed patterns
    constant TWIST_PARAMS_SINE_BITS                : natural := 24;                   -- number of bits for sine computation
    constant TWIST_PARAMS_FIFOGEN_DEPTH            : natural := 2048;                 -- number of samples stored in fifo generators



end Twist_parameters;

package body Twist_parameters is
 
end Twist_parameters;
