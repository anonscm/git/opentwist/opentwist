----------------------------------------------------------------------------------
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity FIFO_Generic is
    Generic (DATA_SIZE : integer;
             FIFO_DEPTH : integer := 1024);
    Port ( clk      : in  std_logic;
           reset    : in  std_logic;
           
           data_in  : in  std_logic_vector(DATA_SIZE - 1 downto 0);
           dwrite   : in  std_logic;
           full_n   : out std_logic;

           elemts   : out integer range 0 to FIFO_DEPTH;
           data_out : out std_logic_vector(DATA_SIZE - 1 downto 0);
           dread    : in  std_logic;
           empty_n  : out std_logic);
end FIFO_Generic;

architecture Behavioral of FIFO_Generic is

    signal read_index  : integer range 0 to FIFO_DEPTH-1;
    signal write_index : integer range 0 to FIFO_DEPTH-1;
    signal nb_elemts   : integer range 0 to FIFO_DEPTH;
    signal fulln_i     : std_logic;
    signal emptyn_i    : std_logic;

    type FIFO_array_t is array (0 to FIFO_DEPTH-1) of std_logic_vector(DATA_SIZE - 1 downto 0);
    signal memory : FIFO_array_t;
    signal prefetched_data : std_logic_vector(DATA_SIZE - 1 downto 0);
    signal prefetched_full : std_logic;
    
begin

    data_out <= prefetched_data;
    full_n   <= fulln_i;
    empty_n  <= prefetched_full;
    
    fulln_i  <= '0' when (nb_elemts >= FIFO_DEPTH) and (prefetched_full = '1') else '1';
    emptyn_i <= '0' when nb_elemts=0 else '1';

    elemts   <= nb_elemts;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                write_index <= 0;
            elsif dwrite = '1' and fulln_i = '1' then
                memory(write_index) <= data_in;
                if write_index = FIFO_DEPTH-1 then
                    write_index <= 0;
                else
                    write_index <= write_index + 1;
                end if;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                read_index <= 0;
                prefetched_data <= (others => '0');
            elsif (dread = '1' or prefetched_full = '0') and emptyn_i = '1'  then
                prefetched_data <= memory(read_index);
                if read_index = FIFO_DEPTH-1 then
                    read_index <= 0;
                else
                    read_index <= read_index + 1;
                end if;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                prefetched_full <= '0';
            elsif emptyn_i = '1'  then
                prefetched_full <= '1';
            elsif dread = '1' and emptyn_i = '0' then
                prefetched_full <= '0';
            end if;
        end if;
    end process;
    


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                nb_elemts <= 0;
            elsif     ((dread = '1' or prefetched_full = '0') and emptyn_i = '1') and not (dwrite = '1' and fulln_i = '1') then
                nb_elemts <= nb_elemts - 1;
            elsif not ((dread = '1' or prefetched_full = '0') and emptyn_i = '1') and     (dwrite = '1' and fulln_i = '1') then
                nb_elemts <= nb_elemts + 1;
            end if;
        end if;
    end process;


end Behavioral;
