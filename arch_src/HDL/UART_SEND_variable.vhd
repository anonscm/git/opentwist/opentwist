----------------------------------------------------------------------------------
-- NEVER SIMULATED, might provide some surprises
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.NUMERIC_STD.ALL;


entity UART_SEND_variable is
    Generic (CLK_FREQU    : integer := 100000000;
             MAX_BAUDRATE : integer := 0;   -- useless, kept for compatibility
             TIME_PREC    : integer := 100;
             DATA_SIZE    : integer := 8);
Port ( clk      : in  STD_LOGIC;
       reset    : in  STD_LOGIC;
       baudrate : in  INTEGER range 0 to CLK_FREQU;
       TX       : out STD_LOGIC;
       din      : in  STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
       den      : in  STD_LOGIC;
       bsy      : out STD_LOGIC);
end UART_SEND_variable;

architecture Behavioral of UART_SEND_variable is


    constant clkfrequ_int : integer := 2*CLK_FREQU/TIME_PREC;
    
    signal div_cnt_pos : integer range 0 to (clkfrequ_int*3); -- the counter for positive edge triggered output
    signal div_cnt_neg : integer range 0 to (clkfrequ_int*3); -- the counter for negative edge triggered output
    signal bitcounter  : integer range 0 to DATA_SIZE + 2;
    signal shift_reg   : std_logic_vector(DATA_SIZE downto 0);
    
    signal local_bsy_n : std_logic;

    signal loc_bdrate  : integer range 0 to CLK_FREQU/(3*TIME_PREC);

    signal pre_TX_pos     : std_logic;
    signal pre_TX_neg     : std_logic;
    signal pre_TX_neg_dly : std_logic;

begin


    process(clk)
    begin
        if rising_edge(clk) then    
            if reset = '1' then
                bitcounter  <= 0;
                div_cnt_pos <= 0;
                div_cnt_neg <= 0;
                shift_reg   <= (DATA_SIZE downto 0 => '1');
                local_bsy_n <= '1';
                pre_TX_pos  <= '0';
                pre_TX_neg  <= '1';
            elsif bitcounter=0 then
                if den = '1' and local_bsy_n = '1' then
                    bitcounter  <= DATA_SIZE + 2;
                    div_cnt_pos <= baudrate*2 + baudrate/2;
                    div_cnt_neg <= baudrate*3 + baudrate/2;
                    shift_reg   <= din & '0';
                    local_bsy_n <= '0';
                    loc_bdrate  <= baudrate;
                    pre_TX_pos  <= pre_TX_neg;
                else
                    local_bsy_n <= '1';
                    pre_TX_pos  <= '0';
                    pre_TX_neg  <= '1';
                end if;
            elsif div_cnt_pos >= clkfrequ_int then
                div_cnt_neg <= div_cnt_neg - clkfrequ_int + loc_bdrate*2;
                div_cnt_pos <= div_cnt_pos - clkfrequ_int + loc_bdrate*2;
                bitcounter  <= bitcounter - 1;
                shift_reg   <= '1' & shift_reg(DATA_SIZE downto 1);
                if bitcounter = 1 then
                    local_bsy_n <= '1';
                end if;
                pre_TX_pos  <= pre_TX_neg xor shift_reg(1);
            elsif div_cnt_neg >= clkfrequ_int then
                div_cnt_neg <= div_cnt_neg - clkfrequ_int + loc_bdrate*2;
                div_cnt_pos <= div_cnt_pos - clkfrequ_int + loc_bdrate*2;
                bitcounter  <= bitcounter - 1;
                shift_reg   <= '1' & shift_reg(DATA_SIZE downto 1);
                --if bitcounter = 1 then
                --    local_bsy_n <= '1';
                --end if;
                pre_TX_neg  <= pre_TX_pos xor shift_reg(1);
            else
                div_cnt_pos <= div_cnt_pos + loc_bdrate*2;
                div_cnt_neg <= div_cnt_neg + loc_bdrate*2;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if falling_edge(clk) then
            pre_TX_neg_dly <= pre_TX_neg;
        end if;
    end process;

    TX  <= (pre_TX_pos xor pre_TX_neg_dly) or local_bsy_n;
    bsy <= not local_bsy_n; 


end Behavioral;
