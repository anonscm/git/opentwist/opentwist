----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_waveform_ctrl is
    port(
            clk              : in  std_logic;
            rst              : in  std_logic;
            params           : in  Twist_Parameters_t;
            status           : out FIFOs_status_t;
            channels         : out Channel_Values_t);
           
end twist_waveform_ctrl;

architecture Behavioral of twist_waveform_ctrl is

    type Source_enable_array is array(0 to TWIST_STIM_CHANNELS) of std_logic_vector(0 to TWIST_TOTAL_SOURCES - 1 );

    signal generator_outputs    : Source_array_t(0 to TWIST_TOTAL_SOURCES-1);
    signal presaturated_outputs : Source_array_t(0 to TWIST_STIM_CHANNELS-1);
    signal enable_bits          : Source_enable_array;

    signal combtree_input       : Source_array_t(0 to TWIST_TOTAL_SOURCES-1);
    signal cycle_counter : integer range 0 to TWIST_COMBTREE_RATE - 1;

begin

    pattern_generators : for pattern in 0 to TWIST_PATTERNS-1 generate
        single_pattern : entity work.twist_pattern_gen
            port map(clk     => clk,
                     reset   => rst,
                     params  => params.patterns(pattern),
                     channel => generator_outputs(pattern+TWIST_PATTNUM_OFFSET)(15 downto 0));

        generator_outputs(pattern+TWIST_PATTNUM_OFFSET)(19 downto 16) <= (others => generator_outputs(pattern)(15)); -- we extend sign bits
        
        pattern_enable_mapping : for chan in 0 to TWIST_STIM_CHANNELS-1 generate
            enable_bits(chan)(pattern+TWIST_PATTNUM_OFFSET) <= params.patterns(pattern).chan_mask(chan);
        end generate;
    end generate;

    sine_generators : for sine in 0 to TWIST_SINES-1 generate
        single_sine : entity work.twist_sine_gen
            port map(clk     => clk,
                     reset   => rst,
                     params  => params.sines(sine),
                     channel => generator_outputs(sine+TWIST_SINENUM_OFFSET)(15 downto 0));

        generator_outputs(sine+TWIST_SINENUM_OFFSET)(19 downto 16) <= (others => generator_outputs(sine+TWIST_SINENUM_OFFSET)(15)); -- we extend sign bits
        
        pattern_enable_mapping : for chan in 0 to TWIST_STIM_CHANNELS-1 generate
            enable_bits(chan)(sine+TWIST_SINENUM_OFFSET) <= params.sines(sine).chan_mask(chan);
        end generate;
    end generate;


    fifo_generators : for fifo in 0 to TWIST_FIFOS-1 generate
        single_fifo : entity work.twist_fifo_gen
            port map(clk     => clk,
                     reset   => rst,
                     params  => params.fifos(fifo),
                     status  => status(fifo),
                     channel => generator_outputs(fifo+TWIST_FIFONUM_OFFSET)(15 downto 0));

        generator_outputs(fifo+TWIST_FIFONUM_OFFSET)(19 downto 16) <= (others => generator_outputs(fifo+TWIST_FIFONUM_OFFSET)(15)); -- we extend sign bits
        
        pattern_enable_mapping : for chan in 0 to TWIST_STIM_CHANNELS-1 generate
            enable_bits(chan)(fifo+TWIST_FIFONUM_OFFSET) <= params.fifos(fifo).chan_mask(chan);
        end generate;
    end generate;


        process(clk)
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    cycle_counter <= TWIST_COMBTREE_RATE - 1;
                elsif cycle_counter = 0 then
                    cycle_counter <= TWIST_COMBTREE_RATE - 1;
                else
                    cycle_counter <= cycle_counter - 1;
                end if;
            end if;
        end process;

    data_passing : if TWIST_COMBTREE_RATE = 1 generate
        combtree_input <= generator_outputs;
    end generate;    
    
    data_passing_bar : if TWIST_COMBTREE_RATE > 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if cycle_counter = TWIST_COMBTREE_RATE - 1 then
                    -- tree input is only updated at each new cycles. It takes resources, but ensures
                    -- experiment repeatability and results will not rely on source number or architecture variants
                    combtree_input <= generator_outputs;
                end if;
            end if;
        end process;
    end generate;

    combination_trees : for chan in 0 to TWIST_STIM_CHANNELS-1 generate
        trunc_instance : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => TWIST_TOTAL_SOURCES,
                         PIPELINE_DEPTH => TWIST_COMBTREE_DEPTH,
                         UPDATE_RATE    => TWIST_COMBTREE_RATE)
            port map(clk     => clk,
                     reset   => rst,
                     sources => combtree_input,
                     enables => enable_bits(chan),
                     channel => presaturated_outputs(chan));
           
        -- This process manages the signal saturation 
        process(clk)
        begin
            if rising_edge(clk) then
                if presaturated_outputs(chan)(19 downto 15) = "00000" or presaturated_outputs(chan)(19 downto 15) = "11111" then
                    channels(chan) <= presaturated_outputs(chan)(15 downto 0);
                elsif presaturated_outputs(chan)(19) = '1' then
                    channels(chan) <= x"8001";
                else
                    channels(chan) <= x"7FFF";
                end if;
            end if;
        end process;        
    
    end generate;




end Behavioral;
