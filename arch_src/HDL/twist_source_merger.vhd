----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


use work.Twist_arch_types.all;

entity twist_source_merger is
    Generic (SOURCE_NUMBER  : integer;
             PIPELINE_DEPTH : integer;                -- number of clock cycles to perform the whole tree
             UPDATE_RATE    : integer := 1);          -- number of clock cycles between updates
    port(
        clk       : in  std_logic;
        reset     : in  std_logic := '1';             -- if reset not connected, module will only work when UPDATE_RATE=1
        sources   : in  Source_array_t(0 to SOURCE_NUMBER-1);
        enables   : in  std_logic_vector(0 to SOURCE_NUMBER-1) := (0 to SOURCE_NUMBER-1 => '1');
        channel   : out source_output_type);
end twist_source_merger;

architecture Behavioral of twist_source_merger is

    constant half_num_src : integer := SOURCE_NUMBER/2;

    constant merge_stages     : integer := logn(SOURCE_NUMBER, UPDATE_RATE +1);
    constant front_submodules : integer := (SOURCE_NUMBER + UPDATE_RATE)/((UPDATE_RATE +1)**(merge_stages - 1)); 



    signal left_side  : source_output_type;
    signal right_side : source_output_type;

    signal cycle_counter : integer range 0 to UPDATE_RATE - 1;
    signal accumulator   : source_output_type;
    signal front_outs    : source_array_t(0 to front_submodules - 1);

begin

----------------------------------------------------------------------------------------------------
-- generic condition (UPDATE_RATE > 1) : less powerful, but less resources and electrical consumption
----------------------------------------------------------------------------------------------------
    Cycle_counter_management : if UPDATE_RATE > 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if reset = '1' then
                    cycle_counter <= UPDATE_RATE - 1;
                elsif cycle_counter = 0 then
                    cycle_counter <= UPDATE_RATE - 1;
                else
                    cycle_counter <= cycle_counter - 1;
                end if;
            end if;
        end process;
        left_side  <= (others => '0');
        right_side <= (others => '0');
    end generate;

    Leaf_multiple_cycles : if UPDATE_RATE > 1 and PIPELINE_DEPTH = UPDATE_RATE and SOURCE_NUMBER<=UPDATE_RATE+1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if reset = '1' then
                    accumulator <= (others => '0');
                    channel     <= (others => '0');
                elsif SOURCE_NUMBER < 3 then
                    accumulator <= (others => '0');
                    if cycle_counter = UPDATE_RATE - 1 then
                        if SOURCE_NUMBER = 1 then
                            if enables(0) = '1' then
                                channel <= sources(0);
                            else
                                channel <= (others => '0');
                            end if;
                        else -- SOURCE_NUMBER = 2
                            case enables(0 to 1) is
                                when  "00"  => channel <= (others => '0');
                                when  "10"  => channel <= sources(0);
                                when  "01"  => channel <= sources(1);
                                when others => channel <= sources(1) + sources(0);
                            end case;                        
                        end if;
                    end if;
                elsif cycle_counter = 0 then
                    case enables(0 to 1) is
                        when  "00"  => accumulator <= (others => '0');
                        when  "10"  => accumulator <= sources(0);
                        when  "01"  => accumulator <= sources(1);
                        when others => accumulator <= sources(1) + sources(0);
                    end case;
                elsif cycle_counter < UPDATE_RATE - 1 then
                    if cycle_counter < SOURCE_NUMBER - 1 then
                        if enables(cycle_counter) = '1' then
                            accumulator <= accumulator + sources(cycle_counter);
                        end if;
                    end if;
                else -- cycle_counter = UPDATE_RATE - 1  and SOURCE_NUMBER > 2
                    if enables(SOURCE_NUMBER - 1) = '1' then
                        channel <= accumulator + sources(SOURCE_NUMBER - 1);
                    else
                        channel <= accumulator;
                    end if;
                end if;
--                accumulator <= (others => '0');
--                channel     <= sources(0);
            end if;
        end process;
        
        front_outs <= (others => (others => '0'));
        
    end generate;
    
    tree_multiple_cycles : if UPDATE_RATE > 1 and SOURCE_NUMBER>UPDATE_RATE+1 and PIPELINE_DEPTH = UPDATE_RATE*merge_stages generate
    
        front_instances : for i in 0 to front_submodules - 1 generate
            front_instance : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => min(SOURCE_NUMBER, (i+1)*(UPDATE_RATE+1)**(merge_stages-1)) - i*(UPDATE_RATE+1)**(merge_stages-1),
                         PIPELINE_DEPTH => PIPELINE_DEPTH - UPDATE_RATE,
                         UPDATE_RATE    => UPDATE_RATE)
            port map(clk     => clk,
                     reset   => reset,
                     sources => sources(i*(UPDATE_RATE+1)**(merge_stages-1) to min(SOURCE_NUMBER, (i+1)*(UPDATE_RATE+1)**(merge_stages-1))-1),
                     enables => enables(i*(UPDATE_RATE+1)**(merge_stages-1) to min(SOURCE_NUMBER, (i+1)*(UPDATE_RATE+1)**(merge_stages-1))-1),
                     channel => front_outs(i));
--            front_outs(i) <= sources(i*(UPDATE_RATE+1)**(front_submodules-1));
        end generate;
        
        final_instance : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => front_submodules,
                         PIPELINE_DEPTH => UPDATE_RATE,
                         UPDATE_RATE    => UPDATE_RATE)
            port map(clk     => clk,
                     reset   => reset,
                     --we leave enables at default value '1'
                     sources => front_outs,
                     channel => channel);
        accumulator   <= (others => '0');
    end generate;

    tree_temporize : if UPDATE_RATE > 1 and PIPELINE_DEPTH > UPDATE_RATE*merge_stages generate

        actual_tree : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => SOURCE_NUMBER,
                         PIPELINE_DEPTH => UPDATE_RATE*merge_stages,
                         UPDATE_RATE    => UPDATE_RATE)
            port map(clk     => clk,
                     reset   => reset,
                     enables => enables,
                     sources => sources,
                     channel => accumulator);

        front_outs <= (0 => accumulator, others => (others => '0'));

        time_compensation_tree : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => 1,
                         PIPELINE_DEPTH => PIPELINE_DEPTH - UPDATE_RATE*merge_stages,
                         UPDATE_RATE    => min(UPDATE_RATE, PIPELINE_DEPTH - UPDATE_RATE*merge_stages))
            port map(clk     => clk,
                     reset   => reset,
                     ----we leave enables at default value '1'
                     sources => front_outs(0 downto 0),
                     channel => channel);
--        accumulator   <= (others => '0');
--        channel <= sources(0);
        
    end generate;

----------------------------------------------------------------------------------------------------
-- specific conditions when UPDATE_RATE=1 (samples are constantly updated)
----------------------------------------------------------------------------------------------------
    Useless_signals_affectation : if UPDATE_RATE = 1 generate
        cycle_counter <= 0;
        accumulator   <= (others => '0');
        front_outs <= (others => (others => '0'));
    end generate;

    Leaf_condition_1_source : if PIPELINE_DEPTH=1 and SOURCE_NUMBER=1 and UPDATE_RATE = 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if enables = "1" then
                    channel <= sources(0);
                else
                    channel <= (others => '0');
                end if;
            end if;
        end process;
    end generate;

    Leaf_condition_2_sources : if PIPELINE_DEPTH=1 and SOURCE_NUMBER>1 and UPDATE_RATE = 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                case enables is
                    when "00"   => channel <= (others => '0');
                    when "10"   => channel <= sources(2-SOURCE_NUMBER);   -- should be 0 because SOURCE_NUMBER should be 2, otherwise, generates an error
                    when "01"   => channel <= sources(1);
                    when others => channel <= sources(1) + sources(0);
                end case;
            end if;
        end process;
    end generate;

    
    Trunc_condition : if SOURCE_NUMBER <= 2**(PIPELINE_DEPTH-1) and PIPELINE_DEPTH>1  and UPDATE_RATE = 1 generate
        -- In this case, we have too much clock cycles
        -- we just add a register on the output...
        trunc_instance : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => SOURCE_NUMBER,
                         PIPELINE_DEPTH => PIPELINE_DEPTH - 1)
            port map(clk => clk,
                     sources => sources,
                     enables => enables,
                     channel => left_side);
            
        process(clk)
        begin
            if rising_edge(clk) then
                channel <= left_side;
            end if;
        end process;
end generate;



    Tree_condition : if SOURCE_NUMBER > 2**(PIPELINE_DEPTH-1) and PIPELINE_DEPTH>1  and UPDATE_RATE = 1 generate

        left_instance : entity work.twist_source_merger
        generic map (SOURCE_NUMBER  => half_num_src,
                     PIPELINE_DEPTH => PIPELINE_DEPTH - 1)
        port map(clk => clk,
                 sources => sources(0 to half_num_src-1),
                 enables => enables(0 to half_num_src-1),
                 channel => left_side);

        right_instance : entity work.twist_source_merger
            generic map (SOURCE_NUMBER  => SOURCE_NUMBER-half_num_src,
                         PIPELINE_DEPTH => PIPELINE_DEPTH - 1)
            port map(clk => clk,
                     sources => sources(half_num_src to SOURCE_NUMBER-1),
                     enables => enables(half_num_src to SOURCE_NUMBER-1),
                     channel => right_side);

        process(clk)
        begin
            if rising_edge(clk) then
                channel <= left_side + right_side;
            end if;
        end process;

    end generate; -- Tree_condition


end Behavioral;
