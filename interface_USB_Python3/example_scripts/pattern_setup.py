#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 12:06:52 2019

@author: bornat
"""

# this is only to access the modue in the ../ dir
import sys
sys.path.append("../")


import TWIST_control as TC

TWIST = TC.TWIST_instance(debug=True)

TWIST.set_pattern_freq(0, 1000000)
TWIST.set_pattern_trigger(0, 0)
TWIST.connect_pattern_to_channel(0, 0)
TWIST.send_pattern_data(0, list(range(1024)))

TWIST.close()
