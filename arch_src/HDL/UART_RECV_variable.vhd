----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UART_RECV_variable is
    Generic (CLK_FREQU    : integer := 100000000;
             MAX_BAUDRATE : integer :=  33333333;
             TIME_PREC    : integer :=       100;
             DATA_SIZE    : integer := 8);
    Port ( clk      : in  STD_LOGIC;
           reset    : in  STD_LOGIC;
           baudrate : in  INTEGER range 0 to CLK_FREQU/TIME_PREC;
           RX       : in  STD_LOGIC;
           dout     : out STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
           den      : out STD_LOGIC);
end UART_RECV_variable;

architecture Behavioral of UART_RECV_variable is

    constant clkfrequ_int : integer := CLK_FREQU/TIME_PREC;

    signal RX_sampled_pos : std_logic;
    signal RX_presmpl_neg : std_logic;
    signal RX_sampled_neg : std_logic;
    
    signal div_cnt_pos : integer range -clkfrequ_int/2 to 3*clkfrequ_int;
    signal div_cnt_neg : integer range -clkfrequ_int/2 to 3*clkfrequ_int;
    signal bitcounter  : integer range 0 to DATA_SIZE + 1;
    signal shift_reg   : std_logic_vector(DATA_SIZE - 1 downto 0);

    signal cnt_pos_ov  : boolean;
    signal cnt_neg_ov  : boolean;
    
    signal brcnt_start_val : integer range 0 to 3*clkfrequ_int;

begin
    
    process(clk)
    begin
        if falling_edge(clk) then
            RX_presmpl_neg <= RX;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            brcnt_start_val <= 2*baudrate + 3*baudrate/4;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then    
            RX_sampled_pos <= RX;
            RX_sampled_neg <= RX_presmpl_neg;
            if reset = '1' then
                bitcounter    <= 0;
                div_cnt_pos   <= 0;
                div_cnt_neg   <= 0;
            elsif bitcounter=0 then
                if RX_sampled_neg = '0' then
                    bitcounter  <= DATA_SIZE + 1;
                    --div_cnt_neg <= 2*baudrate + 3*baudrate/4 - clkfrequ_int/2;
                    div_cnt_neg <= 2*baudrate + 3*baudrate/4 - clkfrequ_int/2;
                    div_cnt_pos <= 3*baudrate +   baudrate/4 - clkfrequ_int/2;
                elsif RX_sampled_pos = '0' then
                    bitcounter  <= DATA_SIZE + 1;
                    --div_cnt_pos <= 2*baudrate + 3*baudrate/4 - clkfrequ_int/2;
                    div_cnt_pos <= 2*baudrate + 3*baudrate/4 - clkfrequ_int/2; 
                    div_cnt_neg <= 2*baudrate +   baudrate/4 - clkfrequ_int/2;
                end if;
            elsif cnt_pos_ov or cnt_neg_ov then
                div_cnt_pos <= div_cnt_pos - clkfrequ_int + baudrate;
                div_cnt_neg <= div_cnt_neg - clkfrequ_int + baudrate;
                bitcounter  <= bitcounter - 1;
            else
                div_cnt_pos <= div_cnt_pos + baudrate;
                div_cnt_neg <= div_cnt_neg + baudrate;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                shift_reg  <= (DATA_SIZE-1 downto 0 => '0');
            elsif cnt_neg_ov then
                shift_reg  <= RX_sampled_neg & shift_reg(DATA_SIZE - 1 downto 1);
            elsif cnt_pos_ov then
                shift_reg  <= RX_sampled_pos & shift_reg(DATA_SIZE - 1 downto 1);
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                dout  <= (DATA_SIZE-1 downto 0 => '0');
                den   <= '0';
            elsif cnt_neg_ov and bitcounter = 1 then
                dout  <= shift_reg;
                den   <= RX_sampled_neg;
            elsif cnt_pos_ov and bitcounter = 1 then
                dout  <= shift_reg;
                den   <= RX_sampled_pos;
            else
                den   <= '0';
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                cnt_pos_ov <= False;
                cnt_neg_ov <= False;
            else
                cnt_pos_ov <= ((div_cnt_pos >= clkfrequ_int) and not (cnt_pos_ov or cnt_neg_ov)) or div_cnt_pos >= 2*clkfrequ_int;
                cnt_neg_ov <= (div_cnt_neg >= clkfrequ_int) and not (cnt_neg_ov or cnt_pos_ov);
            end if;
        end if;
    end process;


end Behavioral;
