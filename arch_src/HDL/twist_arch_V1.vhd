--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_arch_V1 is
port(
           clk               : in std_logic;                        -- the system clock
           clk_hwif          : in std_logic;                        -- a faster clock for hardware interface
           rst               : in std_logic;
           
           rx				     : in std_logic;
		   tx				     : out std_logic;
		   
		   TX_MISO             : out std_logic;
		   RX_HS0_MOSI         : in  std_logic;
		   HS1_SCK             : in  std_logic;
		   HS2_CSn             : in  std_logic;
		   
		   flash_CSn           : out std_logic;
		   flash_clk           : out std_logic;
		   flash_MOSI          : out std_logic;
		   flash_MISO          : in  std_logic;
            
           stim_amplitude_0    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_1    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_2    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_3    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_4    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_5    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_6    : out  STD_LOGIC_VECTOR(7 downto 0);
           stim_amplitude_7    : out  STD_LOGIC_VECTOR(7 downto 0);
           
           stim_dac_CS	     : out  STD_LOGIC_VECTOR(TWIST_STIM_CHANNELS-1 downto 0);
--		   stim_dac_rw		  : out std_logic;
           cathodic_order    : out std_logic_vector(TWIST_STIM_CHANNELS-1 downto 0);
           anodic_order      : out std_logic_vector(TWIST_STIM_CHANNELS-1 downto 0);
           trigg_sync        : inout std_logic_vector(TWIST_EXT_TRIGGS-1 downto 0);
           
           pc_started        : out std_logic_vector(7 downto 0);
           dbgparam          : out Twist_Parameters_t
      );
end twist_arch_V1;

architecture Behavioral of twist_arch_V1 is

    signal rst_sync      : std_logic;
    signal reset         : std_logic;
    signal reset_cnt     : integer range 0 to TWIST_RESETCNT_MAXVAL := 0;

    signal command       : std_logic_vector(15 downto 0);
    signal command_en    : std_logic;
    signal feedback      : std_logic_vector(15 downto 0);
    signal feedback_en   : std_logic;
    signal continued     : std_logic;
    signal decoder_busy  : std_logic;

    signal parameters    : Twist_Parameters_t;
    signal status        : Twist_Status_t;
    signal channels_out  : Channel_Values_t;
    signal channels_ctrl : Total_HW_Channs_t;

begin



Interface : entity work.twist_IF
    port map(
        clk    => clk,
        clk_if => clk_hwif,
        rst    => rst_sync,
        
        rx => rx,
        tx => tx,

        TX_MISO      => TX_MISO,
        RX_HS0_MOSI  => RX_HS0_MOSI,
        HS1_SCK      => HS1_SCK,
        HS2_CSn      => HS2_CSn,
        
        cmd_out => command,
        cmd_en  => command_en,
        exp_d   => continued,
        dbsy    => decoder_busy,
        fbck_in => feedback,
        fbck_en => feedback_en,
        status  => status.hw_interface,
        params  => parameters.interface);



Decoding : entity work.twist_decoder
    port map(
        clk          => clk,
        reset        => reset,
        cmd_in       => command,
        cmd_en       => command_en,
        exp_data     => continued,
        busy         => decoder_busy, 
        fbck_out     => feedback,
        fbck_en      => feedback_en,
        ext_triggers => trigg_sync,
        chans        => channels_out,
        status       => status,
        params       => parameters);



Generators : entity work.twist_waveform_ctrl
    port map(
        clk         => clk,
        rst         => reset,
        params      => parameters,
        status      => status.fifos,
        channels    => channels_out);


Control : entity work.twist_output_manager
    port map(
        clk         => clk,
        rst         => reset,
        params      => parameters.chans,
        usec_cnt    => parameters.sub_us_cnt,
        channels    => channels_out,
        output_ctrl => channels_ctrl);


Flash_SPI_mgr : entity work.master_spi_mgr
    port map(
        clk         => clk,
        rst         => reset,
        params      => parameters.flashdat,
        fbck        => status.flashdat,
        spi_CSn     => flash_CSn,
        spi_SCK     => flash_clk,
        spi_MOSI    => flash_MOSI,
        spi_MISO    => flash_MISO);


    local_reset : process(clk)
    begin
        if rising_edge(clk) then
            rst_sync <= rst;
            if rst_sync = '1' or parameters.reset = '1' then
                reset     <= '1';
                reset_cnt <= 0;
            elsif reset_cnt < TWIST_RESETCNT_MAXVAL then
                reset_cnt <= reset_cnt + 1;
            else
                reset <= '0';
            end if;
        end if;
    end process;

 process(channels_ctrl)
 begin
     for i in 0 to TWIST_STIM_CHANNELS-1 loop
        stim_dac_CS(i)    <= channels_ctrl(i).DAC_CS;
--        stim_dac_rw(i)  <= channels_ctrl(i).DAC_CS;
        cathodic_order(i) <= channels_ctrl(i).Cathod_en;
        anodic_order(i)   <= channels_ctrl(i).Anod_en;
     end loop;
 end process;

 stim_amplitude_0 <= channels_ctrl(0).Amplitude;
 stim_amplitude_1 <= channels_ctrl(1).Amplitude;
 stim_amplitude_2 <= channels_ctrl(2).Amplitude;
 stim_amplitude_3 <= channels_ctrl(3).Amplitude;
 stim_amplitude_4 <= channels_ctrl(4).Amplitude;
 stim_amplitude_5 <= channels_ctrl(5).Amplitude;
 stim_amplitude_6 <= channels_ctrl(6).Amplitude;
 stim_amplitude_7 <= channels_ctrl(7).Amplitude;

 pc_started <= parameters.debug_LED;
 dbgparam   <= parameters;

end Behavioral;

