# User guide for the TWIST frontend

This document is a description of the TWIST front-end from the user point of view.
It might not be up to date wit the last updates. Feedback is highly appreciated,
and may concern several topics:

* lack of information / clarity in this documentation
* system bug / misbehaving with this documentation
* feature request

Feel free to send feedback to Y. Bornat (yannick.bornat@ims-bordeaux.fr)

## Big WARNING to start...

Documentation writing and system development require lots of time, which is quite
pricey. this documentation is generally written AFTER system updates, meaning the
board might not always be 100% compliant to the description here. If you see some
behavior mismatch, please report it (or even better, update it). It will result in
documentation update or bug report according to the situation.


## Outline

1. [Introduction](#introduction)
2. [Quick start tutorial](#quick-start-tutorial)
3. [System description](#system-description)
4. [Python API description](#python-api-description)
5. [Digital Interfaces](#digital-interfaces)
6. [Hardware Command Set](#hardware-command-set)
7. [Known bugs](#known-bugs)
8. [Planned features / improvement ideas](#known-bugs)

\pagebreak

# Introduction

The front-end provides signal generators (sources). These generators can eventually
rely on triggers to be activated (all sources can be configured to rely on any trigger,
but only one of them). Their behavior can be time limited or not. Any source can
contribute to any analog stimulation output (channel). If several generators are
connected to the same output channel, their outputs are added to provide the final
waveform.   

### Board description

![twist board schematic](figs/twist_board.png)

 * (A) the ZIF connector board that hosts the _SONIC_ chip 
 * (B) The _NESO_ FPGA board that embeds system management
 1. Power supply (5V - 6A)
 2. Stimulation current amplitude trimmer
 3. KEEP DISCONNECTED (Power supply controller programmer)
 4. FPGA JTAG connector
 5. Unused/Free connector (8 I/Os)
 6. External trigger interface (Triggers 0 to 3)
 7. RT digital control
 8. KEEP DISCONNECTED (_NESO_ specific power supply)
 9. PC/USB control connector
 10. User pushbuttons
 11. User LEDs
 12. Stimulation current SMA connectors
 13. Embedded resistor enable/disable
 14. Embedded serial capacitor enable/disable
 15. Stimulation current board to board connector (chans 4 to 7)
 16. Stimulation current board to board connector (chans 0 to 3)
 17. Power supply test points

### analog current outputs

![analog connector schematic](figs/stim_chan_connection.png)

Stimulation channels are available in two types of connection: SMA (12) or HE10
(15 and 16). HE10 connectors are intended to be connected to an eventual post-processing
board. each connector handles 4 stimulation channels (connector 16 handles chans 0 to 3
and connector 15 handles chans 4 to 7). Board serigraph refers to stimulation channels
as stim1 to stim8, this nomination is not compatible with the digital architecture.

### computer USB control

The system can be controlled by a computer using a USB connection (9). This connection
creates a new virtual serial port to ease fast development from any language or
environment. An interface is provided in Python language (see
[tutorial](#quick-start-tutorial) and
[specific section](#python-api-description)). All commands are available through this
interface.

Because of its nature, the USB interface is not compatible with real-time experiments
or high performance control (e.g. online updates of signals whit high sampling rates).
This communication port uses a fixed baudrate of 921600 bps.

For development time reasons, there is no separation between instructions from the
USB interface and from the hardware interface. Although the system is able to
respond to commands from these two interfaces alternately, it is not possible to
discriminate them when used at the same time. Therefor, user should not expect the
USB interface to work properly when a hardware system is actually sending commands
at the same time.

### real-time digital control

A full hardware interface is available to control the system with the best performance.
It uses connector (7) and is capable of data transfers up to 100Mbps. Through this
interface, the system can output a maximal rate of 5Msamples/s. Its electrical standard
is 3.3V. More details are available in the [Digital interface](#digital-interface) section

![analog connector schematic](figs/RTcomm_connector.png)

Several modes are available: 

- 8 bit UART (default): Uses _TX_MISO_ and _RX_HS0_MOSI_. This mode is useful
to control _TWIST_ with a microcontroller. It corresponds to a 3.3V UART inerface.
Parameters are 8bits/ 1 stop bit/ no parity / no flow control. Default speed is
115200 bps, but can be set different
(see [Baudrate considerations](#baudrate-considerations)).
- 16 bit UART: Uses _TX_MISO_ and _RX_HS0_MOSI_, may also use _HS1_SCK_ and/or
_HS2_CSn_. This mode is derived from UART behavior and is intended improve performance
with capable controllers. It corresponds to a 3.3V UART interface using 16 bit data words.
This change imporves the transfer efficacy from 80% to 88%. Other parameters
are the same than 8 bit UART mode. Additionnaly, _HS1_SCK_ and _HS2_CSn_ can be
used as second and third TX signals to triple the bandwidth from controller to
_TWIST_. When using these extra signals, a delay of 40ns must be set between
starts on different lines, so that the _TWIST_ system is able to reconstruct
instruction word chronology.

Two other modes are planned:

- 128 bit UART (not available yet): This planned mode gathers data in longer
packets. Available bit rates remain identical, but transfer efficacy increases
from 88% (16bit mode) to 98% at the price of a much higher latency. Data flowing
down from _TWIST_ to controller on _TX_MISO_ remains on 16 bits. Please, manifest
your interest for this mode to get it implemented.
- SPI (not available yet): this mode can improve performance if using a
microcontroller as a master. Please, manifest
your interest for this mode to get it implemented.

##### Baudrate considerations

The _TWIST_ system accepts any baudrate from 1600 bps to 33 332 800 bps by steps
of 1600. The UART speeds have not been extensively tested in real world. Tests have been
successfully performed with a 100MHz based system, therefore, tested frequencies
are 33.333Mbps, 25Mbps, 22.222Mbps, 20Mbps and 16.666Mbps. Frequencies below 15Mbps
are not likely to cause any trouble.


### external trigger communication

The _TWIST_ system can send or receive triggers on connector (6). This is particularly
useful to synchronize stimulator with other aparatus or with experiment-specific
phenomenon. It is also useful to drive an oscilloscope synchronization channel.
Electrical standard is CMOS 3.3V.

![analog connector schematic](figs/trigger_connector.png)


\pagebreak

# Quick start tutorial

### Software requirements and first test

To run properly, the software suite uses Python3. It is advised to install the
Anaconda3 distribution to get a standardized package distribution. After installation,
install the PySerial package.

* on windows: in the anaconda3 software group, launch _anaconda prompt_ then run
`pip install pyserial`
* on linux/OSX: open a terminal and run `pip3 install pyserial`

To use the _TWIST_control.py_ file, the `__init__.py` file is required in the same
directory to allow custom python file importation. It is now time to test your
setup, connect the _TWIST_ board and run the following code
```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance()
    twist_board.close()
```
If the code does not generate any error, then everything is fine. You can get
information on the communication and on the embedded architecture if you activate
the _debug_ feature:
```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance(debug=True)
    twist_board.close()
```
This will display the architecture version, number of sources and other characteristics
of the board.

Once powered, the _TWIST_ board displays an LED chase. Running this code will stop
the chase and clear the LEDs. To test your correct control of the board, you can
change the LED display value. The _set_dbgLED_ method updates the value displayed
on the LEDs from a binary integer. To only light the leftmost LED send value 0x01,
to light each other LED, use values 0x55 or 0xAA. The resulting code is then:

```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance()
    twist_board.set_dbgLED(0x55)
    twist_board.close()
```

**WARNING:** LED ordering is not usual (least significant bit on the left), but is
thought to match channel disposition on the board.

At this step, your software is now able to take control of the board. Congratulations


### Visualize channel signals

To observe the stimulator output current, we will consider that a resistor is
connected between the channel 0 and the ground. An image of the current can be
observed with an oscilloscope probe connected to the channel output. It is possible
to retrieve the current from the leftmost SMA connector, or from the lower leftmost
pin of connector *16*.

##### Output a sine

To output a single sine wave, use the following code:
```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance()
    twist_board.setup_sine(0, 0, 10000, 1.0, enable=True)
    twist_board.close()
```
this code

* uses sine generator source 0 (first argument)
* connects the source to channel output 0 (second argument)
* sets source frequency to 10kHz
* sets amplitude to 1mA

It is possible to make the sine source only output a given number of periods and
have it activated as a response to a trigger. See full documentation of the _setup_sine_
method for more details.

##### Output a pattern

To output a specific patterned waveform, use the following code:
```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance()
    twist_board.setup_pattern(0, 0, 0, 100000, [1.0, -0.5, -0.5, 0])
    twist_board.set_trigg(0)
    twist_board.close()
```
this code

* uses pattern generator source 0 (first argument)
* connects the source to channel output 0 (second argument)
* set the pattern as a response to trigger 0 (third argument)
* sets sampling pattern frequency to 100kHz
* uses pattern waveform: 1mA for 10µs, -500µA for 20 µs, no current afterwards

The _set_trigg_ method activates trigger 0, and therefore pattern 0.
It is also possible to activate this trigger using the
[dedicated pin](#board-description) on the board. In such a configuration, _TWIST_
provides a stimulation as soon as the _trigg0_ input is activated. The computer
may be disconnected without consequences.

This pin _trigg0_ is set as input by default to avoid any damage to external devices. Its
electrical standard is CMOS 3.3V. to activate the trigger, make a temporary shortcut
with the power supply on the same connector.

##### Get the system indefinitely repeat a stimulation

If the previous
experiment is only based on software triggering, adding the line
`twist_board.set_trigg_dir(0, TC.EXT_TRIGG_OUTPUT)` after pattern setup will make
trigger available as output on pin _trigg0_.

It is also possible to set the trigger 0 to activate itself after any other trigger.
Let's say 1s after itself, so it will activate each second. This is done with
the following command: `twist_board.chain_trigg(0, 1000000, 0)`. First argument is
the trigger activated as a consequence of trigger given as third argument. 1000000
is the 1s delay expressed in microseconds. The code finally becomes:
```python
    import TWIST_control as TC
    twist_board = TC.TWIST_instance()
    twist_board.setup_pattern(0, 0, 0, 100000, [1.0, -0.5, -0.5, 0])
    twist_board.set_trigg_dir(0, TC.EXT_TRIGG_OUTPUT)
    twist_board.chain_trigg(0, 1000000, 0)
    twist_board.set_trigg(0)
    twist_board.close()
```




\pagebreak

# System detailed description

As previously stated, the system is a gathering of generators (or sources) that
obey a trigger to provide their output and that contribute to one or several
analog output channels.

Each source obeys only one trigger (but anyone), several sources can obey the
same trigger. Each source can contribute to one or several channel. Several
sources can contribute to a single channel, in such a case, currents are internally
added. A specific 20 bit adder is implemented to gather sources, so it is possible
to use up to 32 sources without overflow errors. A hard-bound protection is
implemented to perform a saturation value if the final output overflows or
underflows the output capacity.

The python interface provided with the system considers all current values in
milliampers. This setting can be overridden changing the value of property
_current_unit_. 

- `twist_board.current_unit = 0.001` will change API behavior
to communicate in ampers
- `twist_board.current_unit = 1000` will change API behavior
to communicate in microampers
- `twist_board.current_unit = 1` will come back to nominal behavior

### Pattern player

* default number of patterns: 8
* default pattern size: 16383 samples

This generator is similar to standard stimulators. A prerecorded pattern is played
as a response to a trigger.


The pattern player stores any sampled waveform and starts playing it back when
its corresponding trigger is active. The internal memory can store up to 16384
samples by default (value can be changed at synthesis level) but the last sample
is used to determine a baseline value (the output value when no pattern is
actually played).

Once triggered, the pattern will be played until the end and any new trigger
will be ignored during playback. Although updating the frequency and output
channels is possible during playback, it is advised not to update pattern
parameters while using it.

The *setup_pattern* method from the *TWIST_control* API is actually a macro that
uses several atomic functions. It requires 

1. The pattern generator to use
2. The channel on which the pattern will be output (or a list of channels)
3. The trigger number to which the pattern will obey
4. The sampling frequency of the samples
5. A sample list to use for the pattern. This could actually be any data container
   that has a *len* method and that can access data in an indexed manner.
6. (optional) The baseline value. If not specified, this value will be set to 0.

The hardware sequence to setup the pattern is equivalent to what the python code
actually does:

- To update the pattern, it is first necessary to send its size with the
  _set_pttrn_lngth_ command. The internal sample pointer is then reset and the
  _feed_source_ command will store samples one by one in the internal memory. If
  more samples than necessary are sent, no loop storing will be performed and the
  system will drop the extra samples. A _data_loss_ error will then be reported.
- The playback frequency is set with the _set_pttrn_frq_ command.
- To observe the output, it is necessary to assign a trigger to listen to, and a
  channel to output to. this can be done with the commands _set_pttrn_trig_  and
  set _set_chan_src_.



### Sine generator

* default number of sine generators: 32
* available sine frequency for each generator: from 1mHz to 248kHz by steps of 1mHz
* frequency accuracy is yet to determine (SIC !)

This generator produces a sine waveform at an arbitrary frequency and amplitude.
If configured to use a trigger, it only generates a given number of periods,
otherwise, the sine waveform is indefinitely output.
It is possible to update the sine frequency while the generator is active.
Depending on users choice, the update is performed as soon as the command is
received, or at the end of the current period. If the amplitude is updated, the
generator will only consider this update at the end of the current period (this
is both a technical choice and a security to avoid discontinuities)

Output accuracy is frequency dependent. Value is updated at a rate of 25MHz for
frequencies above 4kHz, lower frequencies are obtained slowing down the update
process and have no precision improvement. 

The *setup_sine* method from the *TWIST_control* API is actually a macro that
uses several atomic functions. It requires 

1. The sine generator to use
2. The channel on which the sine will be output (or a list of channels)
3. The frequency of the signal to generate
4. The amplitude of the signal
5. (optional) A number of periods to play. If specified, the generator will stop
   by itself after this amount of periods will be generated, otherwise, it will
   indefinitely provide its waveform.
6. (optional) The trigger to obey if the generation does not start immediately.
   if omitted, the generator will not obey any trigger.
7. (optional) Whether the generator should be started when configured. Default
   is *False*.
8. (optional) Whether the frequency should be updated at the command reception
   or at the end of the current period.

All optional parameters default to a value that inhibits the generator behavior,
so at least one of them must be set to observe something on the output channel.
The general use case should fall into one of these two choices:

- continuous generation: enable = True
- limited generation: number of periods AND trigger should be specified.

For the hardware sequence, please, refer to the python code of *setup_sine* that
reflects the procedure.

### FIFO player

* default number of FIFO generators: 8
* default FIFO depth: 16384 samples (most likely to change in future updates)
* FIFO output frequency: from 1Hz to 33MHz by steps of 1Hz

This generator simply outputs the data it receives at a given frequency. It can
be activated by a trigger to make inter-channel synchronization possible, or by
its configuration command, depending on user's preference. The user application
should always pay attention to keep data available in the FIFO. If not, a buffer
underrun will occur. In such a case, the missing samples will be replaced by
zeros to avoid untraceable charge balance. depending on configuration, the
playback can be stopped after underrun or restarted as soon as new samples
are received.


The *setup_FIFO* method from the *TWIST_control* API is actually a macro that
uses several atomic functions. It requires 

1. The FIFO generator number to use
2. The channel on which the samples will be output (or a list of channels)
3. The sampling frequency (which also relies on the user ability to update data
   in real-time)
4. (optional) A trigger to obey (for synchronization)
5. (optional) Whether the generator should be activated at configuration time
   or not
6. (optional) Whether the generator should interpolate between samples (**This
   feature is not implemented yet**)
7. (optional) Whether the generator should stop operating when a buffer underrun
   occurs (default is True).

For this generator to output a result, it should be enabled or configured with
a trigger.

As the FIFO generator requires constant data update, data must be provided
separately using the _send_fifo_data_ method. It only requires a FIFO generator
number to identify the source to update, and a list of samples to send. The method
returns the number of data slots available afterwards.

As usual, the python code of the function explains the low-level commands to perform.

### Pattern modulation processor (projection)

**This feature is not implemented yet**, feel free to make feature requests to help
building a system that fits your needs.

The point of this module is to provide patterns that can be modulated in realtime,
 e.g.
 
* square biphasic stimulation: one phase modulated in amplitude, second phase
modulaed in time
* Pulsed modulated stimulation: changing the number of identical pulses for a
given modulation
* continuous amplitude modulated stimuulation: coninuous still means we alternate
positive and negative phases
* exponential or quadratic waveforms, or any shape you like
* any combination of the above...

It will rely on a specific instruction set DSP.


### Trigger management

The _TWIST_ system embeds 8 triggers to control and synchronize the different
generators of the system. These triggers can be set individually with the
_set_trigg_ method (that requires the number of the trigger to activate), or
collectively with the _bitmap_trigg_ method. In the
second case, an integer representing a mask of the triggers to activate is 
necessary (0x01 codes trigger 0, 0x02 codes trigger 1, 0x04 codes trigger 2...)

It is also possible to activate the triggers 0 to 3 from an external device
through connector (6) (see [board description](#board-description). This
interface uses CMOS 3.3V signaling, any logical value '1' presented for more
than 20 ns will result in activating the corresponding trigger.

The _TWIST_ system makes it possible to modify this behavior and use these
connections as output. If configured so, the system will output a 10 µs pulse
each time the corresponding trigger is activated. The direction choice can be
configured at the pin level with the _set_trigg_dir_ method that takes two
arguments: the number of the trigger pin to affect and its new direction
value. The _TWIST_control_ module provides predefined values:

- EXT_TRIGG_INPUT
- EXT_TRIGG_OUTPUT

The hardware command that controls this direction affects all triggers at once.
If several trigger directions are to be set, it is possible to only notify
the system with the last method call. The first calls should then use the third
optional argument to delay the command (sync=False). The last method call MUST
leave this value to True so that the order is actually transmitted to the board.

##### Sequencing triggers

The system is able to provide a trigger sequence. In such a case, each trigger
activates a specific time after another trigger. The _chain_trigg_ configures
a trigger (given as first argument) to activate a number of microseconds (given
as second argument) after any other trigger (given as third argument).

There are several common use cases:

- periodically generate a stimulation (the trigger activates a given time after
  itself)
- react to some external event with a specific delay
- mix different sine signals with specific phase relationship.


### Output channels

To get the system work as close as possible to everyone's need, it is possible
to some channel specific parameters using the _set_chan_param_ method.

##### Output frequency

The DACs embedded on the board allow a maximal update rate of 20MHz for output
channels, but the bandwidth of the analog chain following is much lower. However,
a high sampling rate helps reducing unwanted spectrum rays. Frequency has been left
configurable to help user determine their own balance between system properties.

Frequency can be set to any integer value expressed in MHz. As the _TWIST_ system
clock is 100MHz, only sub-multiples of 100MHz are strictly periodic (e.g. 1Mhz,
2MHz, 4MHz, 5MHz, 10MHz, 20MHz). Any other value will be _globally respected_
with a jitter lower than 10 ns. (e.g. for a frequency of 7MHz, time between
samples will be 140ns, 140ns, 140ns, 150ns, 140ns, 140ns, 150ns and the sequence
repeats).

##### PWM dynamic extension

When the system is connected to impedance higher than 20kOhms, its dynamic is
reduces because of its voltage limitations. Therefore, it is not possible to use
its full-scale dynamic. One solution is to reduce the maximal output current
with the [dedicated trimmer](#board-description) (6). Another pure digital
solution is to alternate between the nearest values of the current actually
asked for.

To perform this, the DAC command is permanently updated so that the mean value
of the analog output equals the desired value. The principle is equivalent to
digital Pulse Width Modulation (PWM). The low-pass property of the
analog circuitry performs the required filtering. This is particularly interesting
since higher dynamics are required for high impedance values and in such a case
circuit bandwidth is much lower than the sampling frequency.

Output dynamic can be extended to the equivalent of 7 extra bits. If combined
with an output frequency of 20MHz, the added noise only occurs on frequencies
above 150kHz. 
To reduce noise contribution, the sub-sampling counter's bits have been reversed.
For example, if the required value is 42.375 and the dynamic is extended to the
equivalent of 3 bits, the actual output sequence will
be 42, 43, 42, 43, 42, 42, 42, 43 and so on.

It is possible to disable this feature setting and equivalent dynamic increase
of 0 bit.

##### Inversion

It is possible to invert a specific channel to perform a differential stimulation.
User can connect a source to any couple of outputs and set one of them
as inverted. There is then not need to connect the setup to the ground.

##### Channel combination

To compensate the charging effect that occur when a current has been null for some
time (FIXME: add a ref), it is possible to combine two channels in a single one.
This technique divides the number of available electrodes but provides a much
higher output quality. Channel coupling follows a predetermined scheme. It is
only possible to combine even channels with the odd channel immediately
following (e.g. correct combinations are of the form 2n/2n+1).

In such a configuration, all parameters of the odd channel are discarded and
follow parameters from the even channel (same frequency, same dynamic expansion,
same polarity, same sources). Once the even channel is set as "not combined",
the odd channel recovers its previous parameters.


### Output dynamic trimming

Trimmer referenced as (2) makes it possible to tune the maximal output current.
It is not possible to increase maximal output current, but user can define a lower
value to obtain a better dynamic if 1mA stimulation output is not required.

To keep consistency with software tools, the _max_out_milliamps_ property can
be updated accordingly.


\pagebreak

# Python API description

This is a description of a few features that have not been described yet.

### debugging features

##### LED control

This one is easy... refer to the very beginning of the tutorial.  It is possible
to control the LEDs on the board. interesting for debugging.

##### Waveform readback

To ensure that the board has been configured correctly, it is possible to retrieve
the data produced by the system. The _chan_readback_ method takes 4 arguments:

1. The channel to listen to
2. The trigger to activate (in order to synchronize signal production and
   acquisition)
3. The frequency at which the channel should be sampled
4. The number of samples to retrieve.

Data retrieval is performed just before the output channel control, therefore,
it is only possible to check signal generation. Channel inversion, combination
or frequency change are not considered.


### Updating the board architecture

The Python class provides a method to access the embedded SPI Flash memory that
contains the FPGA configuration (and potentially other parameters). The
_flash_update_ method takes the name of a file that contains the image of the
new flash memory content as an argument. By default, the method only performs a
simulation of the upgrade. To actually update the flash memory, set the second
argument as False (simulate=False)

The _check_flash_content_ method makes it possible to verify that the flash
memory has been correctly written. As for the update method, it requires the
filename of the flash image.

Both methods are silent (the provide not output or progress), but they rely
on a similar method which is actually an iterator. _flash_update_iter_ actually
outputs the process advance (from 0 to 1) and _check_flash_content_iter_ output
a tuple: first a boolean indicating that the last portion of data retreived
matches the file provided, then a float between 0 and 1.0 indicating the progress
of the readback.

Although these functions are accessible, it is more advised to use the
_firmware_update.py_ script provided in the example scripts.

\pagebreak

# Digital Interface

#### _Legacy_ UART

The system provides a USB encapsulated UART interface usable with any computer.
This UART uses the following parameters: 

* 921600 bauds
* 8 bits/word
* No Parity
* 1 stop bit
* No flow control

#### Board-to-board interface

A 4-wire interface makes it possible to communicate more conveniently with
microcontrollers, or other FPGA based boards. It uses 4 wires:

* TX_MISO (TWIST output, user input)
* RX_HS0_MOSI (TWIST input, user output)
* HS1_SCK (TWIST input, user output)
* HS2_CSn (TWIST input, user output)

##### UART compatible (default at powerup)

This mode is a compatibility mode. It only uses TX_MISO and RX_HS0_MOSI as a standard
UART interface. In any conditions, this mode is activated if RX_HS0_MOSI is cleared
for 1 ms while HS2_CSn is set (Interface reset).

Communication parameters:
* default 115200 bauds, can be changed with the *set_UART_speed* command
* 8 bits/word
* No Parity
* 1 stop bit
* No flow control

The *set_UART_speed* command updates the frequency to values from 1kbps to 33Mbps
by steps of 1kbps.

If the board connected to the port is only capable of sending 16-bit words, it is
possible to send 8-bit data on the lower byte and to fill the upper byte with 1's.
This corresponds to a transfer mode with 9 stop bits instead of 1. The system will
detect this behavior and provides 8-bit responses with 9 stop bits, so the data
received will not be corrupted. The system will switch back to 1 stop bit as soon
as two 8-bit words will be received within less than 130 microseconds to improve
communication performance. This behavior switch can only be reversed back performing
an interface reset (RX_HS0_MOSI cleared for 1 ms while HS2_CSn is set).

##### SPI mode

This mode is a SPI compatible mode that can go as fast as 20MHz. In any conditions,
this mode is activated if HS2_CSn is cleared for 1 ms. Therefore, any system willing
to communicate through the SPI mode needs to first clear HS2_CSn for 1 ms before
starting the clock transfer.

Transfers are 16 bit transfers, any word below this number of bits will be ignored.
Responses may not immediately follow the command word. Response will always be
different from 0, if no response is available yet, the system will output null
words. Commands can be pipelined, therefore, it is possible to send several commands,
even if no response has been received yet. In this mode, the _NOP_ command (no
operation) does not provide a response, it is then possible to send several _NOP_
commands while waiting for a response.

If using other SPI devices on the same bus, be aware that if RX_HS0_MOSI remains
clearer for 1 ms, the UART compatible mode will be activated, disabling the SPI
mode.

##### Improved UART

This mode follows the UART behavior but with non standard parameters to improve
performance. On RX_HS0_MOSI, words are 16 bits long with one stop bit, no parity
and no control flow. The HS1_SCK and HS2_CSn inputs are available for specific
128-bits words containing 8 16-bits data words. This is particularly useful to
optimize speed when transferring high amounts of data, for example using several
FIFO generators at a high speed each.

If needed, additional HS line could be implemented to improve bandwidth.


\pagebreak

# Hardware command set

The following table shows the system commands and their binary code, refer to the
[detailed command description](#detailed-command-description) for more details.

![table of commands](figs/commands.png)

Commands provide a response to identify whether it has actually been processed.
Response is composed of one or several 16-bit words. The first response word is
generic, remaining words are command specific.

#### Generic response:

| bit | 15 | 14 -> 8 | 7  | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|-----|----|---------|----|---------------------------|  
|     |  1 | n_rem   | 0  | 0 |  0 | 0 | err_dloss | err_dat | err_unkn | ack |

* __ack__: this flag indicates that the command has been properly processed
* __err_unkn__: marks an unknown command
* __err_dat__: marks invalid data in command (typically above expected range)
* __err_dloss__: warns that data has been lost (typically: FIFO overrun or too
much samples in a pattern)
* __n_rem__: number of 16-bit words waiting to be sent to complete the command
response (if more than 127 data words are waiting, 127 will be output here)

## Detailed command descriptions

##### NOP



##### Read_ID



##### Reset_stim

##### Read_info


##### Set_UART_speed

##### Dbg_readback

##### Set_dbg_led

##### Set_pttrn_trig

##### Set_FIFO_freq

##### Set_FIFO_opt

##### Get_FIFO_elts

##### Set_pttrn_lngth

##### Set_chan_src

##### Set_pttrn_frq

##### Set_sine_freq

##### Set_sine_ampl

##### Set_sine_periods

##### Set_sine_opts

##### Set_HS_mode

##### Set_size_mult

##### Trig_chan

##### Feed_source

\pagebreak

# Known bugs

* some sources cannot be connected to channels (depending on source types and
number on the board)
  * there is actually a technical workaround which is implemented but which requires
   much more resources than necessary.
* when switching combined channels to off, channel n+1 must have frequency > 0, otherwise, output current
is no reset to 0.
* sine configuration becomes irresponsive if amplitude set to 0 and end_of_period is set to True.
* sine frequencies lack precision
* changing parameters of an odd channel that have been combined does not generate an error
* no error reporting when hardware UART is configured too high
* updating pattern data may block execution (need a reset, or setup pattern in a single method call)
* SPI interface is not functional yet


# Planned features / improvement ideas

List order is not related with feature priority. Please, contribute, express
preferences or anything you think of...

* start sine with phase shift
* CRC data checking
* error reporting
* pulse generation/modulation
* Pattern modulation processor
* Embedded lowpass filters (see sine generators)
  * generator related ?
  * channel related ?
* Optional linear transitions between samples (FIFO and/or pattern mode)
* Auto charge balance ?
