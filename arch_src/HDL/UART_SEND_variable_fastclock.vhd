----------------------------------------------------------------------------------
-- NEVER SIMULATED, might provide some surprises
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.NUMERIC_STD.ALL;


entity UART_SEND_variable_fastclock is
    Generic (FASTCLK_FREQ : integer := 100000000;
             MAX_BAUDRATE : integer := 33333333;
             TIME_PREC    : integer := 100;
             DATA_SIZE    : integer := 8);
Port ( sysclk   : in  STD_LOGIC;        -- system clock, used for ports
       fastclk  : in  STD_LOGIC;        -- basetime clock, used to cadence output
       reset    : in  STD_LOGIC;
       baudrate : in  INTEGER range 0 to FASTCLK_FREQ/(3*TIME_PREC);
       TX       : out STD_LOGIC;
       din      : in  STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
       den      : in  STD_LOGIC;
       bsy      : out STD_LOGIC);
end UART_SEND_variable_fastclock;

architecture Behavioral of UART_SEND_variable_fastclock is


    constant clkfrequ_int : integer := FASTCLK_FREQ/TIME_PREC;
    
    signal div_cnt    : integer range 0 to (clkfrequ_int + MAX_BAUDRATE);
    signal bitcounter : integer range 0 to DATA_SIZE + 2;
    signal shift_reg  : std_logic_vector(DATA_SIZE downto 0);


    signal loc_baudrate : integer range 0 to FASTCLK_FREQ/(3*TIME_PREC);
    signal lf_din       : std_logic_vector (DATA_SIZE - 1 downto 0);
    signal dly_den      : std_logic;
    signal dly_den_2    : std_logic;
    signal loc_bsy      : std_logic;


begin


    process(sysclk)
    begin
        if rising_edge(sysclk) then
            if reset = '1' then
                loc_baudrate <= 1;
                dly_den      <= '0';
                dly_den_2    <= '0';                                
                bsy          <= '0';
            elsif den = '1' and  dly_den = '0' and dly_den_2 = '0' then
                loc_baudrate <= baudrate;
                lf_din       <= din;
                dly_den      <= '1';
                dly_den_2    <= '0';
                bsy          <= '1';
            elsif dly_den = '1' and dly_den_2 = '0' then
                dly_den      <= '0';
                dly_den_2    <= '1';
                bsy          <= '1';
            else
                dly_den      <= '0';
                dly_den_2    <= '0';
                bsy          <= loc_bsy;
            end if;
        end if;
    end process;



    process(fastclk)
    begin
        if rising_edge(fastclk) then    
            if reset = '1' then
                bitcounter <= 0;
                div_cnt    <= 0;
                shift_reg  <= (DATA_SIZE downto 0 => '1');
                loc_bsy    <= '0';
            elsif bitcounter=0 then
                if dly_den_2 = '1' then
                    bitcounter <= DATA_SIZE + 2;
                    div_cnt    <= loc_baudrate/2;
                    shift_reg  <= lf_din & '0';
                    loc_bsy    <= '1';
                else
                    loc_bsy    <= '0';
                end if;
            elsif div_cnt >= clkfrequ_int then
                div_cnt    <= div_cnt - clkfrequ_int + loc_baudrate;
                bitcounter <= bitcounter - 1;
                shift_reg  <= '1' & shift_reg(DATA_SIZE downto 1);
                if bitcounter = 1 then
                    loc_bsy <= '0';
                end if;
            else
                div_cnt    <= div_cnt + loc_baudrate;
            end if;
        end if;
    end process;

    TX <= shift_reg(0);
    


end Behavioral;
