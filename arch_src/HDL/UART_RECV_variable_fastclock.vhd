----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity UART_RECV_variable_fastclock is
    Generic (FASTCLK_FREQ : integer := 100000000;
             MAX_BAUDRATE : integer :=  33333333;
             TIME_PREC    : integer :=       100;
             DATA_SIZE    : integer := 8);
    Port ( sysclk   : in  STD_LOGIC;
           fastclk  : in  STD_LOGIC;
           reset    : in  STD_LOGIC;
           baudrate : in  INTEGER range 0 to FASTCLK_FREQ/(3*TIME_PREC);
           RX       : in  STD_LOGIC;
           dout     : out STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
           den      : out STD_LOGIC);
end UART_RECV_variable_fastclock;

architecture Behavioral of UART_RECV_variable_fastclock is

    constant clkfrequ_int : integer := FASTCLK_FREQ/TIME_PREC;
    
    signal div_cnt    : integer range -clkfrequ_int/2 to (clkfrequ_int + MAX_BAUDRATE/TIME_PREC);
    signal bitcounter : integer range 0 to DATA_SIZE + 1;
    signal shift_reg  : std_logic_vector(DATA_SIZE - 1 downto 0);
    signal RX_sampled : std_logic;
    signal new_bit    : std_logic;
    
    signal fast_den   : std_logic;
    signal loc_den    : std_logic_vector(1 downto 0);

begin
    

    process(fastclk)
    begin
        if rising_edge(fastclk) then    
            RX_sampled <= RX;
            if reset = '1' then
                bitcounter <= 0;
                div_cnt    <= 0;
                new_bit    <= '0';
            elsif bitcounter=0 then
                if RX_sampled = '0' then
                    bitcounter <= DATA_SIZE + 1;
                    div_cnt    <= 3*baudrate + baudrate/2 - clkfrequ_int/2;
                end if;
                new_bit    <= '0';
            elsif div_cnt >= clkfrequ_int then
                div_cnt    <= div_cnt - clkfrequ_int + baudrate;
                bitcounter <= bitcounter - 1;
                new_bit    <= '1';
            else
                new_bit    <= '0';
                div_cnt    <= div_cnt + baudrate;
            end if;
        end if;
    end process;

    process(fastclk)
    begin
        if rising_edge(fastclk) then
            if reset = '1' then
                shift_reg  <= (DATA_SIZE-1 downto 0 => '0');
            elsif new_bit = '1' then
                shift_reg  <= RX_sampled & shift_reg(DATA_SIZE - 1 downto 1);
            end if;
        end if;
    end process;


    process(fastclk)
    begin
        if rising_edge(fastclk) then
            if reset = '1' then
                dout       <= (DATA_SIZE-1 downto 0 => '0');
                fast_den   <= '0';
            elsif new_bit = '1' and bitcounter = 0 then
                dout       <= shift_reg;
                fast_den   <= RX_sampled;
            else
                fast_den   <= '0';
            end if;
        end if;
    end process;
    
    process(sysclk)
    begin
        if rising_edge(sysclk) then
            if reset = '1' then
                loc_den <= "00";
            elsif loc_den = "00" and fast_den = '1' then
                loc_den <= "10";
            elsif loc_den = "10" then
                loc_den <= "01";
            elsif fast_den = '0' and loc_den = "01" then
                loc_den <= "00";
            end if;
        end if;
    end process;

    den <= loc_den(1);

end Behavioral;
