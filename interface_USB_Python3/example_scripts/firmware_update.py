#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script updates the Flash memory in the FPGA
The full update process lasts 25/30mn
"""

#UPGRADE_FILENAME   = "/tmp/twist_top.bin"
UPGRADE_FILENAME       = "../../arch_src/bitstreams/SmartStim_1_03_TWIST1_to_5.bin"
UPGRADE_FILENAME_ims   = "../../arch_src/bitstreams/SmartStim_1_03_TWIST0.bin"
UPGRADE_FILENAME_tst   = "../../arch_testing/Nexys_prototyping/vivado_project/twist_on_Nexys4_vivado_2016.2.runs/impl_1/twist_on_nexys4_top.bin"

CHECK_ONLY         = False

# this is only to access the module in the ../ dir
import sys
sys.path.append("../")

import os
import TWIST_control as TC
TWIST = TC.TWIST_instance()


# select the proper file for the detected system
if TWIST.board.startswith("Nexys4"):
    print("Detected Nexys4 prototype, using dedicated configuration file")
    UPGRADE_FILENAME = UPGRADE_FILENAME_tst
elif TWIST.board == TC.Boardnames[TC.ID_TWIST_0]:
    print("Detected IMS twist0 board, using specific configuration file")
    UPGRADE_FILENAME = UPGRADE_FILENAME_ims
print("Configuration file is :\n   {}".format(UPGRADE_FILENAME))

# ask confirmation from user
confirm = input("Enter 'yes' to confirm upgrade,\nany other input will perform simulation (Ctrl-C to cancel): ")
UPGRADE_SIMULATION = confirm != "yes"

# This is useful to generate an error if the file does not exit
total = os.path.getsize(UPGRADE_FILENAME)

if not CHECK_ONLY:
    #use this line is you do not want any feedback
    #TWIST.flash_update(UPGRADE_FILENAME, simulate=UPGRADE_SIMULATION)
    
    ##use this if you want feedback on the progress
    if UPGRADE_SIMULATION:
        print("Simulating update...")
    else:
        print("sending new config (DO NOT INTERRUPT !)")
    for advance in TWIST.flash_update_iter(UPGRADE_FILENAME, simulate=UPGRADE_SIMULATION):
        # advance is the number of bytes written so far
        # should always be a multiple of 256, exept when end has been reached
        print("\r {0:3.1f}%".format(100*advance), end="")
    print("\r 100.0%")

# This part is to check if the file is correctly written to Flash memory
# again, check_flash_content_iter gives feedback on the process while it is running,
# and check_flash_content simply runs the process silently.
# The check_flash_content_iter version is preferred because we can stop the
# process if a write error is dedected at the beginning, we can also get
# where the error is.
print("checking new config")
mismatch = False
fails    = 0
for advance, result in TWIST.check_flash_content_iter(UPGRADE_FILENAME):
    if not result:
        print(" : Mismatch @{0:04X}xx".format(int(advance*total/256)))
        mismatch = True
        fails += 1
        if fails>9:
            print("Giving up")
            break
    print("\r {0:3.1f}%".format(100*advance), end="")
if fails<10:
    print("\r 100.0%")

if mismatch and CHECK_ONLY:
    print("Check FAILED !")
elif mismatch and not UPGRADE_SIMULATION:
    print("Data in Flash memory DO NOT match data in original File")
elif mismatch:
    print("OK, data in Flash memory do not match, but we only simulated the programming...")
else:
    print("Data matches the original file, everything looks fine :)")

TWIST.close()
