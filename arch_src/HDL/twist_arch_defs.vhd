----------------------------------------------------------------------------------

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.Twist_parameters.all;

package Twist_arch_types is
    
--     ######  ######  ###    ## ####### ########  #####  ###    ## ######## #######
--    ##      ##    ## ####   ## ##         ##    ##   ## ####   ##    ##    ##
--    ##      ##    ## ## ##  ## #######    ##    ####### ## ##  ##    ##    #######
--    ##      ##    ## ##  ## ##      ##    ##    ##   ## ##  ## ##    ##         ##
--     ######  ######  ##   #### #######    ##    ##   ## ##   ####    ##    #######


    constant TWIST_PROJECT_ID            : std_logic_vector(7 downto 0) := "00000001"; -- 1
    constant TWIST_VERSION_ID            : std_logic_vector(7 downto 0) := "00000011"; -- 3
    constant TWIST_STIM_CHANNELS         : natural := 8;                 -- number of stimulation channels on the system
    constant TWIST_TRIGGER_NUM           : natural := 8;                 -- number of triggers available on the system
    constant TWIST_HS_INPUTS             : natural := 2;
    constant TWIST_EXT_TRIGGS            : natural := 4;                 -- number of external trigger input/output
    constant TWIST_SAMPLE_SIZE           : natural := 16;                -- how many bits in samples
    
    constant TWIST_BASE_CLK_FREQ            : natural := 120000000;            -- system clock frequency in Hz
    --constant TWIST_IF_CLK_FREQ              : natural := 120000000;            -- interface clock frequency in Hz
    constant TWIST_RESETCNT_MAXVAL          : natural := 31;                   -- number of reset clock cycles
    constant TWIST_LEGACY_UART_BAUDRATE     : natural := 921600;               -- UART baudrate of legacy interface
    constant TWIST_LEGACY_UART_TIMEPREC     : natural := 100;                  -- frequency precision quantum for legacy UART
    constant TWIST_HWVAR_UART_INITFREQ      : natural := 115200;               -- initial frequency for hw UART
    constant TWIST_HWVAR_UART_TIMEPREC      : natural := 100;                  -- frequency precision quantum for hw UART
    constant TWIST_IF_TX_FIFODEPTH          : natural := 2048;                 -- FIFO size for feedback sending
    constant TWIST_IF_RX_FIFODEPTH          : natural := 10+TWIST_HS_INPUTS*8; -- FIFO size for unput commands
    constant TWIST_HWVAR_UART_MAXFREQ       : natural := 33333333;             -- maximal frequency allowed for hw UART
    constant TWIST_HWVAR_IDLE_BEF_SWITCH    : natural := 3;                    -- number of idle clock cycles before HW IF mode/frequency switch
    constant TWIST_DATAPACKET_MAXQUANTUM    : natural := 3;                    -- maximal multiplier for data packet size
    constant TWIST_FIFOGEN_MAXFREQ          : natural := 511;                  -- max freq for fifo output (only applied to upper 16 bits) 511 means 33MHz
    constant TWIST_COMBTREE_DEPTH           : natural := 21;                   -- number of pipeline depth for source combination
    constant TWIST_COMBTREE_RATE            : natural := 1;                    -- refresh rate for source combination tree (1 each TWIST_COMBTREE_RATE clock cycle)
    constant TWIST_MAX_DAC_PWMBITS          : natural := 7;                    -- the maximal number of bits added with PWM technique
    constant TWIST_DAC_MAX_FREQ_MHZ         : natural := 20;                   -- the max frequency of DACs in MHz
    constant TWIST_DEFAULT_DAC_PWMBITS      : natural := 2;                    -- the default amount of PWM on channel output
    constant TWIST_DEFAULT_DAC_FREQ_MHZ     : natural := 10;                   -- the default channel sampling frequency in MHz
--    constant TWIST_OFFSET_WHEN_COMBINED     : natural := 2048;                 -- the offset current to avoid to trunc the output when starting (only in combined mode)
    constant TWIST_FLASHSPI_CLK_FREQ        : natural := 25000000;             -- clock frequency for the SPI Flash memory interface 
    constant TWIST_DBGRBK_FREQDIV           : natural := 100;                  -- the ratio between the actual readback frequency and its parameter value
    constant TWIST_DEFAULT_CHAN_COMBINED    : boolean := False;                -- should channels be coupled by default ?
    constant TWIST_DEFAULT_CHAN_INVERTED    : boolean := False;                -- should channels be inverted by default ?
    constant TWIST_TRIGGSYNC_LENGTH         : natural := 1000;                 -- number of clock cycles for sync trigger length
    

    -- constants retreived from lab requirements
    constant TWIST_PATTERNS                 : natural range 1 to                       16 := TWIST_PARAMS_PATTERNS;              -- number of fixed pattern sources on the system
    constant TWIST_SINES                    : natural range 1 to                       32 := TWIST_PARAMS_SINES;                 -- number of sine generator sources on the system
    constant TWIST_FIFOS                    : natural range 1 to                       16 := TWIST_PARAMS_FIFOS;                 -- number of FIFO data sources on the system
    constant TWIST_PATTERN_MAXLENGTH        : natural range 1023 to                 65535 := TWIST_PARAMS_PATTERN_MAXLENGTH;     -- maximal number of samples for fixed patterns
    constant TWIST_SINE_BITS                : natural range 16 to                      32 := TWIST_PARAMS_SINE_BITS;             -- number of bits for sine computation
    constant TWIST_FIFOGEN_DEPTH            : natural range 1023 to                 65536 := TWIST_PARAMS_FIFOGEN_DEPTH;         -- number of samples stored in fifo generators



    -- other constants computed from the ones above
    constant TWIST_BASE_CLK_FREQ_MHZ    : natural := TWIST_BASE_CLK_FREQ/1000000;                   -- system clock frequency in MHz
    constant TWIST_TOTAL_SOURCES        : natural := TWIST_PATTERNS + TWIST_SINES + TWIST_FIFOS;    -- the total number of sources in the system
    constant TWIST_PATTNUM_OFFSET       : natural := 0;                                             -- the offset for pattern data in system-wide vectors
    constant TWIST_SINENUM_OFFSET       : natural := TWIST_PATTERNS;                                -- the offset for sine data in system-wide vectors
    constant TWIST_FIFONUM_OFFSET       : natural := TWIST_PATTERNS + TWIST_SINES;                  -- the offset for fifo data in system-wide vectors
    constant TWIST_READBACK_MAX_SAMPLES : natural := TWIST_IF_TX_FIFODEPTH;                         -- the max number of samples allowed for debug readback must be greater than 7
    constant TWIST_IF_RESET_MAXCNT      : natural := TWIST_BASE_CLK_FREQ/1000;                      -- 1 ms
    constant TWIST_DBGRBK_MAXCNT        : natural := TWIST_BASE_CLK_FREQ/TWIST_DBGRBK_FREQDIV;      -- the counter limit between two samples sent back
    



    subtype sample_type        is signed(TWIST_SAMPLE_SIZE-1 downto 0);
    subtype source_output_type is signed(19 downto 0);



--    ######   #####  ######## ######## ####### ######  ###    ## #######
--    ##   ## ##   ##    ##       ##    ##      ##   ## ####   ## ##
--    ######  #######    ##       ##    #####   ######  ## ##  ## #######
--    ##      ##   ##    ##       ##    ##      ##   ## ##  ## ##      ##
--    ##      ##   ##    ##       ##    ####### ##   ## ##   #### #######
    ------------------------------------------------------------------
    -- parameters related to Patterns
    ------------------------------------------------------------------    
    type Single_Pattern_params_t is
    record
        length    : integer range 0 to TWIST_PATTERN_MAXLENGTH;         -- index of the last sample
        freq      : unsigned(15 downto 0);                              -- times 100Hz
        trigg     : std_logic;                                          -- only '1' during 1 clock cycle
        chan_mask : std_logic_vector(TWIST_STIM_CHANNELS-1 downto 0);   -- =1 when the pattern source contributes to the channel
        trigg_num : integer range 0 to TWIST_TRIGGER_NUM;               -- the number of the trigger we are listening to, TWIST_TRIGGER_NUM means no connection
        
        -- pattern memory access
        addr      : integer range 0 to TWIST_PATTERN_MAXLENGTH;
        write     : std_logic;
        data      : sample_type;
    end record;
    type Patterns_params_t is array(0 to TWIST_PATTERNS-1) of Single_Pattern_params_t;



--    ####### ## ###    ## ####### #######
--    ##      ## ####   ## ##      ##
--    ####### ## ## ##  ## #####   #######
--         ## ## ##  ## ## ##           ##
--    ####### ## ##   #### ####### #######
    ------------------------------------------------------------------
    -- parameters related to Sine waveforms
    ------------------------------------------------------------------
    constant SINE_GEN_MAX_FREQ : integer := 18261; --15218;                      -- maximal value for frequency (to be applied on the upper 16 bits)
                                                                        -- this value is written here because it requires code modifications    
    type Single_Sine_params_t is
    record
        freq      : unsigned(31 downto 0);                              -- expressed in mHz
        amplitude : signed(15 downto 0);                                -- 32768 means max amplitude (1mA)
        periods   : unsigned(31 downto 0);                              -- number of periods to play
        --duration  : integer range 0 to 65535;                           -- playing duration expressed in ms
        load_a    : boolean;                                            -- when '1', immediately stops the generator and loads new amplitude
        active    : boolean;                                            -- when true, ignores trigger and work continuously
        param_w   : boolean;                                            -- if true, wait until end of period to apply param

        trigg     : std_logic;                                          -- only '1' during 1 clock cycle
        chan_mask : std_logic_vector(TWIST_STIM_CHANNELS-1 downto 0);   -- =1 when the sine source contributes to the corresponding channel
        trigg_num : integer range 0 to TWIST_TRIGGER_NUM;               -- the number of the trigger we are listening to, TWIST_TRIGGER_NUM means no connection
    end record;
    type Sines_params_t is array(0 to TWIST_SINES-1) of Single_Sine_params_t;



--    ####### ## #######  ######  #######
--    ##      ## ##      ##    ## ##
--    #####   ## #####   ##    ## #######
--    ##      ## ##      ##    ##      ##
--    ##      ## ##       ######  #######
    ------------------------------------------------------------------
    -- parameters related to FIFOed data output
    ------------------------------------------------------------------
    type Single_FIFO_params_t is
    record
        freq      : integer range 0 to ((TWIST_FIFOGEN_MAXFREQ+1)*(2**16) - 1);  -- the fifo sampling frequency
        enabled   : boolean;                                            -- when true, FIFO is working
        interplt  : boolean;                                            -- when true, interpolates lines between samples
        dofe      : boolean;                                            -- when true, disable on FIFO empty

        chan_mask : std_logic_vector(TWIST_STIM_CHANNELS-1 downto 0);   -- =1 when the FIFO contributes to the channel
        trigg_num : integer range 0 to TWIST_TRIGGER_NUM;               -- the number of the trigger we are listening to, TWIST_TRIGGER_NUM means no connection
        
        sample    : sample_type;                                        -- the new sample we have to write
        sample_en : std_logic;                                          -- '1' when a new sample is being written
    end record;
    type FIFOs_params_t is array(0 to TWIST_FIFOS-1) of Single_FIFO_params_t;

    -- this one is for feedback
    type single_FIFO_status_t is
    record
        full    : std_logic;                                            -- '1' when fifo is full
        empty   : std_logic;                                            -- '1' when fifo is empty
        content : integer range 0 to TWIST_FIFOGEN_DEPTH;               -- number of elements in fifo 
    end record;
    type FIFOs_status_t is array(0 to TWIST_FIFOS-1) of Single_FIFO_status_t;



--    ## ###    ## ######## ####### ######  #######  #####   ###### #######
--    ## ####   ##    ##    ##      ##   ## ##      ##   ## ##      ##
--    ## ## ##  ##    ##    #####   ######  #####   ####### ##      #####
--    ## ##  ## ##    ##    ##      ##   ## ##      ##   ## ##      ##
--    ## ##   ####    ##    ####### ##   ## ##      ##   ##  ###### #######
    ------------------------------------------------------------------
    -- parameters related to communication
    ------------------------------------------------------------------    
    type Communication_params_t is
    record
        req_mode  : std_logic_vector(2 downto 0);       -- 000 : 8bit UART
                                                        -- 001 : 16bit UART
                                                        -- 010 : 16bits at Maxspeed (RX only)
                                                        -- 011 : 16bits at Maxspeed (both RX and TX)
                                                        -- 100 : SPI
        req_speed : integer range 0 to 65535;           -- The speed for the hardware UART link / 1600
        HS_IFs    : integer range 0 to TWIST_HS_INPUTS; -- the number of HS inputs used
    end record;


    type  hw_interface_status_t is
    record
        rst_IF        : std_logic;                    -- '1' means the hardware interface should be reset
        IF_switch_SPI : std_logic;                    -- '1' means the hardware interface switches to spi mode
        IF_speed      : integer range 0 to 65535;     -- The speed for the hardware UART link / 1600
        IF_mode       : std_logic_vector(2 downto 0); -- the actual IF mode (might be different from requested one)
    end record;


--    ####### ##       #####  ####### ##   ##     ####### ######  ##
--    ##      ##      ##   ## ##      ##   ##     ##      ##   ## ##
--    #####   ##      ####### ####### #######     ####### ######  ##
--    ##      ##      ##   ##      ## ##   ##          ## ##      ##
--    ##      ####### ##   ## ####### ##   ##     ####### ##      ##
    ------------------------------------------------------------------
    -- signals related to spi flash
    ------------------------------------------------------------------    
    type flash_mgr_io_t is
    record
        data_to_flash  : std_logic_vector(15 downto 0);
        data_flash_en  : std_logic;
        instr_conted   : std_logic;
        single_byte    : std_logic;
    end record;

    type flash_fbck_t is
    record
        data_from_flash : std_logic_vector(15 downto 0);
        data_flash_en   : std_logic;
        last_data       : std_logic;
    end record;



--     ###### ##   ##  #####  ###    ## ###    ## ####### ##      #######
--    ##      ##   ## ##   ## ####   ## ####   ## ##      ##      ##
--    ##      ####### ####### ## ##  ## ## ##  ## #####   ##      #######
--    ##      ##   ## ##   ## ##  ## ## ##  ## ## ##      ##           ##
--     ###### ##   ## ##   ## ##   #### ##   #### ####### ####### #######
    ------------------------------------------------------------------
    -- parameters specific to signals
    ------------------------------------------------------------------    
    type single_chan_param_t is
    record
        PWM_bits     : integer range 0 to TWIST_MAX_DAC_PWMBITS;   -- the number of equivalent bits to add with PWM technique
        DAC_freq     : integer range 0 to TWIST_DAC_MAX_FREQ_MHZ;  -- the DAC frequency in MHz (0 means off)
        combined     : boolean;                                    -- are the channels combined ?
        comp_current : std_logic_vector(4 downto 0);               -- compensation current when combined
        inverted     : boolean;                                    -- should the channel be inverted ?
    end record;
    type chans_params_t is array(0 to TWIST_STIM_CHANNELS-1) of single_chan_param_t;



--     ######  ####### ###    ## ####### ######   #####  ##
--    ##       ##      ####   ## ##      ##   ## ##   ## ##
--    ##   ### #####   ## ##  ## #####   ######  ####### ##
--    ##    ## ##      ##  ## ## ##      ##   ## ##   ## ##
--     ######  ####### ##   #### ####### ##   ## ##   ## #######
    ------------------------------------------------------------------
    -- parameters gathered in a single record
    ------------------------------------------------------------------    
    type Twist_Parameters_t is
    record
        reset     : std_logic;
        debug_LED : std_logic_vector(7 downto 0);
        patterns  : Patterns_params_t;
        sines     : Sines_params_t;
        fifos     : FIFOs_params_t;
        chans     : chans_params_t;
        interface : Communication_params_t;
        sub_us_cnt: integer range 0 to TWIST_BASE_CLK_FREQ_MHZ-1;
        flashdat  : flash_mgr_io_t;
    end record;
    
    type Twist_Status_t is
    record
        fifos         : FIFOs_status_t;
        flashdat      : flash_fbck_t;
        hw_interface  : hw_interface_status_t;
    end record;


--     ######  ##    ## ######## ######  ##    ## ########      ###### ######## ######  ##
--    ##    ## ##    ##    ##    ##   ## ##    ##    ##        ##         ##    ##   ## ##
--    ##    ## ##    ##    ##    ######  ##    ##    ##        ##         ##    ######  ##
--    ##    ## ##    ##    ##    ##      ##    ##    ##        ##         ##    ##   ## ##
--     ######   ######     ##    ##       ######     ##         ######    ##    ##   ## #######
    ------------------------------------------------------------------
    -- types for output management
    ------------------------------------------------------------------    

    type HW_Chann_t is
    record
        Amplitude : std_logic_vector(7 downto 0);
        DAC_CS    : std_logic;
        Cathod_en : std_logic;
        Anod_en   : std_logic;
    end record;
    
    type Source_array_t is array(natural range <>) of source_output_type;
    type Channel_Values_t is array(0 to TWIST_STIM_CHANNELS-1) of sample_type;
    type Total_HW_Channs_t is array(0 to TWIST_STIM_CHANNELS-1) of HW_Chann_t; 



--    ####### ##    ## ###    ##  ###### ######## ##  ######  ###    ## #######
--    ##      ##    ## ####   ## ##         ##    ## ##    ## ####   ## ##
--    #####   ##    ## ## ##  ## ##         ##    ## ##    ## ## ##  ## #######
--    ##      ##    ## ##  ## ## ##         ##    ## ##    ## ##  ## ##      ##
--    ##       ######  ##   ####  ######    ##    ##  ######  ##   #### #######

    ------------------------------------------------------------------
    -- function declarations for parameters improvement. these functions
    -- are clearly suboptimal, and should only be used for and with
    -- constant values
    ------------------------------------------------------------------    

    function logn (x : positive; n : positive) return natural; -- logarithm of x in base n, result is rounded to the first integer above the actual value
    function min  (x : integer;  y : integer)  return integer; -- returns the min value of x and y
    function max  (x : integer;  y : integer)  return integer; -- returns the max value of x and y

end Twist_arch_types;

package body Twist_arch_types is

   function logn (x : positive; n : positive) return natural is
      variable i : natural;
      variable top : natural;
   begin
      -- mathematically, we should start with i=0 and n=1, but we need to always have a result above
      -- or equal to 1.
      i   := 1;
      top := n;
      while (top < x) loop
         i   := i + 1;
         top := top * n;
      end loop;
      return i;
   end function;

   function min  (x : integer;  y : integer)  return integer is -- returns the min value of x and y
    variable res : integer;
   begin
        if x > y then
            res := y;
        else
            res := x;
        end if;
        return res;
   end function;

   function max  (x : integer;  y : integer)  return integer is -- returns the max value of x and y
    variable res : integer;
   begin
        if x > y then
            res := x;
        else
            res := y;
        end if;
        return res;
   end function;
 
end Twist_arch_types;
