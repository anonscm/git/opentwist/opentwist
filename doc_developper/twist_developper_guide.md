# Developer guide for the Twist Architecture

## main modules / files

Lots of elements exist in various numbers. Stimulation channels only refer to the number of analog outputs of the system. All other modules that might (or not) control a channel is a generator. Generators have their own parameters, receive their own data and may obey to any trigger on the system.

* definitions
* interface
* decoding
* generators
* control

## Definitions

All the system-level definitions are in the twist_arch_defs.vhd file. It also contains the type definitions that are used in the system to be able to use them in ports. Everything is described in a single package.

### constant values

#### system related definitions

* TWIST_PROJECT_ID
* TWIST_VERSION_ID : These two values are not actually used in the system. They are output by the *read_ID* and *read_info* commands to let the user know the versioning of the system. This might help solving some issues. Original Project/Version is 1/1. Later updates will/must keep the same project number and only increase the version number if backward compatibility is provided.


* TWIST_STIM_CHANNELS  : the number of stimulation channels on the system. Although the hardware system provides 8 outputs, the architecture is able to control any number of outputs to improve eventual migration speed.

* TWIST_TRIGGER_NUM    : The number of triggers available on the system. In the early versions of the architecture, only the 8 first triggers can be addressed in an efficient way. The architecture does not support that no trigger is present in the system.

* TWIST_PATTERNS : The number of pattern generators. The instruction set limits this value to a maximum of 16 pattern generators. The Architecture does not support configurations with 0 pattern generator.

* TWIST_SINES    : The number of sine waveform generators. The instruction set limits this value to a maximum of 32 pattern generators. The Architecture does not support configurations with 0 sine generator.

* TWIST_FIFOS    : The number of FIFO waveform generators. The instruction set limits this value to a maximum of 32 pattern generators. The Architecture does not support configurations with 0 FIFO generator.

* TWIST_HS_INPUTS  : The number of alternative High Speed inputs. Such additional inputs are useful to add bandwidth between the user and the system.

#### Technical definitions

* TWIST_BASE_CLK_FREQ : This is to inform each module of the actual system clock frequency. Some modules might not use this value if the frequency precision must be maximized. In such a case, the clock frequency might be set/updated while the system is running.

* TWIST_RESETCNT_MAXVAL : The system embeds a cold start reset to avoid startup issues. This constant gives the number of clock cycles that the reset will last at the minimum.


* TWIST_LEGACY_UART_BAUDRATE
* TWIST_LEGACY_UART_TIMEPREC : These are parameters (baudrate and frequency precision) of the USB embedded UART link (to be controlled by a computer). See documentation of UART module for more details.


* TWIST_LEGACY_UART_FIFODEPTH : The size of the FIFO that contains data to be sent back through the USB embedded UART interface. Data are coming slowly, there is no problem to process them on the fly, but in some cases, big amounts of data are produced by the system (mainly commands *read_info* and *dbg_readback*)



    constant TWIST_HWVAR_UART_MAXFREQ    : integer := 333333;            -- maximal frequency allowed for hw UART
    constant TWIST_HWVAR_UART_TIMEPREC   : integer := 100;               -- frequency precision quantum for hw UART
    constant TWIST_DATAPACKET_MAXQUANTUM : integer := 3;                 -- maximal multiplier for data packet size
    constant TWIST_PATTERN_MAXLENGTH     : integer := 1023;              -- maximal number of samples for fixed patterns

* TWIST_SINE_BITS  : The size of the register used to compute the sine waveform in the sine generator. A low value frees hardware resources, but also lowers the accuracy of the computation.

* TWIST_FIFOGEN_MAXFREQ : The maximal output frequency of the FIFO generator. This value only applies to the upper 16 bits of the frequency register which is actually 32 bits. This means that the actual maximal frequency is (TWIST_FIFOGEN_MAXFREQ+1)x65536 -1

* TWIST_FIFOGEN_DEPTH : The number of samples available in the FIFO generator (The actual FIFO size).

* TWIST_COMBTREE_DEPTH : Pipeline depth of the combination tree. All generator outputs (sources) are added to obtain the value to output to the channel. This is done in an adder tree, with synchronous adders to reduce the critical path. This constant gives the maximal number of registers to go through (and the maximal number of different generators)


#### deduced constants    

    -- other constants computed from the ones above
    constant TWIST_TOTAL_SOURCES        : integer := TWIST_PATTERNS + TWIST_SINES + TWIST_FIFOS;    -- the total number of sources in the system
    constant TWIST_PATTNUM_OFFSET       : integer := 0;                                             -- the offset for pattern data in system-wide vectors
    constant TWIST_SINENUM_OFFSET       : integer := TWIST_PATTERNS;                                -- the offset for sine data in system-wide vectors
    constant TWIST_FIFONUM_OFFSET       : integer := TWIST_PATTERNS + TWIST_SINES;                  -- the offset for fifo data in system-wide vectors
    constant TWIST_READBACK_MAX_SAMPLES : integer := TWIST_LEGACY_UART_FIFODEPTH;                   -- the max number of samples allowed for debug readback must be greater than 7


### data types


## interface

## decoding

## generators

## control