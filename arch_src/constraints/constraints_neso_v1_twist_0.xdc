set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]


#    ###### ##      ##   ##      ##  ######   ######  ###    ### ##   ## #######
#   ##      ##      ##  ##      ### ##  #### ##  #### ####  #### ##   ##    ###
#   ##      ##      #####        ## ## ## ## ## ## ## ## #### ## #######   ###
#   ##      ##      ##  ##       ## ####  ## ####  ## ##  ##  ## ##   ##  ###
#    ###### ####### ##   ##      ##  ######   ######  ##      ## ##   ## #######

set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property PACKAGE_PIN F4 [get_ports clk]
create_clock -period 10.000 [get_ports clk]

#   ######  ###    ###  ######  ######       ######  ######  ###    ## ###    ## #######  ###### ########  ######  ######  #######
#   ##   ## ####  #### ##    ## ##   ##     ##      ##    ## ####   ## ####   ## ##      ##         ##    ##    ## ##   ## ##
#   ######  ## #### ## ##    ## ##   ##     ##      ##    ## ## ##  ## ## ##  ## #####   ##         ##    ##    ## ######  #######
#   ##      ##  ##  ## ##    ## ##   ##     ##      ##    ## ##  ## ## ##  ## ## ##      ##         ##    ##    ## ##   ##      ##
#   ##      ##      ##  ######  ######       ######  ######  ##   #### ##   #### #######  ######    ##     ######  ##   ## #######


# PMOD A
#set_property  { PACKAGE_PIN H4  }  [get_ports {PmodA[0]}]
#set_property  { PACKAGE_PIN G3  }  [get_ports {PmodA[1]}]
#set_property  { PACKAGE_PIN G2  }  [get_ports {PmodA[2]}]
#set_property  { PACKAGE_PIN B4  }  [get_ports {PmodA[3]}]
#set_property  { PACKAGE_PIN G1  }  [get_ports {PmodA[4]}]
#set_property  { PACKAGE_PIN H1  }  [get_ports {PmodA[5]}]
#set_property  { PACKAGE_PIN K2  }  [get_ports {PmodA[6]}]
#set_property  { PACKAGE_PIN K1  }  [get_ports {PmodA[7]}]

# PMOD C
set_property IOSTANDARD LVCMOS33 [get_ports {Pmod2_trigg_sync[0]}]
set_property PACKAGE_PIN C2 [get_ports {Pmod2_trigg_sync[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Pmod2_trigg_sync[1]}]
set_property PACKAGE_PIN C1 [get_ports {Pmod2_trigg_sync[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Pmod2_trigg_sync[2]}]
set_property PACKAGE_PIN D4 [get_ports {Pmod2_trigg_sync[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Pmod2_trigg_sync[3]}]
set_property PACKAGE_PIN C7 [get_ports {Pmod2_trigg_sync[3]}]

# PMOD D
#set_property  SLEW FAST           [get_ports {HS1_SCK}]
set_property IOSTANDARD LVCMOS33 [get_ports HS1_SCK]
set_property PACKAGE_PIN A9 [get_ports HS1_SCK]
#set_property  SLEW FAST           [get_ports {HS2_CSn}]
set_property IOSTANDARD LVCMOS33 [get_ports HS2_CSn]
set_property PACKAGE_PIN B14 [get_ports HS2_CSn]
#set_property  SLEW FAST           [get_ports {RX_HS0_MOSI}]
set_property IOSTANDARD LVCMOS33 [get_ports RX_HS0_MOSI]
set_property PACKAGE_PIN A11 [get_ports RX_HS0_MOSI]
set_property SLEW FAST [get_ports TX_MISO]
set_property IOSTANDARD LVCMOS33 [get_ports TX_MISO]
set_property PACKAGE_PIN A14 [get_ports TX_MISO]


#    ##      ####### ######          ##     ######  ##    ## ######## ########  ######  ###    ## #######
#    ##      ##      ##   ##        ##      ##   ## ##    ##    ##       ##    ##    ## ####   ## ##
#    ##      #####   ##   ##       ##       ######  ##    ##    ##       ##    ##    ## ## ##  ## #######
#    ##      ##      ##   ##      ##        ##   ## ##    ##    ##       ##    ##    ## ##  ## ##      ##
#    ####### ####### ######      ##         ######   ######     ##       ##     ######  ##   #### #######


set_property IOSTANDARD LVCMOS33 [get_ports {LED[0]}]
set_property PACKAGE_PIN U12 [get_ports {LED[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[1]}]
set_property PACKAGE_PIN R10 [get_ports {LED[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[2]}]
set_property PACKAGE_PIN T13 [get_ports {LED[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[3]}]
set_property PACKAGE_PIN R16 [get_ports {LED[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[4]}]
set_property PACKAGE_PIN V15 [get_ports {LED[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[5]}]
set_property PACKAGE_PIN U17 [get_ports {LED[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[6]}]
set_property PACKAGE_PIN N17 [get_ports {LED[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {LED[7]}]
set_property PACKAGE_PIN N14 [get_ports {LED[7]}]

#####################################################################

set_property IOSTANDARD LVCMOS33 [get_ports {Button[0]}]
# Neso v1
set_property PACKAGE_PIN C15 [get_ports {Button[0]}]
# Neso v2
#set_property PACKAGE_PIN J5 [get_ports {Button[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Button[1]}]
set_property PACKAGE_PIN C17 [get_ports {Button[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Button[2]}]
set_property PACKAGE_PIN C14 [get_ports {Button[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Button[3]}]
set_property PACKAGE_PIN B17 [get_ports {Button[3]}]


#    ######  #####  ######## ##   ##  ######  ######       ###### ######## ######  ##
#   ##      ##   ##    ##    ##   ## ##    ## ##   ##     ##         ##    ##   ## ##
#   ##      #######    ##    ####### ##    ## ##   ##     ##         ##    ######  ##
#   ##      ##   ##    ##    ##   ## ##    ## ##   ##     ##         ##    ##   ## ##
#    ###### ##   ##    ##    ##   ##  ######  ######       ######    ##    ##   ## #######

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[0]}]
set_property PACKAGE_PIN M16 [get_ports {Cath[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[1]}]
set_property PACKAGE_PIN M17 [get_ports {Cath[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[2]}]
set_property PACKAGE_PIN R12 [get_ports {Cath[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[3]}]
set_property PACKAGE_PIN R13 [get_ports {Cath[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[4]}]
set_property PACKAGE_PIN L15 [get_ports {Cath[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[5]}]
set_property PACKAGE_PIN L16 [get_ports {Cath[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[6]}]
set_property PACKAGE_PIN H16 [get_ports {Cath[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {Cath[7]}]
set_property PACKAGE_PIN G16 [get_ports {Cath[7]}]


#    #####  ###    ##  ######  ######       ###### ######## ######  ##
#   ##   ## ####   ## ##    ## ##   ##     ##         ##    ##   ## ##
#   ####### ## ##  ## ##    ## ##   ##     ##         ##    ######  ##
#   ##   ## ##  ## ## ##    ## ##   ##     ##         ##    ##   ## ##
#   ##   ## ##   ####  ######  ######       ######    ##    ##   ## #######

set_property IOSTANDARD LVCMOS33 [get_ports {AN[0]}]
set_property PACKAGE_PIN P14 [get_ports {AN[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[1]}]
set_property PACKAGE_PIN P18 [get_ports {AN[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[2]}]
set_property PACKAGE_PIN U18 [get_ports {AN[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[3]}]
set_property PACKAGE_PIN V16 [get_ports {AN[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[4]}]
set_property PACKAGE_PIN T16 [get_ports {AN[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[5]}]
set_property PACKAGE_PIN U13 [get_ports {AN[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[6]}]
set_property PACKAGE_PIN R11 [get_ports {AN[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {AN[7]}]
set_property PACKAGE_PIN V12 [get_ports {AN[7]}]


#   ######   #####   ######     #######
#   ##   ## ##   ## ##               ##
#   ##   ## ####### ##              ##
#   ##   ## ##   ## ##             ##
#   ######  ##   ##  ######        ##


set_property IOSTANDARD LVCMOS33 [get_ports {DB8[0]}]
set_property PACKAGE_PIN E16 [get_ports {DB8[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[3]}]
set_property PACKAGE_PIN G14 [get_ports {DB8[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[4]}]
set_property PACKAGE_PIN E15 [get_ports {DB8[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[1]}]
set_property PACKAGE_PIN H14 [get_ports {DB8[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[2]}]
# Neso V1
set_property PACKAGE_PIN D15 [get_ports {DB8[2]}]
# Neso V2
#set_property PACKAGE_PIN T8 [get_ports {DB8[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[5]}]
set_property PACKAGE_PIN C16 [get_ports {DB8[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[6]}]
set_property PACKAGE_PIN D14 [get_ports {DB8[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB8[7]}]
set_property PACKAGE_PIN B16 [get_ports {DB8[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[7]}]
set_property PACKAGE_PIN F15 [get_ports {CS[7]}]


#   ######   #####   ######      ######
#   ##   ## ##   ## ##          ##
#   ##   ## ####### ##          #######
#   ##   ## ##   ## ##          ##    ##
#   ######  ##   ##  ######      ######


set_property IOSTANDARD LVCMOS33 [get_ports {DB7[0]}]
set_property PACKAGE_PIN K15 [get_ports {DB7[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[1]}]
set_property PACKAGE_PIN M18 [get_ports {DB7[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[2]}]
set_property PACKAGE_PIN L18 [get_ports {DB7[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[3]}]
set_property PACKAGE_PIN J13 [get_ports {DB7[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[4]}]
set_property PACKAGE_PIN K13 [get_ports {DB7[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[5]}]
set_property PACKAGE_PIN G17 [get_ports {DB7[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[6]}]
set_property PACKAGE_PIN H17 [get_ports {DB7[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB7[7]}]
set_property PACKAGE_PIN F16 [get_ports {DB7[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[6]}]
set_property PACKAGE_PIN J15 [get_ports {CS[6]}]


#   ######   #####   ######     #######
#   ##   ## ##   ## ##          ##
#   ##   ## ####### ##          #######
#   ##   ## ##   ## ##               ##
#   ######  ##   ##  ######     #######

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[0]}]
set_property PACKAGE_PIN V17 [get_ports {DB6[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[1]}]
set_property PACKAGE_PIN U16 [get_ports {DB6[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[2]}]
set_property PACKAGE_PIN R17 [get_ports {DB6[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[3]}]
set_property PACKAGE_PIN P17 [get_ports {DB6[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[4]}]
set_property PACKAGE_PIN N16 [get_ports {DB6[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[5]}]
set_property PACKAGE_PIN N15 [get_ports {DB6[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[6]}]
set_property PACKAGE_PIN T18 [get_ports {DB6[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB6[7]}]
set_property PACKAGE_PIN R18 [get_ports {DB6[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[5]}]
set_property PACKAGE_PIN U14 [get_ports {CS[5]}]


#   ######   #####   ######     ##   ##
#   ##   ## ##   ## ##          ##   ##
#   ##   ## ####### ##          #######
#   ##   ## ##   ## ##               ##
#   ######  ##   ##  ######          ##

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[0]}]
set_property PACKAGE_PIN V10 [get_ports {DB5[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[1]}]
set_property PACKAGE_PIN U11 [get_ports {DB5[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[2]}]
set_property PACKAGE_PIN T11 [get_ports {DB5[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[3]}]
set_property PACKAGE_PIN T10 [get_ports {DB5[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[4]}]
set_property PACKAGE_PIN T9 [get_ports {DB5[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[5]}]
set_property PACKAGE_PIN T15 [get_ports {DB5[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[6]}]
set_property PACKAGE_PIN T14 [get_ports {DB5[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB5[7]}]
set_property PACKAGE_PIN V14 [get_ports {DB5[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[4]}]
set_property PACKAGE_PIN V11 [get_ports {CS[4]}]


#   ######   #####   ######     ######
#   ##   ## ##   ## ##               ##
#   ##   ## ####### ##           #####
#   ##   ## ##   ## ##               ##
#   ######  ##   ##  ######     ######

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[0]}]
set_property PACKAGE_PIN B7 [get_ports {DB4[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[1]}]
set_property PACKAGE_PIN B6 [get_ports {DB4[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[2]}]
set_property PACKAGE_PIN C11 [get_ports {DB4[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[3]}]
set_property PACKAGE_PIN C10 [get_ports {DB4[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[4]}]
set_property PACKAGE_PIN C12 [get_ports {DB4[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[5]}]
set_property PACKAGE_PIN B12 [get_ports {DB4[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[6]}]
set_property PACKAGE_PIN A15 [get_ports {DB4[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB4[7]}]
set_property PACKAGE_PIN A16 [get_ports {DB4[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[3]}]
set_property PACKAGE_PIN A5 [get_ports {CS[3]}]


#   ######   #####   ######     ######
#   ##   ## ##   ## ##               ##
#   ##   ## ####### ##           #####
#   ##   ## ##   ## ##          ##
#   ######  ##   ##  ######     #######

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[0]}]
set_property PACKAGE_PIN A3 [get_ports {DB3[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[1]}]
set_property PACKAGE_PIN E3 [get_ports {DB3[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[2]}]
set_property PACKAGE_PIN D3 [get_ports {DB3[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[3]}]
set_property PACKAGE_PIN D5 [get_ports {DB3[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[4]}]
set_property PACKAGE_PIN D8 [get_ports {DB3[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[5]}]
set_property PACKAGE_PIN E7 [get_ports {DB3[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[6]}]
set_property PACKAGE_PIN D7 [get_ports {DB3[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB3[7]}]
set_property PACKAGE_PIN A6 [get_ports {DB3[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[2]}]
set_property PACKAGE_PIN A4 [get_ports {CS[2]}]


#   ######   #####   ######      ##
#   ##   ## ##   ## ##          ###
#   ##   ## ####### ##           ##
#   ##   ## ##   ## ##           ##
#   ######  ##   ##  ######      ##

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[0]}]
set_property PACKAGE_PIN E2 [get_ports {DB2[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[1]}]
set_property PACKAGE_PIN D2 [get_ports {DB2[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[2]}]
set_property PACKAGE_PIN F1 [get_ports {DB2[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[3]}]
set_property PACKAGE_PIN E1 [get_ports {DB2[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[4]}]
set_property PACKAGE_PIN B1 [get_ports {DB2[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[5]}]
set_property PACKAGE_PIN A1 [get_ports {DB2[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[6]}]
set_property PACKAGE_PIN B3 [get_ports {DB2[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB2[7]}]
set_property PACKAGE_PIN B2 [get_ports {DB2[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[1]}]
set_property PACKAGE_PIN J2 [get_ports {CS[1]}]


#   ######   #####   ######      ######
#   ##   ## ##   ## ##          ##  ####
#   ##   ## ####### ##          ## ## ##
#   ##   ## ##   ## ##          ####  ##
#   ######  ##   ##  ######      ######

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[0]}]
set_property PACKAGE_PIN F6 [get_ports {DB1[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[1]}]
set_property PACKAGE_PIN H6 [get_ports {DB1[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[2]}]
set_property PACKAGE_PIN H5 [get_ports {DB1[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[3]}]
set_property PACKAGE_PIN F5 [get_ports {DB1[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[4]}]
set_property PACKAGE_PIN F3 [get_ports {DB1[4]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[5]}]
set_property PACKAGE_PIN E6 [get_ports {DB1[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[6]}]
set_property PACKAGE_PIN E5 [get_ports {DB1[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {DB1[7]}]
set_property PACKAGE_PIN J3 [get_ports {DB1[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {CS[0]}]
set_property PACKAGE_PIN G6 [get_ports {CS[0]}]


#   ####### ######## ######  ##         ##     ##    ## ####### ######
#   ##         ##    ##   ## ##        ##      ##    ## ##      ##   ##
#   #####      ##    ##   ## ##       ##       ##    ## ####### ######
#   ##         ##    ##   ## ##      ##        ##    ##      ## ##   ##
#   ##         ##    ######  ##     ##          ######  ####### ######

#set_property  IOSTANDARD LVCMOS33  [get_ports {data[*]}]

#set_property  PACKAGE_PIN J18   [get_ports {data[7]}]
#set_property  PACKAGE_PIN J17   [get_ports {data[6]}]
#set_property  PACKAGE_PIN G18   [get_ports {data[5]}]
#set_property  PACKAGE_PIN F18   [get_ports {data[4]}]
#set_property  PACKAGE_PIN E18   [get_ports {data[3]}]
#set_property  PACKAGE_PIN D18   [get_ports {data[2]}]

set_property IOSTANDARD LVCMOS33 [get_ports TX]
set_property PACKAGE_PIN B18 [get_ports TX]

set_property IOSTANDARD LVCMOS33 [get_ports RX]
set_property PACKAGE_PIN A18 [get_ports RX]

#set_property  IOSTANDARD LVCMOS33  [get_ports {txe}]
#set_property  PACKAGE_PIN K16   [get_ports {txe}]

#set_property  IOSTANDARD LVCMOS33  [get_ports {rxf}]
#set_property  PACKAGE_PIN G13   [get_ports {rxf}]

#set_property  IOSTANDARD LVCMOS33  [get_ports {wr}]
#set_property  { PACKAGE_PIN M13   [get_ports {wr}]

#set_property  IOSTANDARD LVCMOS33  [get_ports {rd}]
#set_property  { PACKAGE_PIN D9   [get_ports {rd}]

#set_property  IOSTANDARD LVCMOS33  [get_ports {siwub}]
#set_property  { PACKAGE_PIN D10   [get_ports {siwub}]



#   ####### ######  ######   #####  ###    ###      ## ##    ## ###    ## ##    ## ####### ####### ######  ##
#   ##      ##   ## ##   ## ##   ## ####  ####     ##  ##    ## ####   ## ##    ## ##      ##      ##   ##  ##
#   ####### ##   ## ######  ####### ## #### ##     ##  ##    ## ## ##  ## ##    ## ####### #####   ##   ##  ##
#        ## ##   ## ##   ## ##   ## ##  ##  ##     ##  ##    ## ##  ## ## ##    ##      ## ##      ##   ##  ##
#   ####### ######  ##   ## ##   ## ##      ##      ##  ######  ##   ####  ######  ####### ####### ######  ##

######################################################################
##                          DDR3       : MT41J128M16XX-125           #
##                          Frequency  : 400 MHz                     #
##                          Data Width : 16                          #
######################################################################

### PadFunction: IO_L23P_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[0]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[0]}]
##set_property  { PACKAGE_PIN R7   [get_ports {ddr3_dq[0]}]

### PadFunction: IO_L20N_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[1]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[1]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[1]}]
##set_property  { PACKAGE_PIN V6   [get_ports {ddr3_dq[1]}]

### PadFunction: IO_L24P_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[2]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[2]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[2]}]
##set_property  { PACKAGE_PIN R8   [get_ports {ddr3_dq[2]}]

### PadFunction: IO_L22P_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[3]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[3]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[3]}]
##set_property  { PACKAGE_PIN U7   [get_ports {ddr3_dq[3]}]

### PadFunction: IO_L20P_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[4]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[4]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[4]}]
##set_property  { PACKAGE_PIN V7   [get_ports {ddr3_dq[4]}]

### PadFunction: IO_L19P_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[5]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[5]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[5]}]
##set_property  { PACKAGE_PIN R6   [get_ports {ddr3_dq[5]}]

### PadFunction: IO_L22N_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[6]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[6]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[6]}]
##set_property  { PACKAGE_PIN U6   [get_ports {ddr3_dq[6]}]

### PadFunction: IO_L19N_T3_VREF_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[7]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[7]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[7]}]
##set_property  { PACKAGE_PIN R5   [get_ports {ddr3_dq[7]}]

### PadFunction: IO_L12P_T1_MRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[8]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[8]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[8]}]
##set_property  { PACKAGE_PIN T5   [get_ports {ddr3_dq[8]}]

### PadFunction: IO_L8N_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[9]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[9]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[9]}]
##set_property  { PACKAGE_PIN U3   [get_ports {ddr3_dq[9]}]

### PadFunction: IO_L10P_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[10]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[10]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[10]}]
##set_property  { PACKAGE_PIN V5   [get_ports {ddr3_dq[10]}]

### PadFunction: IO_L8P_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[11]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[11]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[11]}]
##set_property  { PACKAGE_PIN U4   [get_ports {ddr3_dq[11]}]

### PadFunction: IO_L10N_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[12]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[12]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[12]}]
##set_property  { PACKAGE_PIN V4   [get_ports {ddr3_dq[12]}]

### PadFunction: IO_L12N_T1_MRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[13]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[13]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[13]}]
##set_property  { PACKAGE_PIN T4   [get_ports {ddr3_dq[13]}]

### PadFunction: IO_L7N_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[14]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[14]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[14]}]
##set_property  { PACKAGE_PIN V1   [get_ports {ddr3_dq[14]}]

### PadFunction: IO_L11N_T1_SRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_dq[15]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dq[15]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dq[15]}]
##set_property  { PACKAGE_PIN T3   [get_ports {ddr3_dq[15]}]

### PadFunction: IO_L2P_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[13]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[13]}]
##set_property  { PACKAGE_PIN K3   [get_ports {ddr3_addr[13]}]

### PadFunction: IO_L18N_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[12]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[12]}]
##set_property  { PACKAGE_PIN N6   [get_ports {ddr3_addr[12]}]

### PadFunction: IO_L5P_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[11]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[11]}]
##set_property  { PACKAGE_PIN K5   [get_ports {ddr3_addr[11]}]

### PadFunction: IO_L15N_T2_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[10]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[10]}]
##set_property  { PACKAGE_PIN R2   [get_ports {ddr3_addr[10]}]

### PadFunction: IO_L13P_T2_MRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[9]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[9]}]
##set_property  { PACKAGE_PIN N5   [get_ports {ddr3_addr[9]}]

### PadFunction: IO_L5N_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[8]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[8]}]
##set_property  { PACKAGE_PIN L4   [get_ports {ddr3_addr[8]}]

### PadFunction: IO_L3N_T0_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[7]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[7]}]
##set_property  { PACKAGE_PIN N1   [get_ports {ddr3_addr[7]}]

### PadFunction: IO_L4N_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[6]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[6]}]
##set_property  { PACKAGE_PIN M2   [get_ports {ddr3_addr[6]}]

### PadFunction: IO_L13N_T2_MRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[5]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[5]}]
##set_property  { PACKAGE_PIN P5   [get_ports {ddr3_addr[5]}]

### PadFunction: IO_L2N_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[4]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[4]}]
##set_property  { PACKAGE_PIN L3   [get_ports {ddr3_addr[4]}]

### PadFunction: IO_L17N_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[3]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[3]}]
##set_property  { PACKAGE_PIN T1   [get_ports {ddr3_addr[3]}]

### PadFunction: IO_L18P_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[2]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[2]}]
##set_property  { PACKAGE_PIN M6   [get_ports {ddr3_addr[2]}]

### PadFunction: IO_L14P_T2_SRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[1]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[1]}]
##set_property  { PACKAGE_PIN P4   [get_ports {ddr3_addr[1]}]

### PadFunction: IO_L16P_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_addr[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_addr[0]}]
##set_property  { PACKAGE_PIN M4   [get_ports {ddr3_addr[0]}]

### PadFunction: IO_L17P_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_ba[2]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_ba[2]}]
##set_property  { PACKAGE_PIN R1   [get_ports {ddr3_ba[2]}]

### PadFunction: IO_L14N_T2_SRCC_34
##set_property  { SLEW FAST   [get_ports {ddr3_ba[1]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_ba[1]}]
##set_property  { PACKAGE_PIN P3   [get_ports {ddr3_ba[1]}]

### PadFunction: IO_L15P_T2_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_ba[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_ba[0]}]
##set_property  { PACKAGE_PIN P2   [get_ports {ddr3_ba[0]}]

### PadFunction: IO_L16N_T2_34
##set_property  { SLEW FAST   [get_ports {ddr3_ras_n}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_ras_n}]
##set_property  { PACKAGE_PIN N4   [get_ports {ddr3_ras_n}]

### PadFunction: IO_L1P_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_cas_n}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_cas_n}]
##set_property  { PACKAGE_PIN L1   [get_ports {ddr3_cas_n}]

### PadFunction: IO_L3P_T0_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_we_n}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_we_n}]
##set_property  { PACKAGE_PIN N2   [get_ports {ddr3_we_n}]

### PadFunction: IO_25_34
##set_property  { SLEW FAST   [get_ports {ddr3_reset_n}]
##set_property  { IOSTANDARD LVCMOS15   [get_ports {ddr3_reset_n}]
##set_property  { PACKAGE_PIN U8   [get_ports {ddr3_reset_n}]

### PadFunction: IO_L1N_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_cke[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_cke[0]}]
##set_property  { PACKAGE_PIN M1   [get_ports {ddr3_cke[0]}]

### PadFunction: IO_L4P_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_odt[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_odt[0]}]
##set_property  { PACKAGE_PIN M3   [get_ports {ddr3_odt[0]}]

### PadFunction: IO_0_34
##set_property  { SLEW FAST   [get_ports {ddr3_cs_n[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_cs_n[0]}]
##set_property  { PACKAGE_PIN K6   [get_ports {ddr3_cs_n[0]}]

### PadFunction: IO_L23N_T3_34
##set_property  { SLEW FAST   [get_ports {ddr3_dm[0]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dm[0]}]
##set_property  { PACKAGE_PIN T6   [get_ports {ddr3_dm[0]}]

### PadFunction: IO_L7P_T1_34
##set_property  { SLEW FAST   [get_ports {ddr3_dm[1]}]
##set_property  { IOSTANDARD SSTL15   [get_ports {ddr3_dm[1]}]
##set_property  { PACKAGE_PIN U1   [get_ports {ddr3_dm[1]}]

### PadFunction: IO_L21P_T3_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_dqs_p[0]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dqs_p[0]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_dqs_p[0]}]
##set_property  { PACKAGE_PIN U9   [get_ports {ddr3_dqs_p[0]}]

### PadFunction: IO_L21N_T3_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_dqs_n[0]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dqs_n[0]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_dqs_n[0]}]
##set_property  { PACKAGE_PIN V9   [get_ports {ddr3_dqs_n[0]}]

### PadFunction: IO_L9P_T1_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_dqs_p[1]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dqs_p[1]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_dqs_p[1]}]
##set_property  { PACKAGE_PIN U2   [get_ports {ddr3_dqs_p[1]}]

### PadFunction: IO_L9N_T1_DQS_34
##set_property  { SLEW FAST   [get_ports {ddr3_dqs_n[1]}]
##set_property  { IN_TERM UNTUNED_SPLIT_50   [get_ports {ddr3_dqs_n[1]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_dqs_n[1]}]
##set_property  { PACKAGE_PIN V2   [get_ports {ddr3_dqs_n[1]}]

### PadFunction: IO_L6P_T0_34
##set_property  { SLEW FAST   [get_ports {ddr3_ck_p[0]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_ck_p[0]}]
##set_property  { PACKAGE_PIN L6   [get_ports {ddr3_ck_p[0]}]

### PadFunction: IO_L6N_T0_VREF_34
##set_property  { SLEW FAST   [get_ports {ddr3_ck_n[0]}]
##set_property  { IOSTANDARD DIFF_SSTL15   [get_ports {ddr3_ck_n[0]}]
##set_property  { PACKAGE_PIN L5   [get_ports {ddr3_ck_n[0]}]


#    ######  ####### ######  ##     ####### ##       #####  ####### ##   ##
#   ##    ## ##      ##   ## ##     ##      ##      ##   ## ##      ##   ##
#   ##    ## ####### ######  ##     #####   ##      ####### ####### #######
#   ## ## ##      ## ##      ##     ##      ##      ##   ##      ## ##   ##
#    ######  ####### ##      ##     ##      ####### ##   ## ####### ##   ##
#       ##

set_property SLEW FAST [get_ports flash_MOSI]
set_property IOSTANDARD LVCMOS33 [get_ports flash_MOSI]
set_property PACKAGE_PIN K17 [get_ports flash_MOSI]

#set_property  SLEW FAST   [get_ports {flash_MISO}]
set_property IOSTANDARD LVCMOS33 [get_ports flash_MISO]
set_property PACKAGE_PIN K18 [get_ports flash_MISO]

#set_property  { SLEW FAST   [get_ports {QSPI_FLASH_IO2}]
#set_property  IOSTANDARD LVCMOS33  [get_ports {QSPI_FLASH_IO2}]
#set_property  { PACKAGE_PIN L14   [get_ports {QSPI_FLASH_IO2}]

#set_property  { SLEW FAST   [get_ports {QSPI_FLASH_IO3}]
#set_property  IOSTANDARD LVCMOS33  [get_ports {QSPI_FLASH_IO3}]
#set_property  { PACKAGE_PIN M14   [get_ports {QSPI_FLASH_IO3}]

#set_property  { SLEW FAST   [get_ports {QSPI_FLASH_SCLK}]
#set_property  IOSTANDARD LVCMOS33  [get_ports {QSPI_FLASH_SCLK}]
#set_property  { PACKAGE_PIN E9   [get_ports {QSPI_FLASH_SCLK}]

set_property SLEW FAST [get_ports flash_CSn]
set_property IOSTANDARD LVCMOS33 [get_ports flash_CSn]
set_property PACKAGE_PIN L13 [get_ports flash_CSn]


set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]


