----------------------------------------------------------------------------------
-- This file is part of the TWIST architecture for stimulation.
-- 
-- it is in charge of the board digital interfaces.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_IF is
    port(
            clk         : in  std_logic;
            clk_if      : in  std_logic;
            rst         : in  std_logic;
           
            rx	        : in  std_logic;                    -- legacy RX (USB)
		    tx          : out std_logic;                    -- legacy tx (USB)

            TX_MISO     : out std_logic;                    -- HW interface pin valid for both UART-TX and SPI-MISO
            RX_HS0_MOSI : in  std_logic;                    -- HW interface pin valid for both UART-RX and SPI-MOSI
            HS1_SCK     : in  std_logic;                    -- HW interface pin valid for both UART-RXHS1 and SPI-SCK
            HS2_CSn     : in  std_logic;                    -- HW interface pin valid for both UART-RXHS2 and SPI-CSn

            cmd_out : out   std_logic_vector(15 downto 0);
            cmd_en  : out   std_logic;
            exp_d   : in    std_logic;
            dbsy    : in    std_logic;
            fbck_in : in    std_logic_vector(15 downto 0);
            fbck_en : in    std_logic;
            status  : out   hw_interface_status_t;            
            params  : in    Communication_params_t);
end twist_IF;

architecture Behavioral of twist_IF is

    type hw_if_mode_type is (uart8,     -- default mode, 8bit words (speeds defaults to 115200)
                             vari16,    -- advanced mode 16bits words, predefined frequency, 128bit words are allowed too
                             hsrx,      -- high speed : advanced mode, but speed is at the max value for RX
                             hsboth,    -- high speed : advanced mode, but speed is at the max value for RX and TX
                             spi);



    signal reset                : std_logic;                                       -- local reset
    signal reset_cnt            : integer range 0 to TWIST_RESETCNT_MAXVAL := 0;   -- to generate a reset on poweron

    signal legacy_uart_recv     : std_logic_vector(7 downto 0);
    signal legacy_uart_recv_en  : std_logic;
    signal legacy_uart_send     : std_logic_vector(7 downto 0);
    signal legacy_uart_send_en  : std_logic;
    signal legacy_uart_send_bsy : std_logic;

    signal legacy_recv_word16   : std_logic_vector(15 downto 0);
    signal legacy_recv_cnt      : std_logic;
    signal legacy_recv_wenable  : std_logic;
    
    signal legacy_send_word16   : std_logic_vector(15 downto 0);
    signal legacy_send_cnt      : integer range 0 to 2;
    signal tx_send_word16       : std_logic_vector(15 downto 0);
    signal tx_fifo_empty_n      : std_logic;
    signal tx_fifo_read         : std_logic;

    signal last_receive_from_legacy : boolean := True;

    signal hw_if_mode     : hw_if_mode_type := uart8;                       -- the signal to store the current mode 
    signal UART_reset_cnt : integer range 0 to TWIST_IF_RESET_MAXCNT;       -- this counter counts the time synced_RX_HS0 is tied low
    signal SPI_reset_cnt  : integer range 0 to TWIST_IF_RESET_MAXCNT;       -- this counter counts the time synced_HS2_CSn is tied low
    signal synced_RX_HS0  : std_logic;                                      -- synced copy of RX_HS0_MOSI
    signal synced_HS2_CSn : std_logic;                                      -- synced copy of HS2_CSn
    signal reset_HWuart   : std_logic;                                      -- reset the HW uart parameters if '1'
    signal reset_spi      : std_logic;                                      -- switch to SPI mode if '1'
    signal hw_if_idle_cnt : integer range 0 to TWIST_HWVAR_IDLE_BEF_SWITCH; -- HWIF idle cycles left to allow mode/baudrate switch
    
    signal HWvar_baudrate        : integer range 0 to TWIST_BASE_CLK_FREQ/(3*TWIST_HWVAR_UART_TIMEPREC);
    signal post_HW_reset         : boolean;                                 -- when True, generates 9 stop bits instead of 1, this makes it possible
                                                                            -- for the controler to use a 16bit UART even after IF reset
                                                                            -- this flag is set after reset, and unset when data received does not start with
                                                                            -- 13 zeros to gain speed (so compatible instructions are : NOP, READ_ID, RESET_STIM,
                                                                            -- READ_INFO, SET_IF_MODE16)
    
    signal HW8bits_uart_recv     : std_logic_vector(7 downto 0);
    signal HW8bits_uart_recv_en  : std_logic;
    signal HW8bits_uart_send     : std_logic_vector(7 downto 0);
    signal HW8bits_uart_send_en  : std_logic;
    signal HW8bits_uart_send_bsy : std_logic;
    signal HW8bits_uart_TX       : std_logic;
    signal HW8bits_2nd_word      : std_logic_vector(7 downto 0);
    signal HW8bits_send_cnt      : integer range 0 to 2;

    signal HW8bits_recv_word16   : std_logic_vector(15 downto 0);
    signal HW8bits_recv_cnt      : std_logic;
    signal HW8bits_recv_wenable  : std_logic;

    signal HW16bits_uart_recv     : std_logic_vector(15 downto 0);
    signal HW16bits_uart_recv_en  : std_logic;
    signal HW16bits_HS1           : std_logic_vector(15 downto 0);
    signal HW16bits_HS1_en        : std_logic;
    signal HW16bits_HS2           : std_logic_vector(15 downto 0);
    signal HW16bits_HS2_en        : std_logic;
    signal HW16bits_uart_send     : std_logic_vector(15 downto 0);
    signal HW16bits_uart_send_en  : std_logic;
    signal HW16bits_uart_send_bsy : std_logic;
    signal HW16bits_uart_TX       : std_logic;

    signal fifo_recv_word16       : std_logic_vector(15 downto 0);
    signal fifo_recv_wenable      : std_logic;
    
    signal spi_bitdecounter       : integer range 0 to 16;
    signal spi_shiftreg_out       : std_logic_vector(15 downto 0);
    signal spi_shiftreg_in        : std_logic_vector(15 downto 0);
    signal synced_spi_CLK         : std_logic;
    signal old_spi_CLK            : std_logic;
    signal old_spi_MOSI           : std_logic;
    signal spi_new_dat_recv       : std_logic;

begin

 
    status.rst_IF        <= reset_HWuart;
    status.IF_switch_SPI <= reset_spi;


    local_reset : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                reset     <= '1';
                reset_cnt <= 0;
            elsif reset_cnt < TWIST_RESETCNT_MAXVAL then
                reset_cnt <= reset_cnt + 1;
                reset     <= '1';
            else
                reset <= '0';
            end if;
        end if;
    end process;


--    ##      #######  ######   #####   ###### ##    ##     ##    ##  #####  ######  ########
--    ##      ##      ##       ##   ## ##       ##  ##      ##    ## ##   ## ##   ##    ##
--    ##      #####   ##   ### ####### ##        ####       ##    ## ####### ######     ##
--    ##      ##      ##    ## ##   ## ##         ##        ##    ## ##   ## ##   ##    ##
--    ####### #######  ######  ##   ##  ######    ##         ######  ##   ## ##   ##    ##




    legacy_receiver : entity work.UART_RECV_generic
        Generic map(CLK_FREQU => TWIST_BASE_CLK_FREQ,
                    BAUDRATE  => TWIST_LEGACY_UART_BAUDRATE,
                    TIME_PREC => TWIST_LEGACY_UART_TIMEPREC,
                    DATA_SIZE => 8)
        Port map ( clk   => clk,
                   reset => reset,
                   RX    => rx,
                   dout  => legacy_uart_recv,
                   den   => legacy_uart_recv_en);


    legacy_sender : entity work.UART_SEND_generic
        Generic map(CLK_FREQU => TWIST_BASE_CLK_FREQ,
                    BAUDRATE  => TWIST_LEGACY_UART_BAUDRATE,
                    TIME_PREC => TWIST_LEGACY_UART_TIMEPREC,
                    DATA_SIZE => 8)
        Port map ( clk   => clk,
                   reset => reset,
                   TX    => tx,
                   din   => legacy_uart_send,
                   den   => legacy_uart_send_en,
                   bsy   => legacy_uart_send_bsy);

    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                legacy_recv_cnt     <= '0';
                legacy_recv_wenable <= '0';
            elsif legacy_uart_recv_en = '1' then
                legacy_recv_word16  <= legacy_recv_word16(7 downto 0) & legacy_uart_recv;
                legacy_recv_cnt     <= not legacy_recv_cnt;
                legacy_recv_wenable <= legacy_recv_cnt;
            else
                legacy_recv_wenable <= '0';
            end if;
        end if;
    end process;

    
    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                legacy_send_cnt     <= 0;
                legacy_uart_send_en <= '0';
                --legacy_tx_fifo_read <= '0';
            elsif tx_fifo_empty_n = '1' and legacy_send_cnt = 0 and tx_fifo_read = '0' and last_receive_from_legacy then
                legacy_send_cnt     <= 2;
                --legacy_tx_fifo_read <= '0';
            elsif legacy_uart_send_bsy = '0' and legacy_uart_send_en = '0' then
                if legacy_send_cnt = 2 then
                    legacy_uart_send_en <= '1';
                    legacy_uart_send    <= legacy_send_word16(15 downto 8);
                    legacy_send_cnt     <= 1;
                    --legacy_tx_fifo_read <= '0';
                elsif legacy_send_cnt = 1 then
                    legacy_uart_send_en <= '1';
                    legacy_uart_send    <= legacy_send_word16( 7 downto 0);
                    legacy_send_cnt     <= 0;
                    --legacy_tx_fifo_read <= '1';
                else
                    legacy_uart_send_en <= '0';
                    --legacy_tx_fifo_read <= '0';
                end if;
            else
                legacy_uart_send_en <= '0';
                --legacy_tx_fifo_read <= '0';
            end if;
        end if;
    end process;


--    ##   ## ##     ##     ######  ########      ######  ######  ###    ## ######## ######   ######  ##
--    ##   ## ##     ##     ##   ##    ##        ##      ##    ## ####   ##    ##    ##   ## ##    ## ##
--    ####### ##  #  ##     ######     ##        ##      ##    ## ## ##  ##    ##    ######  ##    ## ##
--    ##   ## ## ### ##     ##   ##    ##        ##      ##    ## ##  ## ##    ##    ##   ## ##    ## ##
--    ##   ##  ### ###      ##   ##    ##         ######  ######  ##   ####    ##    ##   ##  ######  #######

    process(clk)
    begin
        if rising_edge(clk) then
            synced_RX_HS0  <= RX_HS0_MOSI;
            synced_HS2_CSn <= HS2_CSn;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                UART_reset_cnt <= TWIST_IF_RESET_MAXCNT;
            elsif synced_RX_HS0 = '1' then
                UART_reset_cnt <= TWIST_IF_RESET_MAXCNT;
            elsif UART_reset_cnt > 0 then
                UART_reset_cnt <= UART_reset_cnt - 1;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                SPI_reset_cnt <= TWIST_IF_RESET_MAXCNT;
            elsif synced_HS2_CSn = '1' then
                SPI_reset_cnt <= TWIST_IF_RESET_MAXCNT;
            elsif SPI_reset_cnt > 0 then
                SPI_reset_cnt <= SPI_reset_cnt - 1;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                reset_HWuart <= '1';
            elsif UART_reset_cnt = 0 then
                reset_HWuart <= '1';
            else
                reset_HWuart <= '0';
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                reset_spi <= '0';
            elsif SPI_reset_cnt = 0 and hw_if_mode/=spi then
                reset_spi <= '1';
            else
                reset_spi <= '0';
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                reset_spi <= '0';
            elsif SPI_reset_cnt = 0 and hw_if_mode/=spi then
                reset_spi <= '1';
            else
                reset_spi <= '0';
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                hw_if_mode     <= uart8;
                HWvar_baudrate <= TWIST_HWVAR_UART_INITFREQ/TWIST_HWVAR_UART_TIMEPREC; 
                status.IF_mode  <= "000";
                status.IF_speed <= TWIST_HWVAR_UART_INITFREQ/(16*TWIST_HWVAR_UART_TIMEPREC);
            elsif reset_HWuart = '1' then
                hw_if_mode <= uart8;
                HWvar_baudrate <= TWIST_HWVAR_UART_INITFREQ/TWIST_HWVAR_UART_TIMEPREC; 
                status.IF_mode  <= "000";
                status.IF_speed <= TWIST_HWVAR_UART_INITFREQ/(16*TWIST_HWVAR_UART_TIMEPREC);
            elsif reset_spi = '1' then
                hw_if_mode <= spi;
                status.IF_mode  <= "100";
            elsif hw_if_idle_cnt = 0 then
                case params.req_mode is
                    when "000"  => hw_if_mode <= uart8; 
                    when "001"  => hw_if_mode <= vari16;
                    when "010"  => hw_if_mode <= hsrx;
                    when "011"  => hw_if_mode <= hsboth;
                    when others => hw_if_mode <= spi;
                end case;
                HWvar_baudrate  <= params.req_speed * 16;
                status.IF_mode  <= params.req_mode;
                status.IF_speed <= params.req_speed;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                hw_if_idle_cnt <= TWIST_HWVAR_IDLE_BEF_SWITCH;
            elsif exp_d = '1' or dbsy = '1' or tx_fifo_empty_n = '1' or HW8bits_uart_send_bsy = '1' or HW8bits_send_cnt /= 0 or HW16bits_uart_send_bsy = '1' then
                hw_if_idle_cnt <= TWIST_HWVAR_IDLE_BEF_SWITCH;
            elsif hw_if_idle_cnt > 0 then
                hw_if_idle_cnt <= hw_if_idle_cnt - 1;
            end if;    
        end if;
    end process;


--     #####  ######  ######  ## ######## ####### ######
--    ##   ## ##   ## ##   ## ##    ##    ##      ##   ##
--    ####### ######  ######  ##    ##    #####   ######
--    ##   ## ##   ## ##   ## ##    ##    ##      ##   ##
--    ##   ## ##   ## ######  ##    ##    ####### ##   ##


    TX_FIFO : entity work.FIFO_generic
    generic map( DATA_SIZE  => 16,
                 FIFO_DEPTH => TWIST_IF_TX_FIFODEPTH)
    port map(clk      => clk,
             reset    => reset,
             data_in  => fbck_in,
             dwrite   => fbck_en,
             full_n   => open,      -- the FIFO should never be full !!!
             data_out => tx_send_word16,
             dread    => tx_fifo_read,
             empty_n  => tx_fifo_empty_n);

    RX_FIFO : entity work.FIFO_generic
    generic map( DATA_SIZE  => 16,
                 FIFO_DEPTH => TWIST_IF_RX_FIFODEPTH)
    port map(clk      => clk,
             reset    => reset,
             data_in  => fifo_recv_word16,
             dwrite   => fifo_recv_wenable,
             full_n   => open,      -- the FIFO should never be full !!!
             data_out => cmd_out,
             dread    => '1',
             empty_n  => cmd_en);

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' or reset_HWuart = '1' then
                tx_fifo_read          <= '0';
                HW16bits_uart_send_en <= '0';
                HW8bits_uart_send_en  <= '0';
                legacy_send_word16    <= x"0000";
                HW8bits_2nd_word      <= x"00";
                HW16bits_uart_send    <= x"0000";
                HW8bits_send_cnt      <= 0;
            elsif last_receive_from_legacy then -- data should be sent back through legacy connexion
                if tx_fifo_empty_n = '1' and tx_fifo_read = '0' and legacy_send_cnt = 0 then
                    tx_fifo_read        <= '1';
                    legacy_send_word16  <= tx_send_word16;
                else
                    tx_fifo_read <= '0';
                end if;
                HW8bits_uart_send_en  <= '0';
                HW16bits_uart_send_en <= '0';
            elsif hw_if_mode = uart8 then
                -- 8 bit UART mode...
                -- we also perform tests on 16bits mode because we use a 16bit UART to provide eventual 9 stop bits
                if HW8bits_send_cnt = 0 and HW8bits_uart_send_bsy = '0' and HW8bits_uart_send_en = '0' and HW16bits_uart_send_bsy='0' and HW16bits_uart_send_en = '0' then
                    if tx_fifo_empty_n = '1' and tx_fifo_read = '0' then
                        tx_fifo_read         <= '1';
                        HW8bits_2nd_word     <= tx_send_word16(7 downto 0);
                        HW8bits_send_cnt     <= 1;
                        if post_HW_reset then
                            HW16bits_uart_send    <= x"FF" & tx_send_word16(15 downto 8);
                            HW16bits_uart_send_en <= '1';
                            HW8bits_uart_send_en  <= '0';
                        else
                            HW8bits_uart_send     <= tx_send_word16(15 downto 8);
                            HW8bits_uart_send_en  <= '1';
                            HW16bits_uart_send_en <= '0';
                        end if;
                    else
                        tx_fifo_read          <= '0';
                        HW8bits_uart_send_en  <= '0';                        
                        HW16bits_uart_send_en <= '0';
                    end if;
                elsif HW8bits_send_cnt = 1 and HW8bits_uart_send_bsy = '0' and HW8bits_uart_send_en = '0' and HW16bits_uart_send_bsy='0' and HW16bits_uart_send_en = '0' then
                    tx_fifo_read         <= '0';
                    HW8bits_send_cnt     <= 0;

                    if post_HW_reset then
                        HW16bits_uart_send    <= x"FF" & HW8bits_2nd_word;
                        HW16bits_uart_send_en <= '1';
                        HW8bits_uart_send_en  <= '0';
                    else
                        HW8bits_uart_send     <= HW8bits_2nd_word;
                        HW8bits_uart_send_en  <= '1';
                        HW16bits_uart_send_en <= '0';
                    end if;
                else
                    tx_fifo_read          <= '0';
                    HW8bits_uart_send_en  <= '0';
                    HW16bits_uart_send_en <= '0';
                end if;
            elsif hw_if_mode = vari16 then
                tx_fifo_read          <= '0';
                HW8bits_uart_send_en  <= '0';
                if tx_fifo_empty_n = '1' and tx_fifo_read = '0' and HW16bits_uart_send_bsy='0' and HW16bits_uart_send_en = '0' then
                    tx_fifo_read          <= '1';
                    HW16bits_uart_send    <= tx_send_word16;
                    HW16bits_uart_send_en <= '1';
                else
                    tx_fifo_read          <= '0';
                    HW16bits_uart_send_en <= '0';
                end if;
            else 
                tx_fifo_read <= '0';
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                last_receive_from_legacy <= True;
                fifo_recv_word16         <= x"0000";
                fifo_recv_wenable        <= '0';
            elsif legacy_recv_wenable = '1' then
                last_receive_from_legacy <= True;
                fifo_recv_word16         <= legacy_recv_word16;
                fifo_recv_wenable        <= '1';
            elsif hw_if_mode = uart8 then
                if HW8bits_recv_wenable = '1' then
                    last_receive_from_legacy <= False;
                    fifo_recv_word16         <= HW8bits_recv_word16;
                    fifo_recv_wenable        <= '1';                    
                else
                    fifo_recv_wenable        <= '0';
                end if;
            elsif hw_if_mode = vari16 then
                if HW16bits_uart_recv_en = '1' then
                    last_receive_from_legacy <= False;
                    fifo_recv_word16         <= HW16bits_uart_recv;
                    fifo_recv_wenable        <= '1';
                elsif HW16bits_HS1_en = '1' then
                    last_receive_from_legacy <= False;
                    fifo_recv_word16         <= HW16bits_HS1;
                    fifo_recv_wenable        <= '1';
                elsif HW16bits_HS2_en = '1' then
                    last_receive_from_legacy <= False;
                    fifo_recv_word16         <= HW16bits_HS2;
                    fifo_recv_wenable        <= '1';                    
                else
                    fifo_recv_wenable        <= '0';
                end if;
            elsif hw_if_mode = spi then
                if spi_new_dat_recv = '1' then
--                    ??
                else
                
                end if;
            else 
                fifo_recv_wenable        <= '0';
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                post_HW_reset <= True;
            elsif reset_HWuart = '1' then
                post_HW_reset <= True;
            elsif fifo_recv_word16(15 downto 3) /= (15 downto 3 => '0') and fifo_recv_wenable = '1' then
                post_HW_reset <= False;
            end if;
        end if;
    end process;


--    ##   ## ##     ##     ######  ########     ## ###    ## ######## ####### ######  #######  #####   ###### #######
--    ##   ## ##     ##     ##   ##    ##        ## ####   ##    ##    ##      ##   ## ##      ##   ## ##      ##
--    ####### ##  #  ##     ######     ##        ## ## ##  ##    ##    #####   ######  #####   ####### ##      #####
--    ##   ## ## ### ##     ##   ##    ##        ## ##  ## ##    ##    ##      ##   ## ##      ##   ## ##      ##
--    ##   ##  ### ###      ##   ##    ##        ## ##   ####    ##    ####### ##   ## ##      ##   ##  ###### #######


    -- these modules are for high frequency UART sampling
    -- I hope they will soon increase the bandwidth, but for now, they are not
    -- working :( if working well, they might allow random bandwidth until 33Mbps
    -- and 'why not) particular working values like 50Mbps
--    hw8bits_receiver : entity work.UART_RECV_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 8)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   RX       => RX_HS0_MOSI,
--                   dout     => HW8bits_uart_recv,
--                   den      => HW8bits_uart_recv_en);
--
--
--    hw8bits_sender : entity work.UART_SEND_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 8)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   TX       => HW8bits_uart_tx,
--                   din      => HW8bits_uart_send,
--                   den      => HW8bits_uart_send_en,
--                   bsy      => HW8bits_uart_send_bsy);


    -- these UART modules are not particularly powerful because they sample RX and TX at
    -- the (slow) system clock. therefore, there are timing issues for random high
    -- frequencies. they are still safe to use for baudrates below 16Mbps or for
    -- particular values like 25Mbps or 33.33Mbps

    hw8bits_receiver : entity work.UART_RECV_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 8)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   RX       => RX_HS0_MOSI,
                   dout     => HW8bits_uart_recv,
                   den      => HW8bits_uart_recv_en);


    hw8bits_sender : entity work.UART_SEND_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 8)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   TX       => HW8bits_uart_tx,
                   din      => HW8bits_uart_send,
                   den      => HW8bits_uart_send_en,
                   bsy      => HW8bits_uart_send_bsy);


--    process(clk)
--    begin
--        if rising_edge(clk) then
--            if reset_HWuart = '1' then
--                HW8bits_send_cnt     <= 0;
--                HW8bits_uart_send_en <= '0';
--            elsif tx_fifo_empty_n = '1' and tx_fifo_read = '0' and HW8bits_send_cnt = 0 and not last_receive_from_legacy and hw_if_mode = uart8 then
--                HW8bits_send_cnt     <= 2;
--            elsif HW8bits_uart_send_bsy = '0' and HW8bits_uart_send_en = '0' then
--                if HW8bits_send_cnt = 2 then
--                    HW8bits_uart_send_en <= '1';
--                    HW8bits_uart_send    <= HW8bits_send_word16(15 downto 8);
--                    HW8bits_send_cnt     <= 1;
--                elsif HW8bits_send_cnt = 1 then
--                    HW8bits_uart_send_en <= '1';
--                    HW8bits_uart_send    <= HW8bits_send_word16( 7 downto 0);
--                    HW8bits_send_cnt     <= 0;
--                else
--                    HW8bits_uart_send_en <= '0';
--                end if;
--            else
--                HW8bits_uart_send_en <= '0';
--            end if;
--        end if;
--    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset_HWuart = '1' then
                HW8bits_recv_cnt     <= '0';
                HW8bits_recv_wenable <= '0';
            elsif HW8bits_uart_recv_en = '1' then
                HW8bits_recv_word16  <= HW8bits_recv_word16(7 downto 0) & HW8bits_uart_recv;
                HW8bits_recv_cnt     <= not HW8bits_recv_cnt;
                HW8bits_recv_wenable <= HW8bits_recv_cnt;
            else
                HW8bits_recv_wenable <= '0';
            end if;
        end if;
    end process;



    -- these modules are for high frequency UART sampling
    -- I hope they will soon increase the bandwidth, but for now, they are not
    -- working :( if working well, they might allow random bandwidth until 33Mbps
    -- and 'why not) particular working values like 50Mbps

--    hw16bits_sender : entity work.UART_SEND_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 16)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   TX       => HW16bits_uart_TX,
--                   din      => HW16bits_uart_send,
--                   den      => HW16bits_uart_send_en,
--                   bsy      => HW16bits_uart_send_bsy);
--
--
--    hw16bits_receiver : entity work.UART_RECV_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 16)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   RX       => RX_HS0_MOSI,
--                   dout     => HW16bits_uart_recv,
--                   den      => HW16bits_uart_recv_en);
--
--
--    hw16bits_receiver_HS1 : entity work.UART_RECV_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 16)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   RX       => HS1_SCK,
--                   dout     => HW16bits_HS1,
--                   den      => HW16bits_HS1_en);
--
--
--    hw16bits_receiver_HS2 : entity work.UART_RECV_variable_fastclock
--        Generic map(FASTCLK_FREQ => TWIST_IF_CLK_FREQ,
--                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
--                    DATA_SIZE    => 16)
--        Port map ( sysclk   => clk,
--                   fastclk  => clk_if,
--                   reset    => reset_HWuart,
--                   baudrate => HWvar_baudrate,
--                   RX       => HS2_CSn,
--                   dout     => HW16bits_HS2,
--                   den      => HW16bits_HS2_en);

    -- these UART modules are not particularly powerful because they sample RX and TX at
    -- the (slow) system clock. therefore, there are timing issues for random high
    -- frequencies. they are still safe to use for baudrates below 16Mbps or for
    -- particular values like 25Mbps or 33.33Mbps

    hw16bits_sender : entity work.UART_SEND_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 16)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   TX       => HW16bits_uart_TX,
                   din      => HW16bits_uart_send,
                   den      => HW16bits_uart_send_en,
                   bsy      => HW16bits_uart_send_bsy);

    hw16bits_receiver : entity work.UART_RECV_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 16)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   RX       => RX_HS0_MOSI,
                   dout     => HW16bits_uart_recv,
                   den      => HW16bits_uart_recv_en);


    hw16bits_receiver_HS1 : entity work.UART_RECV_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 16)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   RX       => HS1_SCK,
                   dout     => HW16bits_HS1,
                   den      => HW16bits_HS1_en);


    hw16bits_receiver_HS2 : entity work.UART_RECV_variable
        Generic map(CLK_FREQU    => TWIST_BASE_CLK_FREQ,
                    TIME_PREC    => TWIST_HWVAR_UART_TIMEPREC,
                    DATA_SIZE    => 16)
        Port map ( clk      => clk,
                   reset    => reset_HWuart,
                   baudrate => HWvar_baudrate,
                   RX       => HS2_CSn,
                   dout     => HW16bits_HS2,
                   den      => HW16bits_HS2_en);


    -- temporary affectations for development
    TX_MISO <= HW16bits_uart_TX and HW8bits_uart_tx;
    --TX_MISO <= HW8bits_uart_tx;
    
    --HW16bits_uart_send    <= x"0000";
    --HW16bits_uart_send_en <= '0';


--    ####### ######  ##     ####### ##       #####  ##    ## #######
--    ##      ##   ## ##     ##      ##      ##   ## ##    ## ##
--    ####### ######  ##     ####### ##      ####### ##    ## #####
--         ## ##      ##          ## ##      ##   ##  ##  ##  ##
--    ####### ##      ##     ####### ####### ##   ##   ####   #######

    process(clk)
    begin
        if rising_edge(clk) then
            synced_spi_CLK <= HS1_SCK;
            old_spi_CLK  <= synced_spi_CLK;
            old_spi_MOSI <= synced_RX_HS0;
        end if;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if spi_bitdecounter = 0 then
--                if 
--                    spi_shiftreg_out
            elsif old_spi_clk = '1' and synced_spi_CLK = '0' then
                
            end if;
        end if;
    end process;

end Behavioral;
