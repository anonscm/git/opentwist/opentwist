----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;


entity twist_sine_gen is
    port(   clk              : in  std_logic;
            reset            : in  std_logic;
            params           : in  Single_Sine_params_t;
            channel          : out sample_type);
end twist_sine_gen;

architecture Behavioral of twist_sine_gen is

    constant time_cnt_bits : integer := 33;
    subtype freq_counter is unsigned(time_cnt_bits-1 downto 0);

    -- The following functions help determine constants considering clock frequency
    function comp_ref2(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := '0' & x"EDC810D5"; -- 3989311701
            elsif clkfreq = 120000000 then
                res := '1' & x"1D567A99"; --4787174041;
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref3(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(1990734443, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := '0' & x"8E636FB4";  --2388881332
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref4(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(994883570, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(1193860284, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref5(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(497381047, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(596857256, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref6(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(248682503, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(298419004, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref7(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(124340195, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(149208234, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref8(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(62170097, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(74604117, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref9(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(31084901, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(37301985, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;
    function comp_ref10(clkfreq : in integer) return freq_counter is
        variable res : freq_counter := to_unsigned(0, time_cnt_bits);
        begin
            if clkfreq = 100000000 then
                res := to_unsigned(15542367, time_cnt_bits);
            elsif clkfreq = 120000000 then
                res := to_unsigned(18650841, time_cnt_bits);
            else
                res := to_unsigned(100/0, time_cnt_bits); -- we generate an error because data is not available yet
            end if;
            return res;
        end function;


    constant freq_ref2  : freq_counter :=  comp_ref2(TWIST_BASE_CLK_FREQ);
    constant freq_ref3  : freq_counter :=  comp_ref3(TWIST_BASE_CLK_FREQ); --1990734326;
    constant freq_ref4  : freq_counter :=  comp_ref4(TWIST_BASE_CLK_FREQ); -- 994880397; --994880*1000;
    constant freq_ref5  : freq_counter :=  comp_ref5(TWIST_BASE_CLK_FREQ); -- 497379458; --497379*1000;
    constant freq_ref6  : freq_counter :=  comp_ref6(TWIST_BASE_CLK_FREQ); -- 248682129; --248682*1000;
    constant freq_ref7  : freq_counter :=  comp_ref7(TWIST_BASE_CLK_FREQ); -- 124340115; --124340*1000;
    constant freq_ref8  : freq_counter :=  comp_ref8(TWIST_BASE_CLK_FREQ); --  62169931; -- 62170*1000;
    constant freq_ref9  : freq_counter :=  comp_ref9(TWIST_BASE_CLK_FREQ); --  31084901; -- 31085*1000;
    constant freq_ref10 : freq_counter := comp_ref10(TWIST_BASE_CLK_FREQ); --  15542439; -- 15542*1000;

    signal local_sin_val  : signed(TWIST_SINE_BITS-1 downto 0);
    signal local_cos_val  : signed(TWIST_SINE_BITS-1 downto 0);
    signal preshifted_sin : signed(TWIST_SINE_BITS-1 downto 0);
    signal preshifted_cos : signed(TWIST_SINE_BITS-1 downto 0);    
    --signal time_cnt       : integer range 0 to 2*freq_ref3;
    signal time_cnt       : freq_counter;
    signal time_cnt_inc   : freq_counter;

    type shift_val_t is (nocalc, two, three, four, five, six, seven, eight, nine, ten);
    signal shift_val_sin : shift_val_t;
    signal shift_val_wait: shift_val_t;
    signal shift_val_cos : shift_val_t;
    signal compute_cos   : boolean;
    signal compute_sin   : boolean;
    signal const_shift   : shift_val_t;
    signal const_limit   : freq_counter;
    
    
    signal old_sine_sign : std_logic;
    signal rem_periods   : unsigned(31 downto 0);
    signal last_period   : boolean;
    signal zero_period   : boolean;
    signal running       : boolean;
    
    signal local_freq    : freq_counter;
    signal local_ampl    : signed(15 downto 0);
    signal new_ampl      : boolean;
    --signal update_ampl   : boolean;

begin

    process(clk)
    -- determine whether we should compute the waveform
    begin
        if rising_edge(clk) then
            if reset = '1' then
                running <= False;
            elsif params.active or params.trigg = '1' then
                running <= True;
            elsif last_period and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0' then
                running <= False;
            else 
                running <= rem_periods > 0;
            end if;
        end if;
    end process;

    process(clk)
    -- determines the number of remaining periods
    begin
        if rising_edge(clk) then
            if reset = '1' then
                rem_periods <= (others => '0');
            elsif params.trigg = '1' then
                rem_periods <= params.periods;
            elsif not zero_period and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0' then
                rem_periods <= rem_periods - 1;
            end if;
            last_period <= rem_periods = 1;
            zero_period <= rem_periods = 0;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                local_freq <= (others => '0');
            elsif not params.param_w or (old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0') then
                local_freq <= '0' & params.freq;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                local_ampl <= (others => '0');
                new_ampl   <= False;
            elsif params.load_a then
                local_ampl <= params.amplitude;
                new_ampl   <= True;
            elsif not params.param_w or (old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0') then
                new_ampl <= False;
            end if;
        end if;
    end process;


    time_cnt_inc <= time_cnt + local_freq;
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                time_cnt  <= (others => '0');
                shift_val_sin <= nocalc;
            elsif not running then
                -- not running, so nothing to do...
                time_cnt      <= const_limit - local_freq/2;
                shift_val_sin <= nocalc;
            elsif time_cnt_inc > const_limit then
                time_cnt      <= time_cnt_inc - const_limit;
                shift_val_sin <= const_shift;        
            else
                time_cnt      <= time_cnt_inc;
                shift_val_sin <= nocalc;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                const_shift <= nocalc;
                const_limit <= freq_ref2;
            elsif local_freq*4 > freq_ref3  then
                const_shift <= two;
                const_limit <= freq_ref2;
            elsif local_freq*4 > freq_ref4  then
                const_shift <= three;
                const_limit <= freq_ref3;
            elsif local_freq*4 > freq_ref5  then
                const_shift <= four;
                const_limit <= freq_ref4;
            elsif local_freq*4 > freq_ref6  then
                const_shift <= five;
                const_limit <= freq_ref5;
            elsif local_freq*4 > freq_ref7  then
                const_shift <= six;
                const_limit <= freq_ref6;
            elsif local_freq*4 > freq_ref8  then
                const_shift <= seven;
                const_limit <= freq_ref7;
            elsif local_freq*4 > freq_ref9  then
                const_shift <= eight;
                const_limit <= freq_ref8;
            elsif local_freq*4 > freq_ref10  then
                const_shift <= nine;
                const_limit <= freq_ref9;
            else
                const_shift <= ten;
                const_limit <= freq_ref10;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                shift_val_wait <= nocalc;
                shift_val_cos  <= nocalc;
                compute_cos    <= false;
                compute_sin    <= false;
            elsif (not params.param_w and new_ampl) or (params.param_w and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0') then
                shift_val_wait <= nocalc;
                shift_val_cos  <= nocalc;
                compute_cos    <= false;
                compute_sin    <= false;
            else
                shift_val_wait <= shift_val_sin;
                shift_val_cos  <= shift_val_wait;
                compute_cos    <= shift_val_sin /= nocalc;
                compute_sin    <= shift_val_cos /= nocalc;
            end if;
        end if;
    end process;
    

    process(clk)
    begin
        if rising_edge(clk) then
            case shift_val_sin is
                when two    => preshifted_sin <= (1 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 2);
                when three  => preshifted_sin <= (2 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 3);
                when four   => preshifted_sin <= (3 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 4);
                when five   => preshifted_sin <= (4 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 5);
                when six    => preshifted_sin <= (5 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 6);
                when seven  => preshifted_sin <= (6 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 7);
                when eight  => preshifted_sin <= (7 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 8);
                when nine   => preshifted_sin <= (8 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 9);
                when ten    => preshifted_sin <= (9 downto 0 =>local_sin_val(TWIST_SINE_BITS-1)) & local_sin_val(TWIST_SINE_BITS-1 downto 10);
                when others => null;
            end case;
        end if;
    end process;



    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                local_cos_val <= (others => '0');
            elsif (not params.param_w and new_ampl) or (params.param_w and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0') or not running then
                local_cos_val <= local_ampl & (TWIST_SINE_BITS-17 downto 0 => '0');
            elsif compute_cos then
                local_cos_val <= local_cos_val - preshifted_sin;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            case shift_val_cos is
                when two    => preshifted_cos <= (1 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 2);
                when three  => preshifted_cos <= (2 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 3);
                when four   => preshifted_cos <= (3 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 4);
                when five   => preshifted_cos <= (4 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 5);
                when six    => preshifted_cos <= (5 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 6);
                when seven  => preshifted_cos <= (6 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 7);
                when eight  => preshifted_cos <= (7 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 8);
                when nine   => preshifted_cos <= (8 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 9);
                when ten    => preshifted_cos <= (9 downto 0 =>local_cos_val(TWIST_SINE_BITS-1)) & local_cos_val(TWIST_SINE_BITS-1 downto 10);
                when others => null;
            end case;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                local_sin_val <= (others => '0');
            elsif (not params.param_w and new_ampl) or (params.param_w and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0') or not running then
                local_sin_val <= (others => '0');
            elsif compute_sin then
                local_sin_val <= local_sin_val + preshifted_cos;
            end if;
        end if;
    end process;



    process(clk)
    begin
        if rising_edge(clk) then
            old_sine_sign <= local_sin_val(TWIST_SINE_BITS-1);
            if last_period and old_sine_sign='1' and local_sin_val(TWIST_SINE_BITS-1) = '0' then
                channel   <= (others => '0');
            elsif not running then
                channel   <= (others => '0');
            else
                channel   <= local_sin_val(TWIST_SINE_BITS-1 downto TWIST_SINE_BITS-16);
            end if;
        end if;
    end process;

end Behavioral;
