----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.02.2017 14:22:52
-- Design Name: 
-- Module Name: twist_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity twist_top is
port(
           clk               : in std_logic;
           --rst               : in std_logic;
           
           RX				     : in std_logic;
		   TX				     : out std_logic;

		   TX_MISO           : out std_logic;
		   RX_HS0_MOSI       : in  std_logic;
		   HS1_SCK           : in  std_logic;
		   HS2_CSn           : in  std_logic;

		   flash_CSn         : out std_logic;
           flash_MOSI        : out std_logic;
           flash_MISO        : in  std_logic;

            
           DB1    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB2    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB3    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB4    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB5    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB6    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB7    : out  STD_LOGIC_VECTOR(7 downto 0);
           DB8    : out  STD_LOGIC_VECTOR(7 downto 0);
           
           CS	     : out  STD_LOGIC_VECTOR(7 downto 0);
--		   stim_dac_rw		  : out std_logic;
           Cath    : out std_logic_vector(7 downto 0);
           AN      : out std_logic_vector(7 downto 0);
           
--           PmodA : out std_logic_vector(7 downto 0);
--           PmodC : out std_logic_vector(3 downto 0);
--           PmodD : out std_logic_vector(3 downto 0);

           Pmod2_trigg_sync : inout std_logic_vector(3 downto 0); 
           LED  : out std_logic_vector(7 downto 0);
           Button : in std_logic_vector(3 downto 0)
      );
end twist_top;

architecture Behavioral of twist_top is

component clk_mult
port (  clk_in_100           : in     std_logic;
        clk_out_120          : out    std_logic);
end component;


signal led_tmp    : std_logic_vector(7 Downto 0);
signal flash_clk  : std_logic;
signal reset_bt   : std_logic;
signal bt_synced  : std_logic_vector(3 downto 0);

signal clk_120    : std_logic;


begin

clk_multiplier : clk_mult
port map( clk_in_100  => clk,
          clk_out_120 => clk_120);


Inst_twist_stimulator : entity work.twist_arch_V1
port map(
           clk         => clk_120,
           clk_hwif    => clk_120,
           rst         => reset_bt,
           
           rx          => rx,
		   tx          => tx,

		   TX_MISO     => TX_MISO,
		   RX_HS0_MOSI => RX_HS0_MOSI,
		   HS1_SCK     => HS1_SCK,
		   HS2_CSn     => HS2_CSn,
		   
		   flash_CSn   => flash_CSn,
           flash_clk   => flash_clk,
           flash_MOSI  => flash_MOSI,
           flash_MISO  => flash_MISO,
		   
            
           stim_amplitude_0 => DB1,
           stim_amplitude_1 => DB2,
           stim_amplitude_2 => DB3,
           stim_amplitude_3 => DB4,
           stim_amplitude_4 => DB5,
           stim_amplitude_5 => DB6,
           stim_amplitude_6 => DB7,
           stim_amplitude_7 => DB8,
           
           stim_dac_CS => CS,
           cathodic_order => Cath,
           anodic_order => AN,
           trigg_sync   => Pmod2_trigg_sync,
           pc_started   => led_tmp
      );

LED(3 downto 0) <= not led_tmp(3 downto 0);
LED(7 downto 4) <= not (Button or led_tmp(7 downto 4));

    process(clk)
    begin
        if rising_edge(clk_120) then
            bt_synced <= Button;
            reset_bt  <= Button(0);
--            if bt_synced = "1111" then
--                reset_bt <= '1';
--            else
--                reset_bt <= '0';
--            end if;
        end if;
    end process;

  spi_connect: STARTUPE2
    generic map(      PROG_USR => "FALSE", 
                 SIM_CCLK_FREQ => 0.0)
    port map (    CFGCLK => open,
                 CFGMCLK => open,
                     EOS => open,
                    PREQ => open,
                     CLK => '0',
                     GSR => '0',
                     GTS => '0',
               KEYCLEARB => '0',
                    PACK => '0',
                USRCCLKO => flash_clk,   -- Provide signal to output on CCLK pin 
               USRCCLKTS => '0',       -- Enable CCLK pin  
                USRDONEO => '1',       -- Drive DONE pin High even though tri-state
               USRDONETS => '1' );     -- Maintain tri-state of DONE pin 





end Behavioral;
