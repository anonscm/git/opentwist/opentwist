----------------------------------------------------------------------------------
-- NEVER SIMULATED, might provide some surprises
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.NUMERIC_STD.ALL;


entity UART_SEND_generic is
    Generic (CLK_FREQU : integer := 100000000;
         BAUDRATE  : integer :=  33333333;
         TIME_PREC : integer :=    100000;
         DATA_SIZE : integer := 8);
Port ( clk   : in  STD_LOGIC;
       reset : in  STD_LOGIC;
       TX    : out STD_LOGIC;
       din   : in  STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
       den   : in  STD_LOGIC;
       bsy   : out STD_LOGIC);
end UART_SEND_generic;

architecture Behavioral of UART_SEND_generic is


    constant clkfrequ_int : integer := CLK_FREQU/TIME_PREC;
    constant baudrate_int : integer := BAUDRATE /TIME_PREC;
    
    signal div_cnt    : integer range 0 to (clkfrequ_int + baudrate_int);
    signal bitcounter : integer range 0 to DATA_SIZE + 2;
    signal shift_reg  : std_logic_vector(DATA_SIZE downto 0);


begin


    process(clk)
    begin
        if rising_edge(clk) then    
            if reset = '1' then
                bitcounter <= 0;
                div_cnt    <= 0;
                shift_reg  <= (DATA_SIZE downto 0 => '1');
                bsy        <= '0';
            elsif bitcounter=0 then
                if den = '1' then
                    bitcounter <= DATA_SIZE + 2;
                    div_cnt    <= baudrate_int/2;
                    shift_reg  <= din & '0';
                    bsy        <= '1';
                else
                    bsy        <= '0';
                end if;
            elsif div_cnt >= clkfrequ_int then
                div_cnt    <= div_cnt - clkfrequ_int + baudrate_int;
                bitcounter <= bitcounter - 1;
                shift_reg  <= '1' & shift_reg(DATA_SIZE downto 1);
                if bitcounter = 1 then
                    bsy        <= '0';
                end if;
            else
                div_cnt    <= div_cnt + baudrate_int;
            end if;
        end if;
    end process;

    TX <= shift_reg(0);
    


end Behavioral;
