----------------------------------------------------------------------------------
-- twist_single_output_manager
--
-- this module actually controls the analog part to output the data received on
-- 'channel'. It controls one channels, so all of them may have independant
-- parameters. It receives several parameters that can be considered static at
-- the sample scale but that can be updated by the user at any time.
--
-- a channel output also needs to know how to hebave when combined
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_single_output_manager is
    generic(COMBINED_POSITIVE : boolean);  -- how should the source behave if combined ?
    port(
        clk              : in  std_logic;
        rst              : in  std_logic;
        params           : in  single_chan_param_t;
        usec_cnt         : in  integer range 0 to TWIST_BASE_CLK_FREQ_MHZ-1;
        channel          : in  sample_type;
        output_ctrl      : out HW_Chann_t);

end twist_single_output_manager;

architecture Behavioral of twist_single_output_manager is

    signal loc_output : HW_Chann_t;
    
    type DAC_state_t is (idle,          -- nothing to do, waiting for next sample
                         charge_val,    -- loading value on DAC input
                         write_val0,    -- actually writing value, period 1
                         write_val1);   -- actually writing value, period 2
    signal DAC_state : DAC_state_t;     -- what to do with DAC ?
    signal buffed_sign            : std_logic;                                  -- we output channel absolute value before its sign
                                                                                -- so we keep the sign in a buffer to keep data
                                                                                -- consitency
    signal submicrosecond_counter : integer range 0 to TWIST_BASE_CLK_FREQ_MHZ-1;
    signal new_sample             : std_logic;                                  -- '1' when we have to output a new sample
    
    --constant direct_output   : boolean := True;

    signal pwm_cnt          : integer range 0 to 2**TWIST_MAX_DAC_PWMBITS-1;    -- this counter is the PWM progression
    signal pwm_offset       : unsigned(6 downto 0);                             -- the value to which we actually
                                                                                -- perform the PWM comparison
                                                                                -- pwm_offest is also used to reound the data
                                                                                -- instead of flooring it.
    signal   offset_when_combined : sample_type;                                -- the current used for compensation in combined mode.
    constant rounding_offset      : sample_type := (6 => '1', others => '0');   -- the offset added to make rounding instead of truncature
                                                                                -- bits 0 to 6 are fractionnal parts and rounding requires
                                                                                -- to add 0.5 before truncature
 
    -- The next signals are stages of a pipeline to produce different tests on the output channel
    -- stage 1 : The PWM counter is added to the channel value. This channel value will 
    --           be truncated, which will result in a rounding.
    --           At the same time, we produce an offset to be in phase with the PWM counter
    signal channel_st1        : sample_type;                                     -- channel value after pipeline stage 1
    signal offset_st1         : sample_type;                                     -- compensation current in combined mode, with PWM contribution

    -- stage 2 : the channel value is decomposed in sign and absolute value
    --           if the channel is combined, the offset current is added to the abolute value
    signal chan_sign_st2      : std_logic;                                       -- the sign of the channel (0 = positive)
    signal chan_abs_st2       : sample_type;                                     -- value to send to DAC
    signal offset_st2         : sample_type;                                     -- compensation current to send to DAC

    -- stage 3 : another overflow test after adding the offset current and output
    --           zeroing if the channel is combined
    signal chan_sign_st3     : std_logic;                                       -- the sign of the channel (0 = positive) even in combined
    signal chan_abs_st3      : std_logic_vector(7 downto 0);                    -- value to send to DAC even if combined
    

begin

    output_ctrl <= loc_output;
    
    -- counter for system clock division. we do not increase by 1 and test until
    -- TWIST_BASE_CLK_FREQ_MHZ/params.DAC_freq to avoid the division. So we
    -- increase by params.DAC_freq until TWIST_BASE_CLK_FREQ_MHZ. To keep some
    -- precision, we do not start back at 0, but we remove TWIST_BASE_CLK_FREQ_MHZ
    -- from the counter.
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                submicrosecond_counter <= 0;
                new_sample             <= '0';
            elsif usec_cnt = 0 then
                -- this is to resynchronize all counters at the beginning of the
                -- microsecond. Should only be useful when params.DAC_freq has
                -- been update during the previous microsecond
                submicrosecond_counter <= 0;
                if params.DAC_freq = 0 then
                    new_sample             <= '0';
                else
                    new_sample             <= '1';
                end if;
            elsif submicrosecond_counter < TWIST_BASE_CLK_FREQ_MHZ - params.DAC_freq then
                submicrosecond_counter <= submicrosecond_counter + params.DAC_freq;
                new_sample             <= '0';
            else
                submicrosecond_counter <= submicrosecond_counter + params.DAC_freq - TWIST_BASE_CLK_FREQ_MHZ;
                new_sample             <= '1';
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                DAC_state <= idle;
            elsif new_sample = '1' then
                -- new_sample has priority on the DAC_state sequence. This is to
                -- garantee that all output will be synchronized at the beginning
                -- each microsecond. There might be some perturbation at the end
                -- of the microsecond at which we update params.DAC_freq. It should
                -- not be a trouble since params.DAC_freq is not intended to be
                -- updated during the experiment.
                DAC_state <= charge_val;
            else
                case DAC_state is
                    when charge_val => DAC_state <= write_val0;
                    when write_val0 => DAC_state <= write_val1;
                    when write_val1 => DAC_state <= idle;
                    when idle       => DAC_state <= idle;
                end case;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                pwm_cnt   <= 0;
            elsif new_sample = '1' then
                -- we update pwm_cnt when new_sample = '1' so that its new value is ready
                -- during charge_val, pwm_offset is then ready during write_val0
                -- rounded_channel is ready during write_val1 and loc_output is updated
                -- for the first idle clock cycle. This precaution should not be useful,
                -- because loc_output is only loaded during charge_val, but cleanness
                -- never hurts...
                if pwm_cnt > 0 then
                    pwm_cnt   <= pwm_cnt - 1;
                else
                    pwm_cnt   <= 2**params.PWM_bits - 1;
                end if;                
            end if;
        end if;
    end process;

    -- FIXME : this module might be quite complex in terms of LUT usage and
    -- critical path... might be usefull to optimize or pipeline...
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to 6 loop
                    if params.PWM_bits < i               then  pwm_offset(6-i) <= '1';
                 elsif params.PWM_bits = i               then  pwm_offset(6-i) <= '0';
                 elsif (pwm_cnt mod (2**(i+1))) < (2**i) then  pwm_offset(6-i) <= '0';
                                                         else  pwm_offset(6-i) <= '1';
                 end if;
            end loop;
        end if;
    end process;
        
    
    -- determine compensation current
    process(clk)
    begin
        if rising_edge(clk) then
            case params.comp_current is
                when "00000" => offset_when_combined <= x"0000";
                when "00001" => offset_when_combined <= x"0080";
                when "00010" => offset_when_combined <= x"0100";
                when "00011" => offset_when_combined <= x"0148";
                when "00100" => offset_when_combined <= x"0170";
                when "00101" => offset_when_combined <= x"019D";
                when "00110" => offset_when_combined <= x"01CF";
                when "00111" => offset_when_combined <= x"0207";
                when "01000" => offset_when_combined <= x"0247";
                when "01001" => offset_when_combined <= x"028E";
                when "01010" => offset_when_combined <= x"02DD";
                when "01011" => offset_when_combined <= x"0337";
                when "01100" => offset_when_combined <= x"039B";
                when "01101" => offset_when_combined <= x"040B";
                when "01110" => offset_when_combined <= x"0489";
                when "01111" => offset_when_combined <= x"0517";
                when "10000" => offset_when_combined <= x"05B5";
                when "10001" => offset_when_combined <= x"0657";
                when "10010" => offset_when_combined <= x"072F";
                when "10011" => offset_when_combined <= x"080F";
                when "10100" => offset_when_combined <= x"090A";
                when "10101" => offset_when_combined <= x"0A24";
                when "10110" => offset_when_combined <= x"0B60";
                when "10111" => offset_when_combined <= x"0CC2";
                when "11000" => offset_when_combined <= x"0E50";
                when "11001" => offset_when_combined <= x"100E";
                when "11010" => offset_when_combined <= x"1203";
                when "11011" => offset_when_combined <= x"1434";
                when "11100" => offset_when_combined <= x"16AA";
                when "11101" => offset_when_combined <= x"196D";
                when "11110" => offset_when_combined <= x"1C86";
                when  others => offset_when_combined <= x"1FFF";
            end case;
        end if;
    end process;
    

    -- the channel preparation pipeline...
    process(clk)
    begin
        if rising_edge(clk) then
            
            -- this test is to avoid any kind of overflow.
            -- it is also necessary to ensure that value 0x8000 is not used (there is
            -- no hardware combination to render this value)
            if channel(TWIST_SAMPLE_SIZE-1 downto TWIST_SAMPLE_SIZE-9) = "100000000" then
                channel_st1 <= x"8080";
            elsif channel(TWIST_SAMPLE_SIZE-1 downto TWIST_SAMPLE_SIZE-9) = "011111111" then
                channel_st1 <= x"7F80";
            else
                channel_st1 <= channel + to_integer(pwm_offset);
            end if;
            offset_st1 <= offset_when_combined + to_integer(pwm_offset);
            
            
            -- this is to decompose the sign bit and the value sent to the DAC (absolute value 
            -- of the signal). We process the sign separately to manage the invertion
            if params.inverted xor channel_st1(TWIST_SAMPLE_SIZE-1) = '1' then
                chan_sign_st2 <= '1';
            else
                chan_sign_st2 <= '0';
            end if;
            if channel_st1(TWIST_SAMPLE_SIZE-1) = '1' then
                if params.combined then
                    chan_abs_st2  <= -channel_st1 + offset_when_combined + rounding_offset;
                else
                    chan_abs_st2  <= -channel_st1 + rounding_offset;
                end if;
            else
                if params.combined then
                    chan_abs_st2  <= channel_st1 + offset_when_combined + rounding_offset;
                else
                    chan_abs_st2  <= channel_st1 + rounding_offset;
                end if;
            end if;
            offset_st2 <= offset_st1;


            -- here we manage what happens if the channel is combined
            if (COMBINED_POSITIVE xor chan_sign_st2 = '1') or (not params.combined) then
                chan_sign_st3 <= chan_sign_st2;
                if chan_abs_st2(TWIST_SAMPLE_SIZE-1) = '1' then
                    -- this bit does not code negative, but overflow from stage 2
                    chan_abs_st3 <= x"FF";
                else
                    chan_abs_st3 <= std_logic_vector(chan_abs_st2(TWIST_SAMPLE_SIZE-2 downto TWIST_SAMPLE_SIZE-9));
                end if;
            else
                -- no need to test overflow here, we only ouput the compensation so offset_st2 will always be much smaller
                chan_abs_st3  <= std_logic_vector(offset_st2(TWIST_SAMPLE_SIZE-2 downto TWIST_SAMPLE_SIZE-9));
                if COMBINED_POSITIVE then
                    chan_sign_st3 <= '0';
                else
                    chan_sign_st3 <= '1';
                end if;
            end if;
                
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                loc_output.DAC_CS    <= '1';    -- no write
                loc_output.Amplitude <= x"00";
                loc_output.Anod_en   <= '0';
                loc_output.Cathod_en <= '0';
            elsif DAC_state = charge_val then  -- we start a new sample cycle
                loc_output.DAC_CS    <= '1';    -- no write
                loc_output.Amplitude <= chan_abs_st3;
                buffed_sign <= chan_sign_st3;
            elsif DAC_state = write_val0 or DAC_state = write_val1 then              -- write time
                loc_output.DAC_CS    <= '0';
            else
                loc_output.DAC_CS    <= '1';
                if loc_output.Amplitude /= x"00" then
                    -- if output is 0, we do not need to change sign, it makes less noise for sign changes
                    -- then, this is only done if output value is different from 0
                    loc_output.Cathod_en <= not buffed_sign;
                    loc_output.Anod_en   <= buffed_sign;           
                end if;
                
            end if;
        end if;
    end process;
    
    

end Behavioral;
