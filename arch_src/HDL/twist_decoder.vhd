----------------------------------------------------------------------------------
-- module : twist_decoder.vhd
--
-- This module decodes 16bits intructions received from the interface to affect 
-- system parameters. these parameters are stored in 'params' which is declared
-- in twist_arch_defs.vhd
--
-- Althought this has not been tested yet, the module should be able to receive
-- a 16bit word each clock cycle.
-- There are two exceptions:
--  - for instructions related to the flash SPI communication, there is no
--    internal FIFO to store data words, so any data comming too fast will be lost. 
--  - for data readback instructions, it is not possible to send new instructions
--    while all reading data hass not been retrieved yet. So if reading lasts
--    1 second, it will not be possible to send instructions or data during that
--    second. this might be a problem if there is a need to feed a FIFO.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_decoder is
    port(
        clk      : in  std_logic;
        reset    : in  std_logic;

        cmd_in   : in  std_logic_vector(15 downto 0);
        cmd_en   : in  std_logic;
        exp_data : out std_logic;
        busy     : out std_logic;
        fbck_out : out std_logic_vector(15 downto 0);
        fbck_en  : out std_logic;
        
        ext_triggers : inout std_logic_vector(TWIST_EXT_TRIGGS-1 downto 0);
                                
        chans    : in  Channel_Values_t;
        status   : in  Twist_Status_t;
        params   : out Twist_Parameters_t);
end twist_decoder;

architecture Behavioral of twist_decoder is
    
--    ####### ##  ######  #######     ######  #######  ###### ##       #####  ######   #####  ######## ##  ######  ###    ##
--    ##      ## ##       ##          ##   ## ##      ##      ##      ##   ## ##   ## ##   ##    ##    ## ##    ## ####   ##
--    ####### ## ##   ### #######     ##   ## #####   ##      ##      ####### ######  #######    ##    ## ##    ## ## ##  ##
--         ## ## ##    ##      ##     ##   ## ##      ##      ##      ##   ## ##   ## ##   ##    ##    ## ##    ## ##  ## ##
--    ####### ##  ######  #######     ######  #######  ###### ####### ##   ## ##   ## ##   ##    ##    ##  ######  ##   ####

    signal loc_params      : Twist_Parameters_t;                                      -- local copy of parameters
    signal loc_fbck_en     : std_logic;                                               -- local copy of fbck_en
    signal data_to_receive : integer range 0 to 128*(2**TWIST_DATAPACKET_MAXQUANTUM); -- number of data words to receive yet
    signal data_to_send    : integer range 0 to TWIST_READBACK_MAX_SAMPLES;           -- number of data words to send for long instructionst
    signal resp_flags      : std_logic_vector(15 downto 0);
    signal prev_cmd        : std_logic_vector(15 downto 0);                           -- last command word
    signal prev_cmd_i4     : integer range 0 to 15;                                   -- last 4 bits of prev_cmd as integer
    signal prev_cmd_i5     : integer range 0 to 31;                                   -- last 5 bits of prev_cmd as integer
    signal usw             : std_logic_vector(15 downto 0);                           -- last but two data word (used for 48bits arg commands)
    signal msw             : std_logic_vector(15 downto 0);                           -- last but one data word (used for 32bits arg commands)
    signal lsw             : std_logic_vector(15 downto 0);                           -- last data word (used for commands with args)
    signal prev_dat_i3     : integer range 0 to  7;                                   -- last 3 bits of lsw as integer
    signal prev_dat_i4     : integer range 0 to 15;                                   -- last 4 bits of lsw as integer
    signal prev_dat_i5     : integer range 0 to 31;                                   -- last 5 bits of lsw as integer
    signal prev_dat_i6     : integer range 0 to 63;                                   -- last 6 bits of lsw as integer
    signal prev_dat_i8     : integer range 0 to 255;                                  -- upper 8 bits of lsw as integer
    signal prev_dat_i8u    : integer range 0 to 255;                                  -- upper 8 bits of lsw as integer
    

    type cnt_trigg_array is array(0 to TWIST_TRIGGER_NUM - 1) of unsigned(31 downto 0);
    type source_trigg_array is array(0 to TWIST_TRIGGER_NUM - 1) of integer range 0 to TWIST_TRIGGER_NUM-1;
    type ext_trigg_cnt_array is array(0 to TWIST_EXT_TRIGGS - 1) of integer range 0 to TWIST_TRIGGSYNC_LENGTH;
    signal pending_triggers: std_logic_vector(TWIST_TRIGGER_NUM-1 downto 0);          -- the triggers that will be activated at the end of the current ?s
    signal local_triggers  : std_logic_vector(TWIST_TRIGGER_NUM-1 downto 0);          -- the trigger signals (only active 1 clock cycle), when submicrosecond_counter=0
    signal ext_trigg_cnt   : ext_trigg_cnt_array;                                     -- the timers that produces the ext sync trigger
    signal ext_trigg_dir   : std_logic_vector(TWIST_EXT_TRIGGS-1 downto 0);           -- the direction of external triggers (0: input / 1: output)
    signal synced_ext_trig : std_logic_vector(TWIST_EXT_TRIGGS-1 downto 0);           -- synchronized copy of external trigger input
    signal old_ext_trig    : std_logic_vector(TWIST_EXT_TRIGGS-1 downto 0);           -- previous synchronized copy of external trigger input
    signal ext_trig_valid  : std_logic_vector(TWIST_TRIGGER_NUM-1 downto 0);          -- tells if we should trigger from external source
    signal trigg_decnt     : cnt_trigg_array;                                         -- remaining delay trigger decounter 
    signal trigg_dly_time  : cnt_trigg_array;                                         -- total delay to wait for delayed trigger
    signal trigg_father    : source_trigg_array;                                      -- which trigger to listen to for delayed trigger ?
    
    
    signal packet_size_mlt : integer range 0 to TWIST_DATAPACKET_MAXQUANTUM;
    
    type instruction_type is (unknown,
                              nop,
                              read_ID,
                              reset_stim,
                              read_info,
                              set_IF_speed,
                              set_IF_mode16,
                              dbg_readback,
                              chain_trigg,
                              set_dbg_led,
                              set_pttrn_trig,
                              set_ext_trigg_dir,
                              set_fifo_freq,
                              set_fifo_opt,
                              get_fifo_elts,
                              set_pttrn_lngth,
                              set_chan_src,
                              set_pttrn_freq,
                              set_sine_freq,
                              set_sine_ampl,
                              set_sine_periods,
                              set_chan_opts,
                              set_sine_opt,
                              set_IF_mode,
                              set_size_mult,
                              trig_chan,
                              feed_spiflash_rb,
                              feed_spiflash_norb,
                              feed_spiflash_lsb_rb,
                              feed_spiflash_lsb_norb,
                              feed_pattern_source,
                              feed_fifo_source);
    signal last_instruction   : instruction_type;
    signal sngle_word_instr   : boolean;            -- true on the clock cycle following an instruction that does not require additionnal data
    signal last_data_received : boolean;            -- true on the clock cycle following the last data received
    signal data_just_received : boolean;            -- true on the clock cycle following the reception on data/instruction
    signal cmd_pattern_ok     : boolean;            -- true when pattern num field is valid in command
    signal cmd_channel_ok     : boolean;            -- true when channel num field is valid in command
    signal cmd_sine_ok        : boolean;            -- true when sine num field is valid in command
    signal cmd_fifo_ok        : boolean;            -- true when fifo num field is valid in command
    signal cmd_pcktsizemlt_ok : boolean;            -- true if the last bits of instruction correspond to a valid multiplier
    signal cmd_trigmask_ok    : boolean;            -- true if the last bits of instruction correspond do not trigger non existing trigs
    signal cmd_HS_num         : boolean;            -- true if selected HS number is valid (lower or equal to number available)
    signal cmd_spiflash_rb    : boolean;            -- true if we just received the feed_spiflash_rb or feed_spiflash_lsb_rb command
    signal databyte1_zero     : boolean;            -- true when upper byte of last word received is zero
    signal datb1_zero6l       : boolean;            -- true when upper byte of last word is a valid pattern ID, used for set_pttrn_trig
    signal datb0_pttrn_ok     : boolean;            -- true when lower byte of last word is a valid pattern ID
    signal datb1_pttrn_ok     : boolean;            -- true when upper byte of last word is a valid pattern ID, used for set_pttrn_trig
    signal datb0_sine_ok      : boolean;            -- true when lower byte of last word is a valid sine ID
    signal datb0_fifo_ok      : boolean;            -- true when lower byte of last word is a valid fifo ID
    signal datb0_trigg_ok     : boolean;            -- true when lower byte of last word is a valid trigger ID
    signal datb1_trigg_ok     : boolean;            -- true when upper byte of last word is a valid trigger ID
    signal datb0_src_ok       : boolean;            -- true when lower byte of last word is a valid source ID
    signal datb0_chopt_ok     : boolean;            -- true when lower byte of last word is a valid chan opt value
    signal sine_freq_ok       : boolean;            -- true when upper word is valid for a sine freq
    signal fifo_freq_ok       : boolean;            -- true when upper word is valid for a fifo freq
    signal pttrn_len_ok       : boolean;            -- true when data word is valid for a pattern length
    signal data_freq_ok       : boolean;            -- true when data word is valid for a UART speed
    signal fifonodataloss     : boolean;            -- true when no overflow filling fifo 
    
    signal submicrosecond_counter : integer range 0 to TWIST_BASE_CLK_FREQ_MHZ-1;
    
    -- readback registers
    constant READ_ID_WORDS     : integer := 8;                                       -- the number of 16 bits words sent back by the READ_ID command
    signal channel_to_readback : integer range 0 to TWIST_STIM_CHANNELS - 1;         -- the channel that is being observed
    signal sample_readback_stb : std_logic;                                          -- '1' when the currrent sample should be sent
    --signal sample_readback_dat : sample_type;                                        -- the sample to send
    signal readback_timer      : integer range 0 to TWIST_DBGRBK_MAXCNT + 2**16;     -- timer value
    signal readback_freq       : integer range 0 to 65535;                           -- the sampling frequency
    
    --signal prefetched_fifoelts : std_logic_vector(15 downto 0);                      -- number of elements in fifo prepared for reading 
    signal prefetched_datback  : std_logic_vector(15 downto 0);                      -- data to send back to controler
    
    -- these are for initial LED animation.
    signal LED_fixed           : std_logic := '0';                                   -- when '1', LEDs should not be animated anymore
    signal LED_values          : std_logic_vector(7 downto 0);                       -- LED value
    signal LED_anim_cnt        : integer range 0 to TWIST_BASE_CLK_FREQ-1;           -- the counter to get slow animation
    
begin

    params  <= loc_params;
    fbck_en <= loc_fbck_en;

--    ## ###    ## ####### ######## ######      ######## ####### ####### ######## #######
--    ## ####   ## ##         ##    ##   ##        ##    ##      ##         ##    ##
--    ## ## ##  ## #######    ##    ######         ##    #####   #######    ##    #######
--    ## ##  ## ##      ##    ##    ##   ##        ##    ##           ##    ##         ##
--    ## ##   #### #######    ##    ##   ##        ##    ####### #######    ##    #######
---------------------------------------------------------------------------------------
--    This process performs tests on incoming data. It adds one clock cycle latency
--    but improves performance on decocing
---------------------------------------------------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            data_just_received <= cmd_en = '1' and data_to_receive > 0;
            last_data_received <= cmd_en = '1' and data_to_receive = 1;
            -- these signals improve performance for instruction decoding, they are only affected
            -- when a command word is received
            if reset = '1' then
                prev_cmd           <= x"0000";    -- mandatory for pattern RAM initialization
                cmd_pattern_ok     <= False;
                cmd_channel_ok     <= False;
                cmd_sine_ok        <= False;
                cmd_fifo_ok        <= False;        
                cmd_pcktsizemlt_ok <= False;
                cmd_trigmask_ok    <= False;
                cmd_HS_num         <= False;
                cmd_spiflash_rb    <= False;
            elsif cmd_en = '1' and data_to_receive = 0 then
                prev_cmd           <= cmd_in;
                prev_cmd_i4        <= to_integer(unsigned(cmd_in(3 downto 0)));
                prev_cmd_i5        <= to_integer(unsigned(cmd_in(4 downto 0)));
                cmd_pattern_ok     <= to_integer(unsigned(cmd_in(3 downto 0)))<TWIST_PATTERNS;
                cmd_channel_ok     <= to_integer(unsigned(cmd_in(3 downto 0)))<TWIST_STIM_CHANNELS;
                cmd_sine_ok        <= to_integer(unsigned(cmd_in(4 downto 0)))<TWIST_SINES;
                cmd_fifo_ok        <= to_integer(unsigned(cmd_in(3 downto 0)))<TWIST_FIFOS;
                cmd_pcktsizemlt_ok <= to_integer(unsigned(cmd_in(2 downto 0)))<=TWIST_DATAPACKET_MAXQUANTUM;
                cmd_HS_num         <= to_integer(unsigned(cmd_in(5 downto 3)))<=TWIST_HS_INPUTS;
                cmd_spiflash_rb    <= cmd_in(15)= '1' and (cmd_in(7 downto 0) = x"00" or cmd_in(7 downto 0) = x"02");
                case TWIST_TRIGGER_NUM is
                    when 0      => cmd_trigmask_ok <= cmd_in(7 downto 0) = "00000000";
                    when 1      => cmd_trigmask_ok <= cmd_in(7 downto 1) = "0000000";
                    when 2      => cmd_trigmask_ok <= cmd_in(7 downto 2) = "000000";
                    when 3      => cmd_trigmask_ok <= cmd_in(7 downto 3) = "00000";
                    when 4      => cmd_trigmask_ok <= cmd_in(7 downto 4) = "0000";
                    when 5      => cmd_trigmask_ok <= cmd_in(7 downto 5) = "000";
                    when 6      => cmd_trigmask_ok <= cmd_in(7 downto 6) = "00";
                    when 7      => cmd_trigmask_ok <= cmd_in(7 downto 7) = "0";
                    when others => cmd_trigmask_ok <= True;
                end case;
            else    -- these signals should only be valid for 1 clock cycle, so we reset them as soon as possible
                cmd_spiflash_rb    <= False;
            end if; -- other signals keep their value until the end of the command
            
            -- these signals improve performance for data analysis and decoding, they are affected for each new
            -- instruction word (command or data)
            if reset = '1' then 
                prev_dat_i3    <= 0;
                prev_dat_i4    <= 0;
                prev_dat_i5    <= 0;
                prev_dat_i6    <= 0;
                prev_dat_i8    <= 0;
                prev_dat_i8u   <= 0;
                databyte1_zero <= False;
                datb1_zero6l   <= False;
                datb0_pttrn_ok <= False;
                datb1_pttrn_ok <= False;
                datb0_sine_ok  <= False;
                datb0_fifo_ok  <= False;
                datb0_trigg_ok <= False;
                datb1_trigg_ok <= False;
                datb0_src_ok   <= False;
                pttrn_len_ok   <= False;
                data_freq_ok   <= False;
                datb0_chopt_ok <= False;
            elsif cmd_en = '1' then
                prev_dat_i3    <= to_integer(unsigned(cmd_in( 2 downto 0)));
                prev_dat_i4    <= to_integer(unsigned(cmd_in( 3 downto 0)));
                prev_dat_i5    <= to_integer(unsigned(cmd_in( 4 downto 0)));
                prev_dat_i6    <= to_integer(unsigned(cmd_in( 5 downto 0)));
                prev_dat_i8    <= to_integer(unsigned(cmd_in( 7 downto 0)));
                prev_dat_i8u   <= to_integer(unsigned(cmd_in(15 downto 8)));
                databyte1_zero <= cmd_in(15 downto  8) = x"00";
                --data_freq_ok   <= to_integer(unsigned(cmd_in(15 downto 0))) > 0 and to_integer(unsigned(cmd_in(15 downto 0))) <= (TWIST_HWVAR_UART_MAXFREQ/1600);
                data_freq_ok   <= to_integer(unsigned(cmd_in(15 downto 0))) > 0;
                datb1_zero6l   <= cmd_in(13 downto  8) = "000000";
                datb0_pttrn_ok <= cmd_in( 7 downto  4) = "0001" and to_integer(unsigned(cmd_in( 3 downto 0)))<TWIST_PATTERNS;
                datb1_pttrn_ok <= to_integer(unsigned(cmd_in(15 downto  8))) < TWIST_PATTERNS;
                datb0_sine_ok  <= cmd_in( 7 downto  5) = "010"  and to_integer(unsigned(cmd_in( 4 downto 0)))<TWIST_SINES;
                datb0_fifo_ok  <= cmd_in( 7 downto  4) = "0010"  and to_integer(unsigned(cmd_in( 3 downto 0)))<TWIST_FIFOS;
                datb0_trigg_ok <= to_integer(unsigned(cmd_in( 7 downto 0)))<=TWIST_TRIGGER_NUM;
                datb1_trigg_ok <= to_integer(unsigned(cmd_in(15 downto 8)))< TWIST_TRIGGER_NUM;
                datb0_src_ok   <= (cmd_in(7 downto 4) = "0001" and to_integer(unsigned(cmd_in(3 downto 0)))<TWIST_PATTERNS)
                               or (cmd_in(7 downto 4) = "0010" and to_integer(unsigned(cmd_in(4 downto 0)))<TWIST_FIFOS)
                               or (cmd_in(7 downto 5) = "010"  and to_integer(unsigned(cmd_in(4 downto 0)))<TWIST_SINES); 
                pttrn_len_ok   <= to_integer(unsigned(cmd_in))<= TWIST_PATTERN_MAXLENGTH;
                datb0_chopt_ok <= to_integer(unsigned(cmd_in(5 downto 0)))<=TWIST_DAC_MAX_FREQ_MHZ --and cmd_in( 5 downto  0)/="000000"
                              and to_integer(unsigned(cmd_in(9 downto 6)))<=TWIST_MAX_DAC_PWMBITS;
            end if;
            sine_freq_ok <= to_integer(unsigned(msw))<=SINE_GEN_MAX_FREQ;
            fifo_freq_ok <= to_integer(unsigned(msw))<=TWIST_FIFOGEN_MAXFREQ;
        end if;
    end process;


--     ######  ######  ###    ### ###    ###     ######  ####### ####### ######   ######  ###    ## ####### ####### #######
--    ##      ##    ## ####  #### ####  ####     ##   ## ##      ##      ##   ## ##    ## ####   ## ##      ##      ##
--    ##      ##    ## ## #### ## ## #### ##     ######  #####   ####### ######  ##    ## ## ##  ## ####### #####   #######
--    ##      ##    ## ##  ##  ## ##  ##  ##     ##   ## ##           ## ##      ##    ## ##  ## ##      ## ##           ##
--     ######  ######  ##      ## ##      ##     ##   ## ####### ####### ##       ######  ##   #### ####### ####### #######
-------------------------------------------------------------------------------
--    This process controls the response given to the distant system that sends the intructions
-------------------------------------------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                loc_fbck_en       <= '0';
                fbck_out          <= x"0000";
            elsif last_data_received or sngle_word_instr then
                case last_instruction is
                    when nop                 =>
                        loc_fbck_en      <= not status.hw_interface.IF_mode(2);  -- if IF_mod(2) = '1' we are in SPI mode, and we should not return ACK for nop.
                        fbck_out         <= x"8001"; -- OK Flag
                    when read_ID             =>
                        loc_fbck_en      <= '1';
                        fbck_out         <= x"8101"; -- OK Flag  '0' & TWIST_VERSION_ID & TWIST_PROJECT_ID;
                    when reset_stim          =>
                        loc_fbck_en      <= '1';
                        fbck_out         <= x"8001"; -- OK Flag
                    when read_info           =>
                        loc_fbck_en      <= '1';
                        fbck_out         <= '1' & std_logic_vector(to_unsigned(READ_ID_WORDS, 7)) & x"01"; -- OK FLAGS, followed by READ_ID_WORDS words
                    when set_IF_speed        =>
                        loc_fbck_en      <= '1';
                        if data_freq_ok                      then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_IF_mode16       =>
                        loc_fbck_en      <= '1';
                        fbck_out         <= x"8001"; -- OK Flag
                    when dbg_readback        =>
                        loc_fbck_en      <= '1';
                        if msw(15 downto 7) = "000000000" then
                            fbck_out         <= '1' & msw(6 downto 0) & x"01";    -- OK flag with number of expected samples
                        else
                            fbck_out         <= x"FF01";                          -- OK flag, 127 samples means actually undefined
                        end if;
                    when chain_trigg        =>
                        loc_fbck_en      <= '1';
                        if datb0_trigg_ok and datb1_trigg_ok then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_dbg_led         =>
                        loc_fbck_en  <= '1';
                        if databyte1_zero                    then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_pttrn_trig      =>
                        loc_fbck_en  <= '1';
                        if datb1_pttrn_ok and datb0_trigg_ok then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_ext_trigg_dir  =>
                        loc_fbck_en  <= '1';
                        fbck_out <= x"8001";          -- OK Flag

                    when set_fifo_freq      =>
                        loc_fbck_en  <= '1';
                        if cmd_fifo_ok and fifo_freq_ok      then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_fifo_opt      =>
                        loc_fbck_en  <= '1';
                        if cmd_fifo_ok and datb0_trigg_ok    then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when get_fifo_elts     =>
                        loc_fbck_en  <= '1';
                        if cmd_fifo_ok                       then fbck_out <= x"8101";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_pttrn_lngth     =>
                        loc_fbck_en  <= '1';      
                        if cmd_pattern_ok and pttrn_len_ok   then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data                        
                    when set_chan_src        =>
                        loc_fbck_en  <= '1';      
                        if cmd_channel_ok and datb0_src_ok   then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_pttrn_freq      =>
                        loc_fbck_en  <= '1';      
                        if cmd_pattern_ok                    then fbck_out <= x"8001";          -- OK Flag
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_sine_freq       =>
                        loc_fbck_en  <= '1';      
                        if cmd_sine_ok and sine_freq_ok      then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data                    
                    when set_sine_ampl       =>
                        loc_fbck_en  <= '1';      
                        if cmd_sine_ok                       then fbck_out <= x"8001";          -- OK Flag
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_sine_periods    =>
                        loc_fbck_en  <= '1';      
                        if cmd_sine_ok                       then fbck_out <= x"8001";          -- OK Flag 
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when set_chan_opts        =>
                        loc_fbck_en  <= '1';
                        if cmd_channel_ok and datb0_chopt_ok then fbck_out <= x"8001";         -- OK Flag
                                                             else fbck_out <= x"8004"; end if; -- Invalid Data
                    when set_sine_opt        =>
                        loc_fbck_en  <= '1';
                        if cmd_sine_ok and datb0_trigg_ok and datb1_zero6l then fbck_out <= x"8001";         -- OK Flag
                                                                           else fbck_out <= x"8004"; end if; -- Invalid Data
                    when set_IF_mode         =>
                        loc_fbck_en  <= '1';
                        if cmd_HS_num                        then fbck_out <= x"8001";         -- OK Flag
                                                             else fbck_out <= x"8004"; end if; -- Invalid Data
                    when set_size_mult       =>
                        loc_fbck_en          <= '1';
                        if cmd_pcktsizemlt_ok                then fbck_out <= x"8001";          -- OK Flag
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when trig_chan           =>
                        loc_fbck_en          <= '1';
                        if cmd_trigmask_ok                   then fbck_out <= x"8001";          -- OK Flag
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when feed_spiflash_rb | feed_spiflash_lsb_rb =>
                        -- we do not send feedback now because it would mean it sent after the last data byte.
                        -- we want it to be sent before the first data byte. so it is sent when data_to_send > 0, see further
                        loc_fbck_en          <= '0';
                    when feed_spiflash_norb | feed_spiflash_lsb_norb =>
                        loc_fbck_en          <= '0';
                        -- we do not send feedback now because we want it sent fater the last SPI transfer is issued.
                        -- so it is sent when data_to_send > 0, see further
                    when feed_pattern_source =>
                        loc_fbck_en  <= '1';      
                        if cmd_pattern_ok                    then fbck_out <= x"8001";          -- OK Flag
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data
                    when feed_fifo_source    =>
                        loc_fbck_en  <= '1';
                        if cmd_fifo_ok and fifonodataloss    then fbck_out <= x"8101";          -- OK Flag
                        elsif cmd_fifo_ok                    then fbck_out <= x"8108";          -- data loss
                                                             else fbck_out <= x"8004"; end if;  -- Invalid Data            
                    when unknown             =>
                        fbck_out         <= x"8002"; -- Unknown Flag
                        loc_fbck_en      <= '1';
                end case;
            elsif data_to_send>0 and loc_fbck_en = '0' then
                case last_instruction is
                    when read_ID   =>
                        loc_fbck_en      <= '1';
                        fbck_out         <= TWIST_PROJECT_ID & TWIST_VERSION_ID;
                    when read_info =>
                        loc_fbck_en      <= '1';
                        case data_to_send is
                            when 8      => fbck_out <= TWIST_PROJECT_ID & TWIST_VERSION_ID;
                            when 7      => fbck_out <= std_logic_vector(to_unsigned(TWIST_STIM_CHANNELS,         8)) & std_logic_vector(to_unsigned(TWIST_TRIGGER_NUM,           8));
                            when 6      => fbck_out <= std_logic_vector(to_unsigned(TWIST_PATTERNS,              8)) & std_logic_vector(to_unsigned(TWIST_SINES,                 8));
                            when 5      => fbck_out <= std_logic_vector(to_unsigned(TWIST_FIFOS,                 8)) & std_logic_vector(to_unsigned(0,                           8));
                            when 4      => fbck_out <= std_logic_vector(to_unsigned(TWIST_PATTERN_MAXLENGTH,    16));
                            when 3      => fbck_out <= std_logic_vector(to_unsigned(TWIST_FIFOGEN_DEPTH,        16));
                            when 2      => fbck_out <= std_logic_vector(to_unsigned(TWIST_DATAPACKET_MAXQUANTUM, 3)) & std_logic_vector(to_unsigned(TWIST_READBACK_MAX_SAMPLES, 13));
                            when 1      => fbck_out <= std_logic_vector(to_unsigned(TWIST_EXT_TRIGGS,            4)) & std_logic_vector(to_unsigned(TWIST_HS_INPUTS,             3)) & "000000000";
                            when others => fbck_out <= x"0000";
                        end case;
                    when dbg_readback =>
                        loc_fbck_en <= sample_readback_stb;
                        fbck_out    <= prefetched_datback;
                    when get_fifo_elts | feed_fifo_source =>
                        loc_fbck_en <= '1';
                        fbck_out    <= prefetched_datback;
                    when feed_spiflash_rb | feed_spiflash_lsb_rb =>
                        if cmd_spiflash_rb then
                            -- could be nice to actually display the amount of data to send back
                            -- might take some resources as the master is perfectly aware of the data amount => necessary ?
                            loc_fbck_en       <= '1';
                            fbck_out          <= x"FF01";                            
                        else
                            loc_fbck_en          <= status.flashdat.data_flash_en;
                            fbck_out             <= status.flashdat.data_from_flash;
                        end if;               
                    when feed_spiflash_norb | feed_spiflash_lsb_norb =>
                        loc_fbck_en          <= status.flashdat.last_data;
                        fbck_out             <= x"8001";          -- OK Flag               
                    when others    =>
                        loc_fbck_en       <= '0';
                        fbck_out          <= x"0000";                
                end case;
            else
                loc_fbck_en       <= '0';
                fbck_out          <= x"0000";                
            end if;
        end if;
    end process;

    -- in this process, we pipeline data to be output.
    -- prefetched_datback is used in the previous process, this pipeline step shortens
    -- the critical path. I fear it to be huge because of its high level of multiplexing
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                prefetched_datback <= x"0000";
            elsif sngle_word_instr and last_instruction = get_fifo_elts and cmd_fifo_ok then
                prefetched_datback <= std_logic_vector(to_unsigned(status.fifos(prev_cmd_i4).content, 16));
            elsif last_data_received and last_instruction = feed_fifo_source and cmd_fifo_ok then
                prefetched_datback <= std_logic_vector(to_unsigned(TWIST_FIFOGEN_DEPTH - status.fifos(prev_cmd_i4).content - 1, 16));
            elsif last_instruction = dbg_readback then
                prefetched_datback <= std_logic_vector(chans(channel_to_readback));
            end if;
        end if;
    end process;


--    ## ###    ## ####### ######## ######      ######  #######  ######  ######  ######  ## ###    ##  ######
--    ## ####   ## ##         ##    ##   ##     ##   ## ##      ##      ##    ## ##   ## ## ####   ## ##
--    ## ## ##  ## #######    ##    ######      ##   ## #####   ##      ##    ## ##   ## ## ## ##  ## ##   ###
--    ## ##  ## ##      ##    ##    ##   ##     ##   ## ##      ##      ##    ## ##   ## ## ##  ## ## ##    ##
--    ## ##   #### #######    ##    ##   ##     ######  #######  ######  ######  ######  ## ##   ####  ######
-----------------------------------------------------------------------------------------
--    Here is the real instruction decoder.
--    nothing particularly complex, just lots of tests
--    The process is not using a 'case' statement because the number of bits to 
--    test is always changing.
--    the 'or' mask is always repeated (even is useless) to improve symetry and
--    reading comfort
-----------------------------------------------------------------------------------------

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                data_to_receive   <= 0;
                last_instruction  <= unknown;
                sngle_word_instr  <= False;
            elsif cmd_en = '1' then
                if data_to_receive > 1 then     -- we need a specific statement so that msw and usw are established 1 period before data is analysed
                    msw <= cmd_in;
                    usw <= msw;
                end if;
                if data_to_receive > 0 then
                    -----------------------------------------------------------------------
                    -- Here, we manage received data
                    -----------------------------------------------------------------------
                    data_to_receive   <= data_to_receive - 1;
                    sngle_word_instr  <= False;
                    lsw <= cmd_in;
                else
                    -----------------------------------------------------------------------
                    -- Here, we manage commands
                    -----------------------------------------------------------------------

                    if    (cmd_in or x"0000" )= x"0000" then
                        data_to_receive  <= 0;                       -- NOP, no args expected
                        sngle_word_instr <= True;
                        last_instruction <= nop;
                    elsif (cmd_in or x"0000" )= x"0001" then
                        data_to_receive  <= 0;                       -- READ_ID, no args expected
                        sngle_word_instr <= True;
                        last_instruction <= read_ID;
                    elsif (cmd_in or x"0000" )= x"0002" then 
                        data_to_receive  <= 0;                       -- RESET_STIM, no args expected
                        sngle_word_instr <= True;
                        last_instruction <= reset_stim; 
                    elsif (cmd_in or x"0000" )= x"0003" then 
                        data_to_receive  <= 0;                       -- READ_INFO, no args expected
                        sngle_word_instr <= True;
                        last_instruction <= read_info; 
                    elsif (cmd_in or x"0000" )= x"0004" then         -- Set IF speed
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_IF_speed;
                    elsif (cmd_in or x"0000" )= x"0005" then         -- set_IF_mode16, no args expected
                        data_to_receive  <= 0;
                        sngle_word_instr <= True;
                        last_instruction <= set_IF_mode16; 
                    elsif (cmd_in or x"0000" )= x"0006" then         -- DBG_readback
                        data_to_receive  <= 3;
                        sngle_word_instr <= False;
                        last_instruction <= dbg_readback;
                    elsif (cmd_in or x"0000" )= x"0007" then         -- CHAIN_TRIGG
                        data_to_receive  <= 3;
                        sngle_word_instr <= False;
                        last_instruction <= chain_trigg;
                    elsif (cmd_in or x"0000" )= x"0009" then
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= unknown;
                    elsif (cmd_in or x"0000" )= x"000A" then        -- set_dbg_LED,
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_dbg_LED;
                    elsif (cmd_in or x"0000" )= x"000B" then        -- set_pttrn_trig
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_pttrn_trig;
                    elsif (cmd_in or x"0000" )= x"000C" then        -- set_ext_trigg_dir
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_ext_trigg_dir;
                    elsif (cmd_in or x"000F" )= x"002F" then        -- set_fifo_freq
                        data_to_receive  <= 2;
                        sngle_word_instr <= False;
                        last_instruction <= set_fifo_freq;
                    elsif (cmd_in or x"000F" )= x"003F" then        -- set_fifo_opt
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_fifo_opt;
                    elsif (cmd_in or x"000F" )= x"004F" then        -- get_fifo_elts,
                        data_to_receive  <= 0;
                        sngle_word_instr <= True;
                        last_instruction <= get_fifo_elts;
                    elsif (cmd_in or x"000F" )= x"005F" then        -- set_pttrn_lngth,
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_pttrn_lngth;
                    elsif (cmd_in or x"000F" )= x"006F" then        -- set_chan_src
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_chan_src;
                    elsif (cmd_in or x"000F" )= x"007F" then        -- set_pttrn_frq
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_pttrn_freq;
                    elsif (cmd_in or x"001F" )= x"009F" then        -- set_sine_frq
                        data_to_receive  <= 2;
                        sngle_word_instr <= False;
                        last_instruction <= set_sine_freq;
                    elsif (cmd_in or x"001F" )= x"00BF" then        -- set_sine_ampl
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_sine_ampl;
                    elsif (cmd_in or x"001F" )= x"00DF" then        -- set_sine_periods
                        data_to_receive  <= 2;
                        sngle_word_instr <= False;
                        last_instruction <= set_sine_periods;
                    --elsif (cmd_in or x"001F" )= x"00FF" then        -- set_sine_time not implemented yet
                    --    --data_to_receive <= 1;
                    --    --last_instruction <= set_sine_time;
                    --    data_to_receive  <= 0;
                    --    sngle_word_instr <= True;
                    --    last_instruction <= unknown;
                    elsif (cmd_in or x"000F" )= x"00EF" then        -- set_chan_opts
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_chan_opts;
                    elsif (cmd_in or x"001F" )= x"011F" then        -- set_sine_opt
                        data_to_receive  <= 1;
                        sngle_word_instr <= False;
                        last_instruction <= set_sine_opt;
                    elsif (cmd_in or x"003F" )= x"017F" then        -- set_IF_mode
                        data_to_receive  <= 1;
                        sngle_word_instr <= True;
                        last_instruction <= set_IF_mode;
                    elsif (cmd_in or x"0007" )= x"0187" then        -- set_size_mult
                        data_to_receive  <= 0;
                        sngle_word_instr <= True;
                        last_instruction <= set_size_mult;
                    elsif (cmd_in or x"00FF" )= x"03FF" then        -- trig_chan
                        data_to_receive  <= 0;
                        sngle_word_instr <= True;
                        last_instruction <= trig_chan;
                    elsif (cmd_in or x"7FFF" )= x"FFFF" then        -- feed_source
                        case packet_size_mlt is
                            when 0 => data_to_receive <= to_integer(unsigned(cmd_in(14 downto 8))) + 1;
                            when 1 => data_to_receive <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*2;
                            when 2 => data_to_receive <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*4;
                            when 3 => data_to_receive <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*8;
                        end case;
                        case cmd_in(7 downto 4) is
                            when "0000" => case cmd_in(3 downto 0) is
                                                when "0000" => last_instruction <= feed_spiflash_rb;
                                                when "0001" => last_instruction <= feed_spiflash_norb;
                                                when "0010" => last_instruction <= feed_spiflash_lsb_rb;
                                                when "0011" => last_instruction <= feed_spiflash_lsb_norb;
                                                when others => last_instruction <= unknown;
                                           end case ;
                            when "0001" => last_instruction <= feed_pattern_source;
                            when "0010" => last_instruction <= feed_fifo_source;
                            when others => last_instruction <= unknown;
                        end case;
                        sngle_word_instr <= False;
                    else
                        data_to_receive  <= 0;     -- unknown command
                        sngle_word_instr <= True;
                        last_instruction <= unknown;
                    end if;
                end if;
            else
                sngle_word_instr <= False;
            end if;
        end if;
    end process;


    -- this smaller process only use the decoding to indicate the status of the
    -- module : are we expecting an instruction word ? a data word ?
    -- is some processing being performed ?
    process(clk)
    begin
        if rising_edge(clk) then
            if data_to_send=0 or (data_to_send=1 and loc_fbck_en='1') then
                busy <= cmd_en;
            else
                busy <= '1';
            end if;
            if data_to_receive > 0 then
                exp_data <= '1';
            else
                exp_data <= '0';
            end if;
        end if;
    end process;


--    ####### ##    ## ####### ########     ######   #####  ######   #####  ###    ### #######
--    ##       ##  ##  ##         ##        ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    #######   ####   #######    ##        ######  ####### ######  ####### ## #### ## #######
--         ##    ##         ##    ##        ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--    #######    ##    #######    ##        ##      ##   ## ##   ## ##   ## ##      ## #######
    ------------------------------------------------------------------------------
    -- Here, we manage system parameters and debug things...
    ------------------------------------------------------------------------------
    process(clk)
        -- data_to_send represents the number of data words to send back. The commands ACK response is 
        -- included, so we should increase the data transfer by one.
    begin
        if rising_edge(clk) then
            if reset = '1' then
                data_to_send <= 0;
            elsif sngle_word_instr and last_instruction = read_id then
                data_to_send <= 2; -- ACK + 1 data
            elsif sngle_word_instr and last_instruction = read_info then
                data_to_send <= READ_ID_WORDS + 1; -- ACK + READ_ID_WORDS data
            elsif sngle_word_instr and last_instruction = get_fifo_elts and cmd_fifo_ok then
                data_to_send <= 2; -- ACK + fifo elts
            elsif last_data_received and last_instruction = feed_fifo_source and cmd_fifo_ok then
                data_to_send <= 2; -- ACK + fifo elts
            elsif last_data_received and last_instruction = dbg_readback then
                data_to_send <= to_integer(unsigned(msw))+1; -- we need to add 1 because the response flag will be considered in the count 
            elsif data_to_receive = 0 and cmd_en = '1' and cmd_in(15) = '1' and cmd_in(7 downto 2) = "000000" and cmd_in(0)='0'then
                -- SPI data with readback
                case packet_size_mlt is
                    when 0 => data_to_send <= to_integer(unsigned(cmd_in(14 downto 8))) + 1     +1;
                    when 1 => data_to_send <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*2 +1;
                    when 2 => data_to_send <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*4 +1;
                    when 3 => data_to_send <= (to_integer(unsigned(cmd_in(14 downto 8))) + 1)*8 +1;
                end case;
            elsif data_to_receive = 0 and cmd_en = '1' and cmd_in(15) = '1' and cmd_in(7 downto 2) = "000000" and cmd_in(0)='1'then
                -- SPI data without readback
                data_to_send <= 1;
            elsif loc_fbck_en = '1' and data_to_send > 0 then
                data_to_send <= data_to_send - 1;
            end if;
        end if;
    end process;

    -- local system reset
    -- FIXME : should check if still used !
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                loc_params.reset <= '0';
            elsif sngle_word_instr and last_instruction = reset_stim then
                loc_params.reset <= '1';
            end if;
        end if;
    end process;

    -- debug LEDs management
    -- LEDs are quite usefull for different low level purposes.
    -- after reset, a slow chase should be visible, this is convenient to test if
    -- the architecture has been properly loaded on the FPGA and if it is
    -- actually running (comes from xdc version mismatch in which reset was
    -- always enabled)
    -- as soon as a value has been assigned to the LEDs, there is no more
    -- animation, it is then possible to use them as proof of proper decoding
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                LED_anim_cnt  <= TWIST_BASE_CLK_FREQ-1;
            elsif LED_anim_cnt = 0 then
                LED_anim_cnt  <= TWIST_BASE_CLK_FREQ-1;
            else
                LED_anim_cnt  <= LED_anim_cnt-1;
            end if;
        end if;
    end process;

    loc_params.debug_LED <= LED_values;
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                LED_values <= x"01";
                LED_fixed  <= '0';
            elsif last_data_received and last_instruction = set_dbg_led and databyte1_zero then
                LED_values <= lsw(7 downto 0);
                LED_fixed  <= '1';
            elsif LED_anim_cnt = 0 and LED_fixed = '0' then
                LED_values <= LED_values(6 downto 0) & LED_values(7);
            end if;
        end if;
    end process;

    -- channel readback
    -- this part is particularly usefull to check the data actually asked to
    -- the DAC. its purpose is to compare whether signal issues are the
    -- consequence of bugs in the digital part or in the analog part
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                channel_to_readback <= 0;
                readback_freq       <= 0;
            elsif last_data_received and last_instruction = dbg_readback then
                channel_to_readback <= to_integer(unsigned(lsw(15 downto 8)));
                readback_freq       <= to_integer(unsigned(usw(15 downto 0)));
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                readback_timer      <= 0;
                sample_readback_stb <= '0';
            elsif last_data_received and last_instruction = dbg_readback and to_integer(unsigned(lsw(7 downto 0))) /= TWIST_TRIGGER_NUM then
                readback_timer      <= 0;
                sample_readback_stb <= '0';
            elsif readback_timer >= TWIST_DBGRBK_MAXCNT then
                readback_timer      <= readback_timer - TWIST_DBGRBK_MAXCNT + readback_freq;
                sample_readback_stb <= '1'; 
            else
                readback_timer      <= readback_timer + readback_freq;
                sample_readback_stb <= '0';
            end if;
        end if;
    end process;

    -- submicrosecond counter
    -- this module actually counts the number of clock cycles elapsed since the
    -- beginning of the current microsecond
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                submicrosecond_counter <= 0;
            elsif submicrosecond_counter < TWIST_BASE_CLK_FREQ_MHZ-1 then
                submicrosecond_counter <= submicrosecond_counter + 1;
            else
                submicrosecond_counter <= 0;                
            end if;
        end if;
    end process;

    loc_params.sub_us_cnt <= submicrosecond_counter;


--    ######   #####  ######## ######## ####### ######  ###    ##      ######   #####  ######   #####  ###    ### #######
--    ##   ## ##   ##    ##       ##    ##      ##   ## ####   ##      ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    ######  #######    ##       ##    #####   ######  ## ##  ##      ######  ####### ######  ####### ## #### ## #######
--    ##      ##   ##    ##       ##    ##      ##   ## ##  ## ##      ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--    ##      ##   ##    ##       ##    ####### ##   ## ##   ####      ##      ##   ## ##   ## ##   ## ##      ## #######
    ------------------------------------------------------------------------------
    -- Here, we manage parameters for patterns
    ------------------------------------------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(i).length <= 0;
                elsif last_data_received and last_instruction = set_pttrn_lngth and i=prev_cmd_i4 then
                    if to_integer(unsigned(lsw)) <= TWIST_PATTERN_MAXLENGTH then
                        loc_params.patterns(i).length <= to_integer(unsigned(lsw));
                    end if;
                end if;
            end loop;            
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(i).freq   <= (others => '0');
                elsif last_data_received and last_instruction = set_pttrn_freq and i=prev_cmd_i4 then
                    loc_params.patterns(i).freq   <= unsigned(lsw);
                end if;
            end loop;            
        end if;
    end process;


    -- pattern memory management
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(i).data  <= (others => '0');
                elsif data_just_received then 
                    loc_params.patterns(i).data  <= signed(lsw);
                end if;
            end loop;

            for i in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(i).write <= '1';
                elsif data_just_received and i=prev_cmd_i4 and last_instruction = feed_pattern_source then    -- receiving data
                    loc_params.patterns(i).write <= '1';
                else
                    loc_params.patterns(i).write <= '0';
                end if;
            end loop;            
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(i).addr <= 0;
                elsif last_data_received and last_instruction = set_pttrn_lngth and i=prev_cmd_i4 then
                    loc_params.patterns(i).addr <= 0;
                elsif loc_params.patterns(i).write = '1' and prev_cmd(15) = '1' then        -- we test prev_cmd to avoid incrementing after reset
                    loc_params.patterns(i).addr <= loc_params.patterns(i).addr + 1;
                end if;
            end loop;            
        end if;
    end process;


    -- connections between sources and channels
    process(clk)
    begin
        if rising_edge(clk) then
            for patt in 0 to TWIST_PATTERNS-1 loop
                if reset = '1' then
                    loc_params.patterns(patt).chan_mask <= (TWIST_STIM_CHANNELS - 1 downto 0 => '0');
                elsif last_data_received and last_instruction = set_chan_src and datb0_pttrn_ok then
                    if cmd_channel_ok and prev_dat_i4=patt then
                        loc_params.patterns(patt).chan_mask(prev_cmd_i4) <= cmd_in(15);
                    end if;
                end if;
            end loop;
        end if;
    end process;

    -- trigger activation
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_PATTERNS - 1 loop
                if loc_params.patterns(i).trigg_num = TWIST_TRIGGER_NUM then
                    loc_params.patterns(i).trigg <= '0';
                else
                    loc_params.patterns(i).trigg <= local_triggers(loc_params.patterns(i).trigg_num);
                end if;
            end loop;
        end if;
    end process;

    -- trigger selection
    process(clk)
    begin
        if rising_edge(clk) then
            for patt in 0 to TWIST_PATTERNS - 1 loop
                if reset = '1' then
                    loc_params.patterns(patt).trigg_num <= TWIST_TRIGGER_NUM;
                elsif last_data_received and last_instruction = set_pttrn_trig and patt=prev_dat_i8u and datb0_trigg_ok then
                    loc_params.patterns(patt).trigg_num <= prev_dat_i8;
                end if;
            end loop;
        end if;
    end process;


--    ####### ## ###    ## #######     ######   #####  ######   #####  ###    ### #######
--    ##      ## ####   ## ##          ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    ####### ## ## ##  ## #####       ######  ####### ######  ####### ## #### ## #######
--         ## ## ##  ## ## ##          ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--    ####### ## ##   #### #######     ##      ##   ## ##   ## ##   ## ##      ## #######
    ------------------------------------------------------------------------------
    -- Here, we manage parameters for sine generators
    ------------------------------------------------------------------------------
    --amplitude loading
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_SINES-1 loop
                loc_params.sines(i).amplitude <= signed(lsw);
                loc_params.sines(i).load_a <=  last_data_received and last_instruction = set_sine_ampl
                                           and i=prev_cmd_i5 and reset = '0';
            end loop;            
        end if;
    end process;

    --frequence loading
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_SINES-1 loop
                if reset = '1' then
                    loc_params.sines(i).freq <= (others => '0');
                elsif last_data_received and last_instruction = set_sine_freq and i=prev_cmd_i5 and sine_freq_ok then
                    loc_params.sines(i).freq <= unsigned(msw & lsw);
                end if;
            end loop;            
        end if;
    end process;

    --periods limit loading
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_SINES-1 loop
                if reset = '1' then
                    loc_params.sines(i).periods <= (others => '0');
                elsif last_data_received and last_instruction = set_sine_periods and i=prev_cmd_i5 then
                    loc_params.sines(i).periods <= unsigned(msw & cmd_in);
                end if;
            end loop;            
        end if;
    end process;

    --opt parameters
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_SINES-1 loop
                if reset = '1' then
                    loc_params.sines(i).active    <= False;
                    loc_params.sines(i).param_w   <= False;
                    loc_params.sines(i).trigg_num <= TWIST_TRIGGER_NUM;
                elsif last_data_received and last_instruction = set_sine_opt and i=prev_cmd_i5 and datb0_trigg_ok then
                    loc_params.sines(i).active    <= lsw(15) = '1';
                    loc_params.sines(i).param_w   <= lsw(14) = '1';
                    loc_params.sines(i).trigg_num <= prev_dat_i8;
                end if;
            end loop;            
        end if;
    end process;


    -- trigger activation
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_SINES - 1 loop
                if loc_params.sines(i).trigg_num = TWIST_TRIGGER_NUM then
                    loc_params.sines(i).trigg <= '0';
                else
                    loc_params.sines(i).trigg <= local_triggers(loc_params.sines(i).trigg_num);
                end if;
            end loop;
        end if;
    end process;


    -- connections between sources and channels
    process(clk)
    begin
        if rising_edge(clk) then
            for sine in 0 to TWIST_SINES-1 loop
                if reset = '1' then
                    loc_params.sines(sine).chan_mask <= (TWIST_STIM_CHANNELS - 1 downto 0 => '0');
                elsif last_data_received and last_instruction = set_chan_src and datb0_sine_ok and cmd_channel_ok and prev_dat_i5=sine then
                    loc_params.sines(sine).chan_mask(prev_cmd_i5) <= lsw(15);
                end if;
            end loop;
        end if;
    end process;


--    ####### ## #######  ######      ######   #####  ######   #####  ###    ### #######
--    ##      ## ##      ##    ##     ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    #####   ## #####   ##    ##     ######  ####### ######  ####### ## #### ## #######
--    ##      ## ##      ##    ##     ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--    ##      ## ##       ######      ##      ##   ## ##   ## ##   ## ##      ## #######
    ------------------------------------------------------------------------------
    -- FIFO parameters
    ------------------------------------------------------------------------------
    --frequence loading
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_FIFOS-1 loop
                if reset = '1' then
                    loc_params.fifos(i).freq <= 0;
                elsif last_data_received and last_instruction = set_fifo_freq and i=prev_cmd_i4 and fifo_freq_ok then
                    loc_params.fifos(i).freq <= to_integer(unsigned(msw & lsw));
                end if;
            end loop;            
        end if;
    end process;

    --opt parameters
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_FIFOS-1 loop
                if reset = '1' then
                    loc_params.fifos(i).enabled    <= False;
                    loc_params.fifos(i).interplt   <= False;
                    loc_params.fifos(i).dofe       <= False;
                    loc_params.fifos(i).trigg_num  <= TWIST_TRIGGER_NUM;
                elsif last_data_received and last_instruction = set_fifo_opt and i=prev_cmd_i4 and datb0_trigg_ok then
                    loc_params.fifos(i).enabled    <= lsw(15) = '1';
                    loc_params.fifos(i).interplt   <= lsw(14) = '1';
                    loc_params.fifos(i).dofe       <= lsw(13) = '1';
                    loc_params.fifos(i).trigg_num  <= prev_dat_i8;
                elsif local_triggers(loc_params.fifos(i).trigg_num) = '1' then
                    loc_params.fifos(i).enabled    <= True;
                elsif status.fifos(i).empty = '1' and loc_params.fifos(i).dofe then
                    loc_params.fifos(i).enabled    <= False;
                end if;
            end loop;            
        end if;
    end process;



    -- fifo data reception
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_FIFOS-1 loop
                if reset = '1' then
                    loc_params.fifos(i).sample  <= (others => '0');
                elsif data_just_received then 
                    loc_params.fifos(i).sample  <= signed(lsw);
                end if;

                if reset = '1' then
                    loc_params.fifos(i).sample_en <= '0';
                elsif data_just_received and i=prev_cmd_i4 and last_instruction = feed_fifo_source then    -- receiving data
                    loc_params.fifos(i).sample_en <= '1';
                else
                    loc_params.fifos(i).sample_en <= '0';
                end if;
            end loop;
            if reset = '1' then
                fifonodataloss <= True;
            elsif cmd_en = '1' and data_to_receive = 0 and cmd_in(15)='1' then  -- when we receive a new bunch of data
                fifonodataloss <= True;
            elsif loc_params.fifos(prev_cmd_i4).sample_en = '1' and status.fifos(prev_cmd_i4).full = '1' then
                fifonodataloss <= False;
            end if;            
        end if;
    end process;


    -- connections between sources and channels
    process(clk)
    begin
        if rising_edge(clk) then
            for fifo in 0 to TWIST_FIFOS-1 loop
                if reset = '1' then
                    loc_params.fifos(fifo).chan_mask <= (TWIST_STIM_CHANNELS - 1 downto 0 => '0');
                elsif last_data_received and last_instruction = set_chan_src and datb0_fifo_ok and cmd_channel_ok and prev_dat_i4=fifo then
                    loc_params.fifos(fifo).chan_mask(prev_cmd_i4) <= lsw(15);
                end if;
            end loop;
        end if;
    end process;


--     ###### ##   ##  #####  ###    ## ###    ## ####### ##          ######   #####  ######   #####  ###    ### #######
--    ##      ##   ## ##   ## ####   ## ####   ## ##      ##          ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    ##      ####### ####### ## ##  ## ## ##  ## #####   ##          ######  ####### ######  ####### ## #### ## #######
--    ##      ##   ## ##   ## ##  ## ## ##  ## ## ##      ##          ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--     ###### ##   ## ##   ## ##   #### ##   #### ####### #######     ##      ##   ## ##   ## ##   ## ##      ## #######
------------------------------------------------------------------------------
--    channel specific parameter definitions.
------------------------------------------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            for chan in 0 to TWIST_STIM_CHANNELS-1 loop
                if reset = '1' then
                    loc_params.chans(chan).PWM_bits     <= TWIST_DEFAULT_DAC_PWMBITS;
                    loc_params.chans(chan).DAC_freq     <= TWIST_DEFAULT_DAC_FREQ_MHZ;
                    loc_params.chans(chan).inverted     <= False;
                    loc_params.chans(chan).combined     <= False;
                    loc_params.chans(chan).comp_current <= "00000";
                elsif last_data_received and last_instruction = set_chan_opts and datb0_chopt_ok and chan=prev_cmd_i4 then
                    loc_params.chans(chan).PWM_bits     <= to_integer(unsigned(lsw(9 downto 6)));
                    loc_params.chans(chan).DAC_freq     <= prev_dat_i6;
                    loc_params.chans(chan).inverted     <= lsw(15) = '1';
                    loc_params.chans(chan).combined     <= lsw(14 downto 10) /= "00000";
                    loc_params.chans(chan).comp_current <= lsw(14 downto 10);
                end if;
            end loop;
        end if;
    end process;



--    ######## ######  ##  ######   ######  ####### ######  #######
--       ##    ##   ## ## ##       ##       ##      ##   ## ##
--       ##    ######  ## ##   ### ##   ### #####   ######  #######
--       ##    ##   ## ## ##    ## ##    ## ##      ##   ##      ##
--       ##    ##   ## ##  ######   ######  ####### ##   ## #######
    ------------------------------------------------------------------------------
    -- trigger definitions.
    ------------------------------------------------------------------------------
    process(clk)
    -- FIXME: only work if TWIST_TRIGGER_NUM <= 8 !!
    begin
        if rising_edge(clk) then
            for trigg in 0 to TWIST_TRIGGER_NUM-1 loop
                if sngle_word_instr and last_instruction = trig_chan then
                    pending_triggers(trigg) <= prev_cmd(trigg);
                elsif last_data_received and last_instruction = dbg_readback and trigg = to_integer(unsigned(lsw(7 downto 0))) then
                    pending_triggers(trigg) <= '1';
                elsif ext_trig_valid(trigg) = '1' then
                    pending_triggers(trigg) <= '1';
                elsif submicrosecond_counter = 0 then
                    if trigg_decnt(trigg) = 1 then
                        pending_triggers(trigg) <= '1';
                    else
                        pending_triggers(trigg) <= '0';
                    end if;
                end if;
            end loop;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset='1' then
                ext_trigg_dir <= (others => '0');
            elsif last_data_received and last_instruction = set_ext_trigg_dir then
                ext_trigg_dir <= lsw(TWIST_EXT_TRIGGS-1 downto 0);
            end if;
        end if; 
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                local_triggers <= (others => '0');
            elsif submicrosecond_counter = 0 then
                local_triggers <= pending_triggers;
            else
                local_triggers <= (others => '0');
            end if;
        end if;
    end process;
    
    process(clk) -- this process manages external triggers and their counter
    begin
        if rising_edge(clk) then
            for i in 0 to TWIST_EXT_TRIGGS-1 loop
                if reset = '1' then
                    ext_trigg_cnt(i) <= 0;
                elsif pending_triggers(i) = '1' and submicrosecond_counter = 0 then
                    ext_trigg_cnt(i) <= TWIST_TRIGGSYNC_LENGTH;
                elsif ext_trigg_cnt(i) > 0 then
                    ext_trigg_cnt(i) <= ext_trigg_cnt(i) - 1;
                end if;
                
                if ext_trigg_dir(i) = '0' then
                    ext_triggers(i) <= 'Z';
                elsif ext_trigg_cnt(i) = 0 then
                    ext_triggers(i) <= '0';
                else
                    ext_triggers(i) <= '1';
                end if;
            end loop;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            synced_ext_trig <= ext_triggers;
            old_ext_trig    <= synced_ext_trig;
            for i in 0 to TWIST_TRIGGER_NUM - 1 loop
                if i >= TWIST_EXT_TRIGGS then
                    ext_trig_valid(i) <= '0';
                else
                    ext_trig_valid(i) <= not old_ext_trig(i) and synced_ext_trig(i) and not ext_trigg_dir(i);
                end if; 
            end loop;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            for trigg in 0 to TWIST_TRIGGER_NUM-1 loop
                if reset = '1' then
                    trigg_decnt(trigg)  <= x"00000000";
                elsif submicrosecond_counter = 0 and trigg_decnt(trigg) /= 0 then
                    trigg_decnt(trigg)  <= trigg_decnt(trigg) - 1;
                elsif local_triggers(trigg_father(trigg)) = '1' then
                    trigg_decnt(trigg)  <= trigg_dly_time(trigg);
                end if;
            end loop;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            for trigg in 0 to TWIST_TRIGGER_NUM-1 loop
                if reset = '1' then
                    trigg_dly_time(trigg)  <= x"00000000";
                    trigg_father(trigg)    <= 0;
                elsif last_data_received and last_instruction = chain_trigg and datb0_trigg_ok and datb1_trigg_ok and prev_dat_i8=trigg then
                    trigg_dly_time(trigg)  <= unsigned(usw & msw);
                    trigg_father(trigg)    <= prev_dat_i8u;
                end if;
            end loop;
        end if;
    end process;


--    ####### ##   ## ########     ## #######     ######   #####  ######   #####  ###    ### #######
--    ##       ## ##     ##        ## ##          ##   ## ##   ## ##   ## ##   ## ####  #### ##
--    #####     ###      ##        ## #####       ######  ####### ######  ####### ## #### ## #######
--    ##       ## ##     ##        ## ##          ##      ##   ## ##   ## ##   ## ##  ##  ##      ##
--    ####### ##   ##    ##        ## ##          ##      ##   ## ##   ## ##   ## ##      ## #######
    ------------------------------------------------------------------------------
    -- interface parameters
    ------------------------------------------------------------------------------

    process(clk)    -- data size multiplier
    begin
        if rising_edge(clk) then
            if reset = '1' then
                packet_size_mlt <= 0;
            elsif sngle_word_instr and last_instruction = set_size_mult and cmd_pcktsizemlt_ok then
                packet_size_mlt <= to_integer(unsigned(prev_cmd(2 downto 0)));
            end if;
        end if;
    end process;



    process(clk)    -- interface speed
    begin
        if rising_edge(clk) then
            if reset = '1' then
                loc_params.interface.req_speed <= TWIST_HWVAR_UART_INITFREQ/1600;
            elsif status.hw_interface.rst_IF = '1' then
                loc_params.interface.req_speed <= TWIST_HWVAR_UART_INITFREQ/1600;
            elsif last_data_received and last_instruction = set_IF_speed and data_freq_ok then
                loc_params.interface.req_speed <= to_integer(unsigned(lsw));
            end if;
        end if;
    end process;

    process(clk)    -- interface mode
    begin
        if rising_edge(clk) then
            if reset = '1' then
                loc_params.interface.req_mode <= "000";
                loc_params.interface.HS_IFs   <= 0;
            elsif status.hw_interface.rst_IF = '1' then
                loc_params.interface.req_mode <= "000";
                loc_params.interface.HS_IFs   <= 0;
            elsif sngle_word_instr and last_instruction = set_IF_mode and cmd_HS_num then
                loc_params.interface.req_mode <= prev_cmd(2 downto 0);
                loc_params.interface.HS_IFs   <= to_integer(unsigned(prev_cmd(5 downto 3)));
            elsif sngle_word_instr and last_instruction = set_IF_mode16 then
                loc_params.interface.req_mode <= "001";
                loc_params.interface.HS_IFs   <= TWIST_HS_INPUTS;
            end if;
        end if;
    end process;

--    ####### ##       #####  ####### ##   ##     ####### ######  ##      ###### ######## ######  ##
--    ##      ##      ##   ## ##      ##   ##     ##      ##   ## ##     ##         ##    ##   ## ##
--    #####   ##      ####### ####### #######     ####### ######  ##     ##         ##    ######  ##
--    ##      ##      ##   ##      ## ##   ##          ## ##      ##     ##         ##    ##   ## ##
--    ##      ####### ##   ## ####### ##   ##     ####### ##      ##      ######    ##    ##   ## #######
    ------------------------------------------------------------------------------
    -- management of spi FLASH for system arch and parameters updates
    ------------------------------------------------------------------------------

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                loc_params.flashdat.data_to_flash <= x"0000";
                loc_params.flashdat.data_flash_en <= '0';
                loc_params.flashdat.instr_conted  <= '0';
                loc_params.flashdat.single_byte   <= '0';
            elsif (last_instruction = feed_spiflash_rb or last_instruction = feed_spiflash_norb) and (data_to_receive>0) then
                loc_params.flashdat.instr_conted  <= '1';
                loc_params.flashdat.data_to_flash <= cmd_in;
                loc_params.flashdat.data_flash_en <= cmd_en;
                loc_params.flashdat.single_byte   <= '0';
            elsif (last_instruction = feed_spiflash_lsb_rb or last_instruction = feed_spiflash_lsb_norb) and (data_to_receive>0) then
                loc_params.flashdat.instr_conted  <= '1';
                loc_params.flashdat.data_to_flash <= cmd_in(7 downto 0) & x"00";
                loc_params.flashdat.data_flash_en <= cmd_en;
                loc_params.flashdat.single_byte   <= '1';
            else
                loc_params.flashdat.instr_conted  <= '0';
                loc_params.flashdat.data_flash_en <= '0';
            end if;
        end if;
    end process;



end Behavioral;
