#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: bornat

This code is the API for the TWIST board
This is the first usable version and is provided with system 1.2
It can still wirk with earlier system preversions, but update is
higly encouraged.

"""

SOFTWARE_VERSION = "1.0.0"

SUPPORTED_VERSIONS = {0x0001 : "september 2020",
                      0x0100 : "september 2020",
                      0x0101 : "september 2020",
                      0x0102 : None,
                      0x0103 : None}

FILE_WRITE_SUFFIX = "_WRITE.txt"
FILE_FBCK_SUFFIX  = "_FBCK.txt"
FILE_VHD_SUFFIX   = "_VHD-TB.txt"

RESP_OK       = 0x8001
RESP_UNKW_CMD = 0x8002
RESP_INV_DAT  = 0x8004
RESP_DAT_LOST = 0x8008

EXT_TRIGG_INPUT  = 0
EXT_TRIGG_OUTPUT = 1



ID_Nexys4_1    = 'USB VID:PID=0403:6010 SER=210274663103B'
ID_Nexys4_3    = 'USB VID:PID=0403:6010 SER=210274663100B'
ID_Nexys4_4    = 'USB VID:PID=0403:6010 SER=210274628301B'
ID_Nexys4DDR_1 = 'USB VID:PID=0403:6010 SER=210292743230B'
ID_Nexys4DDR_2 = 'USB VID:PID=0403:6010 SER=210292745552B'
ID_Nexys4DDR_3 = 'USB VID:PID=0403:6010 SER=210292745577B'
ID_Nexys4DDR_u = 'USB VID:PID=0403:6010 SER=210292743233B'
ID_TWIST_0     = 'USB VID:PID=0403:6010 SER=FTL4OQGB'      # uses SONIC s6
ID_TWIST_1     = 'USB VID:PID=0403:6010 SER=FT4082NKA'     # uses SONIC s1
ID_TWIST_2     = 'USB VID:PID=0403:6010 SER=FT3JF4RWA'     # uses SONIC s?
ID_TWIST_3     = 'USB VID:PID=0403:6010 SER=FT466ZFLA'     # uses SONIC s3
ID_TWIST_4     = 'USB VID:PID=0403:6010 SER=FT407ZLWA'     # uses SONIC s2
ID_TWIST_5     = 'USB VID:PID=0403:6010 SER=FT4081KSA'     # uses SONIC s5


ID_any         = [ID_TWIST_0, ID_TWIST_1, ID_TWIST_2, ID_TWIST_3, ID_TWIST_4, ID_TWIST_5, ID_Nexys4_1, ID_Nexys4_3, ID_Nexys4_4, ID_Nexys4DDR_1, ID_Nexys4DDR_2, ID_Nexys4DDR_3, ID_Nexys4DDR_u]

Boardnames = { ID_TWIST_0:     'TWIST 0',
               ID_TWIST_1:     'TWIST 1',
               ID_TWIST_2:     'TWIST 2',
               ID_TWIST_3:     'TWIST 3',
               ID_TWIST_4:     'TWIST 4',
               ID_TWIST_5:     'TWIST 5',
               ID_Nexys4_1:    'Nexys4 1',
               ID_Nexys4_3:    'Nexys4 3',
               ID_Nexys4_4:    'Nexys4 4',
               ID_Nexys4DDR_1: 'Nexys4DDR 1',
               ID_Nexys4DDR_2: 'Nexys4DDR 2',
               ID_Nexys4DDR_3: 'Nexys4DDR 3',
               ID_Nexys4DDR_u: 'Nexys4DDR u'}

# these are the compensation current values in function of the parameter
comb_comp_currents = [0.0,               0.003906369212928, 0.007812738425855, 0.010010071108127,
                      0.01122914160055,  0.012596675860058, 0.014130754457276, 0.015851659894257,
                      0.01778214476537,  0.019947732576016, 0.022377055196354, 0.02510223141164,
                      0.028159291574086, 0.031588653970686, 0.03543565920522,  0.039751169659643,
                      0.04459224196052,  0.050022881341385, 0.05611488787466,  0.062948805761425,
                      0.070614988229871, 0.079214792121767, 0.088861917960929, 0.099683913221115,
                      0.1118238586685,   0.125442260074356, 0.140719170308823, 0.157856569872593,
                      0.177081037340216, 0.198646745021736, 0.222838819449185, 0.249977111117893]

import serial
from serial.tools import list_ports as list_serial_ports
from time import sleep as sleep
from os import path as path
from sys import platform as platform


class TWIST_instance(object):
    supported_versions = SUPPORTED_VERSIONS
    dbgmsg_tab          = 80
    max_out_milliamps   = 1.0
    current_unit        = 1.0     # if 0.001  interface talks in Ampers
                                  # if 1.0,   interface talks in milliAmps
                                  # if 1000.0 interface talks in microAmps
    current_unit_string = "mA"
    def __init__(self, ID=ID_any, debug=False, hw_debug=False, no_ver_warn=False, fake_filename="", baudrate = 921600):
        """
        opens the serial interface to the hardware board identified by ID.
        ID is the hardware description string of the serial interface. If ID is
        a list, any of the devices in the list is opened (whichever is detected
        first). If no ID provided, considers any known system
        if verbose is true, displays lots of debugging messages, considered
        false if omitted
        """
        self.debug       = debug
        self.hwdbg       = hw_debug
        self.no_ver_warn = no_ver_warn
        self.portname    = ""
        self.baudrate    = baudrate
        if isinstance(ID, str):
            # OK, let's get a list in all cases
            ID = [ID]
        for p in list_serial_ports.comports():
            #we loop on all serial devices, we display the device curretly process
            self.dbg_print('Found device:')
            for i in range(3):
                self.dbg_print('    ' + p[i])

            # we adapt the board identifier according the operation system
            if platform == 'darwin':
                sys_identifier = p[2][:p[2].find(" LOCATION=")]
                if p[0][-1]=='0':
                    sys_identifier += "A"
                elif p[0][-1]=='1':
                    sys_identifier += "B"
                print(sys_identifier)                    
            elif platform == "linux":
                sys_identifier = p[2][:p[2].find(" LOCATION=")]
                if p[2][-3:] == "1.0":
                    sys_identifier += "A"
                elif p[2][-3:] == "1.1":
                    sys_identifier += "B"
            else:
                sys_identifier = p[2]

            # finally, we check if the device is in the available devices
            if (sys_identifier in ID) or (p[0] in ID): 
                self.dbg_print("Peripheral found on %s" %p[0])
                self.portname = p[0]
                #self.serial   = serial.Serial(p[0], baudrate = 115200)
                self.serial   = serial.Serial(p[0], baudrate = self.baudrate)
                if p[0] in ID:
                    self.board = p[0]
                else:
                    self.board = Boardnames[sys_identifier]
                break
        if self.portname == "":
            #if fake_filename == "":
            raise Exception("Could not find stimulation device")
            #self.serial   = TWIST_fake_instance(fake_filename)
        self.last_err = ""
        self.pending  = 0
        self.serial.read(self.serial.inWaiting())
        self.dbg_print("Establishing contact with board ... ")
        bytecount = 0
        while (True):
            self.write(chr(0))
            bytecount += 1
            sleep(0.1)
            if self.serial.inWaiting() >0:
                break
        if bytecount>2:
            self.dbg_print("Had to send unusually high amout NOPs (" + str(bytecount-2) + ')')
        self.get_ACK()
        self.chkinfo()
        self.reset()
        self.set_dbgLED(0)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()
        return False    

    def close(self):
        self.serial.close()
        
    def write(self, ustring):
        #self.serial.write(ustring.encode(encoding='latin_1'))
        self.serial.write(bytearray(ustring, encoding='latin_1'))
    
    def dbg_print(self, msg, cont=False, dbgtab=False):
        if self.debug:
            if dbgtab:
                print ((msg + (' '*self.dbgmsg_tab))[:self.dbgmsg_tab], end="")
            elif cont:
                print (msg, end="")
            else:
                print (msg)
    def get_ACK(self):
        """ analyses the response received and stores info ...
            returns False when no error was produced, otherwise True
        """
        R=self.serial.read(2)
        resp=R[0]*256 + R[1]
        if resp < 2**15:
            self.dbg_print( "Invalid ACK return code : " + hex(resp))
            self.last_err = "No ACK"
            return True
        resp = resp - 2**15
        pending = (resp - (resp%256))//256
        resp = resp%256
        if resp == 1:
            self.pending = pending
            return False
        self.dbg_print("ERROR")
        self.pending = 0
        if pending>0:
            self.dbg_print("Should not receive data with errors (" + str(pending) + " words expected)")
        if resp == 2:
            self.dbg_print("Unknown Command")
            self.last_err = "Unknown Command"
            return True
        if resp == 4:
            self.dbg_print("Invalid Data")
            self.last_err = "Invalid Data"
            return True
        if resp == 8:
            self.dbg_print("Data Lost")
            self.last_err = "Data Lost"
            return True
        self.dbg_print("Unexpected Resp." + str(hex(R[0]*256 + R[1])))
        self.last_err = "Unexpected Resp."
        return True
    
    def check_no_resp_yet(self):
        if not self.hwdbg:
            return
        sleep(0.1)
        if self.serial.inWaiting() >0:
            self.dbg_print("ERROR")
            written = 0
            for i in range(self.serial.inWaiting()):
                print(("0" + hex(ord(self.serial.read(1)))[2:])[-2:], end="")
                written += 1
                if written>=16:
                    print(" ")
                    written = 0
            raise Exception("System should not respond so fast")
    
    def amplitude_transcode(self, ampl, signed=True):
        if ampl>self.max_out_milliamps*self.current_unit:
            raise Exception(str(ampl) + ": Current value too high")
        if ampl<0 and not signed:
            raise Exception(str(ampl) + ": Negative current value not allowed")
        if ampl<-self.max_out_milliamps*self.current_unit:
            raise Exception(str(ampl) + ": Current value too low")
        ratio = ampl/(self.max_out_milliamps*self.current_unit)
        if signed:
            num = int(ratio * 32640)
            if num<0:
                num += 65536
        else:
            num = int(ratio * 65280)
        return num
    
    def get_data(self):
        for i in range(self.pending):
            R = self.serial.read(2)
            yield R[0]*256 + R[1]
    
    def chkinfo(self):
        self.dbg_print("Checking board version ...", dbgtab=True)
        self.write("\x00\x01")  # READ ID command
        if self.get_ACK():
            raise Exception(self.last_err)
        self.BOARD_PROJECT = ord(self.serial.read(1))
        self.BOARD_VERSION = ord(self.serial.read(1))
        if self.pending != 1:
            print(self.pending)
            raise Exception("Unexpected amount of data to receive")
        self.BOARD_VER_ID = self.BOARD_PROJECT*256 + self.BOARD_VERSION
        if self.BOARD_VER_ID not in self.supported_versions:
            raise Exception("Unsupported version " + str(self.BOARD_PROJECT) + ' / ' + str(self.BOARD_VERSION))
        self.dbg_print("OK")
        self.dbg_print("Version is " + str(self.BOARD_PROJECT) + ' / ' + str(self.BOARD_VERSION))
        if self.supported_versions[self.BOARD_VER_ID]!=None and not self.no_ver_warn:
            print("#"*70)
            print("WARNING: deprecated version")
            print("Hardware TWIST version is deprecated, support will end in " + self.supported_versions[self.BOARD_VER_ID])
            print("This is most probably the consequence of a critical bug fix")
            print("After this date, script updates will not be able to control the BOARD")
            print("If you wish to go on with this hardware version pay attention to save")
            print("your software in a secure place...")
            print("You may ignore this warning setting 'no_ver_warn' to True while calling")
            print("TWIST_instance to connect")
            print("#"*70)
        
        self.dbg_print("Reading board capabilities ...", dbgtab=True)
        self.write("\x00\x03")
        if self.get_ACK():
            raise Exception(self.last_err)
        if self.BOARD_VER_ID in [0x0001, 0x0100, 0x0101]:
            #BACKPORT:to remove in sept 2020
            if self.pending != 7:
                print(self.pending)
                raise Exception("Unexpected amount of data to receive")
            l = list(self.get_data())
            #l[0] is version and project ID, should have already been checked
            self.TWIST_STIM_CHANNELS         = l[1]//256
            self.TWIST_TRIGGER_NUM           = l[1]%256
            self.TWIST_PATTERNS              = l[2]//256
            self.TWIST_SINES                 = l[2]%256
            self.TWIST_FIFOS                 = l[3]//256
            self.TWIST_HS_INPUTS             = l[4]//8192
            self.TWIST_PATTERN_MAXLENGTH     = l[4]%8192
            self.TWIST_DATAPACKET_MAXQUANTUM = l[5]//8192
            self.TWIST_READBACK_MAX_SAMPLES  = l[5]%8192
            self.TWIST_EXT_TRIGGERS          = l[6]//4096
            self.dbg_print("OK")
            self.dbg_print("+ Stimulation channels            : " + str(self.TWIST_STIM_CHANNELS))
            self.dbg_print("+ Triggers                        : " + str(self.TWIST_TRIGGER_NUM))
            self.dbg_print("+ Pattern players                 : " + str(self.TWIST_PATTERNS))
            self.dbg_print("+ Sine generators                 : " + str(self.TWIST_SINES))
            self.dbg_print("+ FIFO players                    : " + str(self.TWIST_FIFOS))
            self.dbg_print("+ High speed communication inputs : " + str(self.TWIST_HS_INPUTS))
            self.dbg_print("+ Max pattern Length              : " + str(self.TWIST_PATTERN_MAXLENGTH))
            self.dbg_print("+ Max data packet size            : " + str(128* (2**self.TWIST_DATAPACKET_MAXQUANTUM)))
            self.dbg_print("+ Max readback data size          : " + str(self.TWIST_READBACK_MAX_SAMPLES))
            self.dbg_print("+ External Trigger I/O            : " + str(self.TWIST_EXT_TRIGGERS))
        else:
            if self.pending != 8:
                print(self.pending)
                raise Exception("Unexpected amount of data to receive")
            l = list(self.get_data())
            #l[0] is version and project ID, should have already been checked
            self.TWIST_STIM_CHANNELS         = l[1]//256
            self.TWIST_TRIGGER_NUM           = l[1]%256
            self.TWIST_PATTERNS              = l[2]//256
            self.TWIST_SINES                 = l[2]%256
            self.TWIST_FIFOS                 = l[3]//256
            self.TWIST_PATTERN_MAXLENGTH     = l[4]
            self.TWIST_FIFO_DEPTH            = l[5]
            self.TWIST_DATAPACKET_MAXQUANTUM = l[6]//8192
            self.TWIST_READBACK_MAX_SAMPLES  = l[6]%8192
            self.TWIST_EXT_TRIGGERS          = l[7]//4096
            self.TWIST_HS_INPUTS             = (l[7]//512)%8

            self.dbg_print("OK")
            self.dbg_print("+ Stimulation channels            : " + str(self.TWIST_STIM_CHANNELS))
            self.dbg_print("+ Triggers                        : " + str(self.TWIST_TRIGGER_NUM))
            self.dbg_print("+ Pattern players                 : " + str(self.TWIST_PATTERNS))
            self.dbg_print("+ Sine generators                 : " + str(self.TWIST_SINES))
            self.dbg_print("+ FIFO players                    : " + str(self.TWIST_FIFOS))
            self.dbg_print("+ Max pattern Length              : " + str(self.TWIST_PATTERN_MAXLENGTH))
            self.dbg_print("+ Embedded FIFOs size (samples)   : " + str(self.TWIST_FIFO_DEPTH))
            self.dbg_print("+ Max data packet size            : " + str(128* (2**self.TWIST_DATAPACKET_MAXQUANTUM)))
            self.dbg_print("+ Max readback data size          : " + str(self.TWIST_READBACK_MAX_SAMPLES))
            self.dbg_print("+ External Trigger I/O            : " + str(self.TWIST_EXT_TRIGGERS))
            self.dbg_print("+ High speed communication inputs : " + str(self.TWIST_HS_INPUTS))
    
    def reset(self):
        self.dbg_print("Resetting system ...", dbgtab=True)
        self.write("\x00\x02")
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
        self.sizemlt = 0
        self.ext_trigg_dir = [EXT_TRIGG_INPUT]*self.TWIST_EXT_TRIGGERS
    
    def set_dbgLED(self, ledmap):
        self.dbg_print("Setting LEDs to " + hex(ledmap)[2:] + " ...", dbgtab=True)
        self.write("\x00\x0A")
        self.check_no_resp_yet()
        self.write("\x00" + chr(ledmap))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
    
    def set_HWIF_speed(self, baudrate):
        rounded = (baudrate + 799)//1600
        self.dbg_print("Setting HW UART speed to " + str(rounded*1600) + "bps ...", dbgtab=True)
        self.write("\x00\x04")
        self.check_no_resp_yet()
        self.write(chr(rounded//256) + chr(rounded%256))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_HWIF_mode(self, mode):
        modes = ["8bits UART", "16bits xUART", "16bits High speed xUART", "128bits High speed xUART", "SPI"]
        self.dbg_print("Setting HW interface mode to " + modes[mode] + " ...", dbgtab=True)
        self.write("\x01" + chr(mode + 0x40))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
    
    def set_chan_param(self, chan, freq=5, PWMbits=0, inverted=False, combined=False, comp_curr=0.1):
        if freq>20:
            raise Exception("DAC frequency too high")
        #if freq==0:
        #    raise Exception("DAC frequency can't be null")
        if PWMbits not in range(8):
            raise Exception("Invalid number of PWM bits")
        if combined:
            if chan%2==1:
                raise Exception("Cannot set odd channel as combined")
            if inverted:
                self.dbg_print("Setting combined chans " + str(chan) + "/" + str(chan+1) + "(inverted) at " + str(freq) + " MHz, " + str(PWMbits) + " extra bits", dbgtab=True)
            else:
                self.dbg_print("Setting combined chans " + str(chan) + "/" + str(chan+1) + " at " + str(freq) + " MHz, " + str(PWMbits) + " extra bits", dbgtab=True)
        elif inverted:
            self.dbg_print("Setting chan " + str(chan) + "(inverted) at " + str(freq) + " MHz, " + str(PWMbits) + " extra bits", dbgtab=True)
        else:
            self.dbg_print("Setting chan " + str(chan) + " at " + str(freq) + " MHz, " + str(PWMbits) + " extra bits", dbgtab=True)
        self.write("\x00" + chr(0xE0 + chan))
        if inverted:
            data = 0x8000 + int(freq)
        else:
            data = int(freq)
        if combined:
            #indx =  min(comb_comp_currents, key=lambda x:abs(x-comp_curr))
            dists = [abs(x-comp_curr) for x in comb_comp_currents]
            indx = dists.index(min(dists))
            data += indx<<10
        data += int(PWMbits) << 6
        self.write(chr(data//256) + chr(data%256))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_pattern_length(self, pattern, length):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        if length>self.TWIST_PATTERN_MAXLENGTH:
            raise Exception("Pattern too long")
        self.dbg_print("Setting length " + str(length) + " for pattern " + str(pattern) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x50 + pattern))
        self.check_no_resp_yet()
        self.write(chr(length//256) + chr(length%256))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_pattern_freq(self, pattern, freq):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        if self.BOARD_VER_ID < 0x103:
            argfreq = int((freq+49)/100)
            self.dbg_print("Setting sampling freq " + str(argfreq*100) + "Hz for pattern " + str(pattern) + " ...", dbgtab=True)
        else:
            argfreq = int((freq+249)/500)
            self.dbg_print("Setting sampling freq " + str(argfreq*500) + "Hz for pattern " + str(pattern) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x70 + pattern))
        self.check_no_resp_yet()
        self.write(chr(argfreq//256) + chr(argfreq%256))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def send_pattern_data(self, pattern, l):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        n = len(l)
        self.dbg_print("Sending " + str(n) + " samples for pattern " + str(pattern) + " ...", dbgtab=True)
        index = 0
        while n>0:
            if n>=128:
                self.write("\xFF" + chr(0x10 + pattern))
            else:
                self.write(chr(0x80 + n -1) + chr(0x10 + pattern))
            for i in range(min(n, 128)):
                if i==min(n, 128) - 1:
                    self.check_no_resp_yet()
                c = self.amplitude_transcode(l[index])
                if c<0:
                    c+=65536
                self.write(chr(c//256) + chr(c%256))
                index += 1
            if self.get_ACK():
                raise Exception(self.last_err)
            n-=128
        self.dbg_print("OK")
            
    def connect_pattern_to_channel(self, pattern, channel):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Connecting pattern " + str(pattern) + " to channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x80" + chr(0x10 + pattern))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def disconnect_pattern_from_channel(self, pattern, channel):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Disconnecting pattern " + str(pattern) + " from channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x00" + chr(0x10 + pattern))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
        
    def set_pattern_trigger(self, pattern, trigger ):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        if trigger>=self.TWIST_TRIGGER_NUM:
            raise Exception("Not so much triggers on the system")
        self.dbg_print("Set pattern " + str(pattern) + " listen to trigger " + str(trigger) + " ...", dbgtab=True)
        self.write("\x00\x0B")
        self.check_no_resp_yet()
        self.write(chr(pattern) + chr(trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def remove_pattern_trigger(self, pattern):
        if pattern>=self.TWIST_PATTERNS:
            raise Exception("Not so much patterns on the system")
        self.dbg_print("Removing trigger for pattern " + str(pattern) + " ...", dbgtab=True)
        self.write("\x00\x0B")
        self.check_no_resp_yet()
        self.write(chr(pattern) + chr(self.TWIST_TRIGGER_NUM))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def bitmap_trigg(self, trigs):
        if trigs>255:
            raise Exception("Cannot trigg more than 8 chans at a time")
        self.dbg_print("Activating triggers with mask " + ("0" + hex(trigs)[2:])[-2:] + " ...", dbgtab=True)
        self.write("\x03" + chr(trigs))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
        
    def set_trigg(self, trigger):
        if trigger>7:
            #fixme: once fixed in hardware, should rely on self.TWIST_TRIGGER_NUM
            raise Exception("Max trigger value is 7 till now")
        self.dbg_print("Activating trigger " + str(trigger) + " ...", dbgtab=True)
        self.write("\x03" + chr(2**trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
    
    def chain_trigg(self, delayed_trigger, microseconds, origin_trigger=0):
        if delayed_trigger>7:
            #fixme: once fixed in hardware, should rely on self.TWIST_TRIGGER_NUM
            raise Exception("Max trigger value is 7 till now")
        if microseconds>0:
            if origin_trigger==None:
                raise Exception("Origin trigger reference required")
            if origin_trigger >7:
                raise Exception("Max trigger value is 7 till now")
            self.dbg_print("Trigger " + str(delayed_trigger) + " shoots " + str(microseconds) + "us after trigg " + str(origin_trigger) + " ...", dbgtab=True)
        else:
            self.dbg_print("Trigger " + str(delayed_trigger) +  " has no dependency...", dbgtab=True)
        self.write(("\x00\x07") + chr((microseconds//16777216)%256) + chr((microseconds//65535)%256)+ chr((microseconds//256)%256) + chr(microseconds%256) + chr(origin_trigger) + chr(delayed_trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_trigg_dir(self, trigger, direction, sync=True):
        if trigger>=self.TWIST_EXT_TRIGGERS:
            raise Exception("Nonexistent Trigger, or not connectable to ext device")
        if direction not in [EXT_TRIGG_INPUT, EXT_TRIGG_OUTPUT]:
            raise Exception("Unknown direction")
        if direction == EXT_TRIGG_INPUT:
            self.dbg_print("Trigger " + str(trigger) + " obeys its source I/O as input ...", dbgtab=True)
        if direction == EXT_TRIGG_OUTPUT:
            self.dbg_print("Trigger " + str(trigger) + " is output on its I/O ...", dbgtab=True)
        self.ext_trigg_dir[trigger] = direction
        if sync:
            bitmap = 0
            for i in range(self.TWIST_EXT_TRIGGERS-1, -1, -1):
                bitmap= 2*bitmap + self.ext_trigg_dir[i]
            self.write("\x00\x0C" + chr(bitmap//256) + chr(bitmap%256))
            if self.get_ACK():
                raise Exception(self.last_err)
            self.dbg_print("OK")
        else:
            self.dbg_print("delayed")

    def set_sine_ampl(self, sine, ampl):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        if self.BOARD_VER_ID in [0x0001, 0x0100, 0x0101]:
            #BACKPORT:to remove in sept 2020
            dig_ampl = self.amplitude_transcode(ampl, signed=False)
        else:
            dig_ampl = self.amplitude_transcode(ampl)            
        self.dbg_print("Setting amplitude " + str(ampl) + self.current_unit_string + " for sine " + str(sine) + " ...", dbgtab=True)
        self.write("\x00" + chr(0xA0 + sine))
        self.check_no_resp_yet()
        self.write(chr(dig_ampl//256) + chr(dig_ampl%256))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_sine_freq(self, sine, freq):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        self.dbg_print("Setting freq " + str(freq) + "Hz for sine " + str(sine) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x80 + sine))
        self.check_no_resp_yet()
        arg = int(freq*1000)
        self.write(chr(arg//(2**24)))
        arg = arg%(2**24)
        self.write(chr(arg//(2**16)))
        arg = arg%(2**16)
        self.check_no_resp_yet()
        self.write(chr(arg//(2**8)))
        self.write(chr(arg%(2**8)))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def connect_sine_to_channel(self, sine, channel):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Connecting sine " + str(sine) + " to channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x80" + chr(0x40 + sine))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def disconnect_sine_from_channel(self, sine, channel):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Disconnecting sine " + str(sine) + " from channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x00" + chr(0x40 + sine))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
        
    def set_sine_options(self, sine, trigger=None, enable=None, end_of_period=None ):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        if trigger!=None:
            if trigger>=self.TWIST_TRIGGER_NUM:
                raise Exception("Not so much triggers on the system")
            l_trigger = trigger
        else:
            l_trigger = self.TWIST_TRIGGER_NUM
        if enable==None or enable ==False:
            l_enable = 0
        else:
            l_enable = 1
        if end_of_period==None or end_of_period==True:
            l_end_of_period = 1
        else:
            l_end_of_period= 0
        disp=(l_enable, l_end_of_period, l_trigger)
        self.dbg_print("Set options " + str(disp) + " for sine  " + str(sine) + " ...", dbgtab=True)
        self.write("\x01" + chr(sine))
        self.check_no_resp_yet()
        self.write(chr(l_enable*128 + l_end_of_period*64) + chr(l_trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
    
    def set_sine_periods(self, sine, periods):
        if sine>=self.TWIST_SINES:
            raise Exception("Not so much sine generators on the system")
        if periods>=2**32:
            raise Exception("Too much periods ...")
        self.dbg_print("Setting " + str(periods) + " for triggered sine " + str(sine) + " ...", dbgtab=True)
        self.write("\x00" + chr(0xC0 + sine))
        self.check_no_resp_yet()
        arg = periods
        self.write(chr(arg//(2**24)))
        arg = arg%(2**24)
        self.write(chr(arg//(2**16)))
        arg = arg%(2**16)
        self.check_no_resp_yet()
        self.write(chr(arg//(2**8)))
        self.write(chr(arg%(2**8)))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def chan_readback(self, channel, trigger, freq, samples):
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much channels")
        if samples>=2**16-1:
            raise Exception("Too much samples ...")
        loc_trigger = trigger
        if trigger == None:
            loc_trigger = self.TWIST_TRIGGER_NUM
        elif trigger >= self.TWIST_TRIGGER_NUM:
            raise Exception("Not so much triggers ...")
        arg = int((freq+49)/100)
        self.dbg_print("Sampling channel " + str(channel) + " response to trigger " + str(trigger) + ", " + str(samples) + " samples @ " + str(arg*100) + "Hz ...", dbgtab=True)        
        self.write("\x00\x06")
        self.check_no_resp_yet()
        self.write(chr(arg//(2**8)))
        self.write(chr(arg%(2**8)))
        self.check_no_resp_yet()
        self.write(chr(samples//(2**8)))
        self.write(chr(samples%(2**8)))
        self.check_no_resp_yet()
        self.write(chr(channel))
        self.write(chr(loc_trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")
        self.dbg_print("   -> Retreiving data ...", dbgtab=True)
        l = []
        for i in range(samples):
            R = self.serial.read(2)
            s = R[0]*256 + R[1]
            if s>=2**15:
                s-=2**16
            l.append(s*self.max_out_milliamps*self.current_unit/32767)
        self.dbg_print("OK")
        return l


    def set_fifo_freq(self, fifo, freq):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much fifo generators on the system")
        arg = int(freq)
        self.dbg_print("Setting freq " + str(arg) + "Hz for fifo output " + str(fifo) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x20 + fifo))
        self.check_no_resp_yet()
        self.write(chr(arg//(2**24)))
        arg = arg%(2**24)
        self.write(chr(arg//(2**16)))
        arg = arg%(2**16)
        self.check_no_resp_yet()
        self.write(chr(arg//(2**8)))
        self.write(chr(arg%(2**8)))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def set_fifo_options(self, fifo, trigger=None, enable=None, interpolate=None, disable_when_empty=None ):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much fifo generators on the system")
        if trigger!=None:
            if trigger>=self.TWIST_TRIGGER_NUM:
                raise Exception("Not so much triggers on the system")
            l_trigger = trigger
        else:
            l_trigger = self.TWIST_TRIGGER_NUM
        if enable==None or enable ==False:
            l_enable = 0
        else:
            l_enable = 1
        if interpolate==None or interpolate==True:
            l_interpolate = 1
        else:
            l_interpolate = 0
        if disable_when_empty==None or disable_when_empty==True:
            l_disable_when_empty = 1
        else:
            l_disable_when_empty = 0
        disp=(l_enable, l_interpolate, l_disable_when_empty, l_trigger)
        self.dbg_print("Set options " + str(disp) + " for fifo  " + str(fifo) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x30 + fifo))
        self.check_no_resp_yet()
        self.write(chr(l_enable*128 + l_interpolate*64 + l_disable_when_empty*32) + chr(l_trigger))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")


    def connect_fifo_to_channel(self, fifo, channel):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much sine generators on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Connecting fifo " + str(fifo) + " to channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x80" + chr(0x20 + fifo))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def disconnect_fifo_from_channel(self, fifo, channel):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much fifo generators on the system")
        if channel>=self.TWIST_STIM_CHANNELS:
            raise Exception("Not so much stim chanels on the system")
        self.dbg_print("Disconnecting fifo " + str(fifo) + " from channel " + str(channel) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x60 + channel))
        self.check_no_resp_yet()
        self.write("\x00" + chr(0x20 + fifo))
        if self.get_ACK():
            raise Exception(self.last_err)
        self.dbg_print("OK")

    def send_fifo_data(self, fifo, l):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much fifos on the system")
        n = len(l)
        self.dbg_print("Sending " + str(n) + " samples for fifo " + str(fifo) + " ...", dbgtab=True)
        index = 0
        while n>0:
            if n>1024 or n%8==0:
                self.set_size_mlt(3)
                packet_size = min(1024, n)
                header_size = packet_size//8 - 1
            elif n>512 or n%4==0:
                self.set_size_mlt(2)
                packet_size = min(512, n)
                header_size = packet_size//4 - 1
            elif n>256 or n%2==0:
                self.set_size_mlt(1)
                packet_size = min(256, n)
                header_size = packet_size//2 - 1
            else:
                self.set_size_mlt(0)
                packet_size = min(128, n)
                header_size = packet_size-1
            self.write(chr(0x80 + header_size) + chr(0x20 + fifo))
            for i in range(packet_size):
                if i==packet_size - 1:
                    self.check_no_resp_yet()
                c = self.amplitude_transcode(l[index])
                if c<0:
                    c+=65536                
                self.write(chr(c//256) + chr(c%256))
                index += 1
            if self.get_ACK():
                raise Exception(self.last_err)
            if self.BOARD_VER_ID not in [0x0001, 0x0100, 0x0101]:
                #BACKPORT:should always be the case in sept 2020
                l = list(self.get_data())
                if len(l)!=1:
                    raise Exception("too much data received")
                samples_left = l[0]
            n-=packet_size
        self.dbg_print("OK")
        if self.BOARD_VER_ID not in [0x0001, 0x0100, 0x0101]:
            #BACKPORT:should always be the case in sept 2020
            return samples_left

    def get_fifo_elements(self, fifo):
        if fifo>=self.TWIST_FIFOS:
            raise Exception("Not so much fifos on the system")
        self.dbg_print("Retreiving number of elements in fifo " + str(fifo) + " ...", dbgtab=True)
        self.write("\x00" + chr(0x40 + fifo))
        if self.get_ACK():
            raise Exception(self.last_err)
        if self.pending!=1:
            raise Exception("should get 1 word...")
        self.dbg_print("OK")
        R=self.serial.read(2)
        s = R[0]*256 + R[1]
        self.dbg_print("Fifo contains " + str(s) + " elements ...")
        return s
    
    def set_size_mlt(self, sizepw2):
        if sizepw2==self.sizemlt:
            self.dbg_print("No need to change Size multiplier")
            return
        if sizepw2>self.TWIST_DATAPACKET_MAXQUANTUM:
            raise Exception("Size multiplier too high")
        #self.dbg_print("Changing Size multiplier to " + str(sizepw2) + "...", dbgtab=True)
        self.write("\x01" + chr(0x80 + sizepw2))
        if self.get_ACK():
            raise Exception(self.last_err)
        #self.dbg_print("OK")
        self.sizemlt = sizepw2
    def flash_spi_send(self, data):
        self.flash_spi_exc(data, readback=False)
    def flash_spi_exc(self, data, readback=True):
        if len(data)==0:
            raise Exception("No data to send")
        if len(data)%2 == 1:
            if len(data)>128:
                raise Exception("Invalid data length : too long for odd length")
            if self.sizemlt > 0:
                self.set_size_mlt(0)
            if readback:
                command = chr(127+len(data)) + chr(2)
            else:
                command = chr(127+len(data)) + chr(3)
            for c in data:
                command += chr(0) + c
            self.write(command)
            self.get_ACK()
            if readback:
                response = ""
                for _ in data:
                    r = self.serial.read(2)
                    response += chr(r[1])
                return response
            return
        if len(data)%4 == 2:
            if len(data)>256:
                raise Exception("Invalid data length : too long or not multiple of 4")
            if self.sizemlt > 0:
                self.set_size_mlt(0)
            if readback:
                command = chr(127+len(data)//2) + chr(0) + data
            else:
                command = chr(127+len(data)//2) + chr(1) + data
            self.write(command)
            self.get_ACK()
            if readback:
                r = self.serial.read(len(data))
                return r.decode('latin_1')
            return
        if len(data)>512:
            raise Exception("Invalid data_length : too long, must be lower than 512")
        if self.sizemlt != 1:
            self.set_size_mlt(1)
        if readback:
            command = chr(127+len(data)//4) + chr(0) + data
        else:
            command = chr(127+len(data)//4) + chr(1) + data
        self.write(command)
        self.get_ACK()
        if readback:
            r = self.serial.read(len(data))
            return r.decode('latin_1')
        return
    def wait_until_end_of_flash_op(self):
        while True:
            r = self.flash_spi_exc("\x05\x00") # read status register
            if ord(r[1])%2 == 0:
                # write in progress = 0
                return
    def flash_update_iter(self, filename, simulate=True):
        Flash_ID = self.flash_spi_exc("\x9F" + "\x00"*3)
        Flash_ID = self.flash_spi_exc("\x9F" + "\x00"*3)
        if Flash_ID == "\xff\x01\x20\x18":
            self.dbg_print("Xilinx board identified")
        elif Flash_ID == "\xff\x20\xba\x18":
            self.dbg_print("TWIST Board identified")
        else:
            raise Exception("Could not recognize board")
        filesize    = path.getsize(filename)
        remsize     = filesize
        dest_sector = 0
        dest_page   = 0
        yield 0.0
        with open(filename, 'rb') as bitfile:
            while remsize>0:
                if dest_page==0:
                    # the beginning of the sector, we must clear it
                    if simulate:
                        self.flash_spi_send("\x04") # write disable
                        self.flash_spi_send("\x04" + chr(dest_sector) + "\x00\x00") # clear write enable (will have no effect)
                    else:
                        self.flash_spi_send("\x06") # write enable
                        self.flash_spi_send("\xD8" + chr(dest_sector) + "\x00\x00") # sector erase
                    self.wait_until_end_of_flash_op()
                dat = bitfile.read(256).decode("latin_1")
                if len(dat)%4 !=0:
                    dat += "\x00"*(4-len(dat)%4)
                if simulate:
                    self.flash_spi_send("\x04") # write disable
                    self.flash_spi_send("\x03" + chr(dest_sector) + chr(dest_page) + "\x00" + dat) # read instead of write
                else:
                    self.flash_spi_send("\x06") # write enable
                    self.flash_spi_send("\x02" + chr(dest_sector) + chr(dest_page) + "\x00" + dat) # actually writing data
                dest_page += 1
                if dest_page==256:
                    dest_page = 0
                    dest_sector += 1
                remsize -= 256
                yield min(filesize, filesize-remsize)/filesize
                self.wait_until_end_of_flash_op()

    def flash_update(self, filename, simulate=True):
        for _ in self.flash_update_iter(filename, simulate):
            pass
    def check_flash_content_iter(self, ref_filename):
        self.flash_spi_exc("\x9F" + "\x00"*3)
        filesize    = path.getsize(ref_filename)
        chunks = filesize//256 + 1
        with open(ref_filename, 'rb') as bitfile:
            for chunk_num in range(chunks):
                file_dat  = bitfile.read(256)
                flash_dat = self.flash_spi_exc('\x03' + chr(chunk_num//256) + chr(chunk_num%256) + "\x00"*(len(file_dat)+1))
                flash_dat = flash_dat[4:].encode('latin_1')
#                if flash_dat != file_dat:
#                    print("chunk #" + str(chunk_num))
#                    print("addr / file  / flash")
#                    for i in range(len(file_dat)):
#                        print(" %2X      %2X     %2X"%(i, file_dat[i], ord(flash_dat[i])))
                yield (chunk_num/chunks, flash_dat == file_dat)
#                print("\r {0:3.1f}%".format(100*(chunk_num+1)/chunks), end="")
    def check_flash_content(self, ref_filename):
        for _ in self.check_flash_content_iter(self, ref_filename):
            pass

    def setup_sine(self, num, chan, freq, ampl, periods=None, trigger=None, enable=None, end_of_period=None):
        self.set_sine_freq(num,freq)
        self.set_sine_ampl(num, ampl)
        self.set_sine_options(num, trigger, enable, end_of_period)
        if periods!=None:
            self.set_sine_periods(num, periods)
        if isinstance(chan, list):
            for i in chan:
                self.connect_sine_to_channel(num, i)
        else:
            self.connect_sine_to_channel(num, chan)

    def setup_pattern(self, num, chan, trigger, sfreq, data, baseline = 0.0):
        self.set_pattern_freq(num, sfreq)
        self.set_pattern_trigger(num, trigger)
        self.set_pattern_length(num, len(data))
        d_to_send = list(data[:])
        d_to_send.append(baseline)
        self.send_pattern_data(num,d_to_send)
        if isinstance(chan, list):
            for i in chan:
                self.connect_pattern_to_channel(num, i)
        else:
            self.connect_pattern_to_channel(num, chan)
    
    def setup_FIFO(self, num, chan, sfreq, trigger=None, enable=None, interpolate=None, disable_when_empty=None):
        self.set_fifo_freq(num, sfreq)
        self.set_fifo_options(num, trigger, enable, interpolate, disable_when_empty)
        if isinstance(chan, list):
            for i in chan:
                self.connect_fifo_to_channel(num, i)
        else:
            self.connect_fifo_to_channel(num, chan)

    


if __name__ == '__main__':
    raise Exception("This file is not intended to be executed")

# Known issues:
#   no error reported when asking for a frequency above the max capacity (should be fixed now)
#   no error reported while asking for too high number of samples for readback
#   too much samples received for sampling freqs higher than 50MHz
#   data loss of fifo feeding not reported
#   number of samples for FIFOs and waveforms should be powers of 2 
