----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity UART_RECV_generic is
    Generic (CLK_FREQU : integer := 100000000;
             BAUDRATE  : integer :=  33333333;
             TIME_PREC : integer :=    100000;
             DATA_SIZE : integer := 8);
    Port ( clk   : in STD_LOGIC;
           reset : in STD_LOGIC;
           RX    : in STD_LOGIC;
           dout  : out STD_LOGIC_VECTOR (DATA_SIZE - 1 downto 0);
           den   : out STD_LOGIC);
end UART_RECV_generic;

architecture Behavioral of UART_RECV_generic is

    constant clkfrequ_int : integer := CLK_FREQU/TIME_PREC;
    constant baudrate_int : integer := BAUDRATE /TIME_PREC;
    
    signal div_cnt    : integer range -clkfrequ_int/2 to (clkfrequ_int + baudrate_int);
    signal bitcounter : integer range 0 to DATA_SIZE + 1;
    signal shift_reg  : std_logic_vector(DATA_SIZE - 1 downto 0);
    signal RX_sampled : std_logic;
    signal new_bit    : std_logic;
    

begin

    process(clk)
    begin
        if rising_edge(clk) then    
            RX_sampled <= RX;
            if reset = '1' then
                bitcounter <= 0;
                div_cnt    <= 0;
                new_bit    <= '0';
            elsif bitcounter=0 then
                if RX_sampled = '0' then
                    bitcounter <= DATA_SIZE + 1;
                    div_cnt    <= 3*baudrate_int + baudrate_int/2  - clkfrequ_int/2;
                end if;
                new_bit    <= '0';
            elsif div_cnt >= clkfrequ_int then
                div_cnt    <= div_cnt - clkfrequ_int + baudrate_int;
                bitcounter <= bitcounter - 1;
                new_bit    <= '1';
            else
                new_bit    <= '0';
                div_cnt    <= div_cnt + baudrate_int;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                shift_reg  <= (DATA_SIZE-1 downto 0 => '0');
            elsif new_bit = '1' then
                shift_reg  <= RX_sampled & shift_reg(DATA_SIZE - 1 downto 1);
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                dout  <= (DATA_SIZE-1 downto 0 => '0');
                den   <= '0';
            elsif new_bit = '1' and bitcounter = 0 then
                dout  <= shift_reg;
                den   <= RX_sampled;
            else
                den   <= '0';
            end if;
        end if;
    end process;


end Behavioral;
