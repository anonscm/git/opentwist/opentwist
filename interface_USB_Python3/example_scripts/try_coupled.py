#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 28 12:08:20 2019

@author: bornat
"""




# this is only to access the module in the ../ dir
import sys
sys.path.append("../")


import TWIST_control as TC

TWIST = TC.TWIST_instance(debug=True, no_ver_warn=True)
TWIST.setup_sine(0, 0, 50000, 1.0, trigger=0, periods=4)
TWIST.set_chan_param(0, freq=5, PWMbits=0, combined=False)
TWIST.chain_trigg(0, 100000, 0)
TWIST.set_trigg_dir(0, TC.EXT_TRIGG_OUTPUT)
TWIST.set_trigg(0)

TWIST.close()
print("Job's done")
