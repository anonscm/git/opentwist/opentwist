----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_pattern_gen is
    port(
        clk              : in  std_logic;
        reset            : in  std_logic;
        params           : in  Single_Pattern_params_t;
        channel          : out sample_type);
end twist_pattern_gen;

architecture Behavioral of twist_pattern_gen is

    constant FREQ_PRECISION : integer := 500;

    type pattern_mem_t is array (0 to TWIST_PATTERN_MAXLENGTH) of sample_type;
    signal pattern_mem : pattern_mem_t;

    signal playing     : boolean;
    signal index       : integer range 0 to TWIST_PATTERN_MAXLENGTH; 
    signal time_cnt    : integer range 0 to 2**16 + TWIST_BASE_CLK_FREQ/100;
    
    signal loc_params  : Single_Pattern_params_t;

begin

--    process(clk)
--    begin
--        if rising_edge(clk) then
--            loc_params <= params;
--        end if;
--    end process;
    loc_params <= params;

    process(clk)
    begin
        if rising_edge(clk) then
            if loc_params.write = '1' then
                pattern_mem(loc_params.addr) <= loc_params.data;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            channel <= pattern_mem(index);
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                playing <= False;
            elsif loc_params.trigg = '1' then
                playing <= True;
            elsif index = loc_params.length then --and time_cnt >= TWIST_BASE_CLK_FREQ/100 then
                playing <= False;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                index <= 0;
            elsif playing then
                if time_cnt >= TWIST_BASE_CLK_FREQ/FREQ_PRECISION then
                    index <= index + 1;
                end if;
            elsif loc_params.trigg = '1' then
                    index <= 0;
            else
                index <= loc_params.length;
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                time_cnt <= 0;
            elsif playing then
                if time_cnt >= TWIST_BASE_CLK_FREQ/FREQ_PRECISION then
                    time_cnt <= time_cnt - TWIST_BASE_CLK_FREQ/FREQ_PRECISION + to_integer(loc_params.freq);
                else
                    time_cnt <= time_cnt + to_integer(loc_params.freq);
                end if;
            elsif loc_params.trigg = '1' then
                    time_cnt <= to_integer(loc_params.freq);
            else
                time_cnt <= 0;
            end if;
        end if;
    end process;



end Behavioral;
