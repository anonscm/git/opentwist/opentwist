-- twist_FIFO_gen
-- 
-- This module is part of the TWIST control architecture. It provides a FIFOed DAC output.
-- Apart the system clock and reset, its only input is a record that contains all necessary parameters. Its outputs
-- are some status values and the value of the sample to be sent the to stimulation channel.
--
-- it also uses some constants defnied in the project package (Twist_arch_types in twist_arch_defs.vhd)
--        TWIST_BASE_CLK_FREQ   (integer) the system clock frequency
--        TWIST_FIFOGEN_MAXFREQ (integer) the max frequency supported for a FIFO output (divided by 2**16)
--        TWIST_FIFOGEN_DEPTH   (integer) the size of FIFOs to be used in these generators
--        TWIST_SAMPLE_SIZE     (integer) then number of bits in each sample
--        
--
-- the Single_FIFO_params_t type contains the following fields:
--        freq      (integer)          the sampling frequency
--        enabled   (boolean)          shows if the module is actually used
--        interplt  (boolean)          NOT implemented YET : tell if the module should output a constant sample
--                                     value or if it should output a ramp from last sample to current sample.
--        dofe      (boolean)          not used locally. When True, the module will self-disable if embedded FIFO
--                                     becomes empty

--        chan_mask (std_logic_vector) not used locally. each bit shows whether the module output is
--                                     used for its the corresponding channel
--        trigg_num (integer)          not used locally. the trigger the module is responding to. This value is
--                                     stored here for conveniency but is used in another part of the architecture
        
--        sample    (sample_type)      new sample to store in FIFO
--        sample_en (std_logic)        sample enablefor FIFO storage
--
--
--
-- The single_FIFO_status_t contains the following fields
--        full    (std_logic) FIFO full, used to report eventual data loss when filling the FIFO
--        empty   (std_logic) FIFO empty, used to report realtime issues and to manage the 'enabled' in conjonction
--                            with the 'dofe' bit
--        content (integer)   number of elements in fifo, reported to user when asked for




library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;


entity twist_FIFO_gen is
    port(
    clk              : in  std_logic;
    reset            : in  std_logic;
    params           : in  Single_FIFO_params_t;
    status           : out single_FIFO_Status_t;
    channel          : out sample_type);
end twist_FIFO_gen;


architecture Behavioral of twist_FIFO_gen is

    signal timer_cnt     : integer range 0 to TWIST_BASE_CLK_FREQ + (TWIST_FIFOGEN_MAXFREQ+1)*(2**16); -- the timer counter
    signal sample_strobe : std_logic;                                                                  -- '1' when we read a new sample

    -- local copy of values to output
    signal full_n      : std_logic;
    signal empty_n     : std_logic;
    
    signal fifo_output : std_logic_vector(TWIST_SAMPLE_SIZE-1 downto 0); -- The FIFO output which is not necessarily the module output


begin


    status.full    <= not full_n;
    status.empty   <= not empty_n;

    -- To manage sample output, th module does not rely on a frequency divider, but on
    -- a counter which is incremented by the output frequency value. once the counter exceeds 
    -- the clock frequency, it is time to ouput a new sample. At that time, the counter
    -- is not reset, but decreased by the clock frequency value. It is then possible to easily
    -- provide frequencies that are not submultiples of the clock frequency.
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' or not params.enabled  then 
                timer_cnt     <= 0;
                sample_strobe <= '0';
            elsif timer_cnt >= TWIST_BASE_CLK_FREQ then
                timer_cnt     <= timer_cnt - TWIST_BASE_CLK_FREQ + params.freq;
                sample_strobe <= '1';
            else
                timer_cnt     <= timer_cnt + params.freq;
                sample_strobe <= '0';
            end if;
        end if;
    end process;

    -- we simply call a generic FIFO module, 
    sample_file : entity work.FIFO_generic
    generic map( DATA_SIZE  => TWIST_SAMPLE_SIZE,
                 FIFO_DEPTH => TWIST_FIFOGEN_DEPTH)
    port map(clk      => clk,
             reset    => reset,
             data_in  => std_logic_vector(params.sample),
             dwrite   => params.sample_en,
             full_n   => full_n,
             elemts   => status.content,
             data_out => fifo_output,
             dread    => sample_strobe,
             empty_n  => empty_n);

    -- This process manages the module output : the FIFO output when it is working, otherwise '0'
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                channel <= (others => '0');
            elsif params.enabled and empty_n = '1' then
                channel <= signed(fifo_output);
            else
                channel <= (others => '0');
            end if;
        end if;
    end process;





    


end Behavioral;
