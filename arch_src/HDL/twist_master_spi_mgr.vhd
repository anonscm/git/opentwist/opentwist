----------------------------------------------------------------------------------
-- the spi module controlled by twist_IF that manages the link with Flash memory
-- the flash memory contains (at least) the FPGA bitstream
-- 
-- this version is kind of dumb, and does not support data queuing,so it is necessary
-- to wait for the feedback to send another command.
--
-- by default, the module manages 16bits data packets. This is not convenient for
-- WREN et WRDIS commands that require only 8 bits to be sent. the params.single_byte
-- bit then indicate that we only use the upper 8 bits.
--
-- while params.instr_conted is '1', the module expects more data and leave SPI_CSn
-- low. if '0', SPI_CSn will be '1' as soon as the transfer is over. It will be tied
-- low for the next transfer.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Twist_arch_types.all;

entity master_spi_mgr is
    Port ( clk      : in  STD_LOGIC;
           rst      : in  STD_LOGIC;
           params   : in flash_mgr_io_t;
           fbck     : out flash_fbck_t;
           spi_CSn  : out std_logic;
           spi_SCK  : out std_logic;
           spi_MOSI : out std_logic;
           spi_MISO : in  std_logic);

end master_spi_mgr;

architecture Behavioral of master_spi_mgr is

    constant max_halfper : integer := (TWIST_BASE_CLK_FREQ-1) / (2*TWIST_FLASHSPI_CLK_FREQ);
    signal half_per_clk : integer range 0 to max_halfper;   -- the counter for each half period
    signal bits_to_send : integer range 0 to 16;            -- the number of bits in the shift register that are yet to send
    
    signal synced_MISO   : std_logic;
    signal spi_falled    : boolean;                         -- true the clock period after a falling edge of  spi_SCK
    signal shift_reg_in  : std_logic_vector(15 downto 0);
    signal shift_reg_out : std_logic_vector(15 downto 0);
    --signal data_recv_dly : integer range 0 to 2;

    signal local_SCK : std_logic;
    signal local_CSn : std_logic;

begin

    spi_SCK  <= local_SCK;
    spi_CSn  <= local_CSn;
    spi_MOSI <= shift_reg_out(15);
    --miso data should be read while clk edge is falling
    --mosi data should be updated on falling edge too 

    -- Base clock management
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                half_per_clk <= 0;
            elsif half_per_clk = 0 and bits_to_send > 0 then
                half_per_clk <= max_halfper;
            else
                half_per_clk <= half_per_clk - 1;
            end if;
        end if;
    end process;

    -- bit counting and output shift register
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bits_to_send  <= 0;
                shift_reg_out <= x"0000";
            elsif local_sck = '1' and half_per_clk = 0 then -- we are about to get a falling edge, bits_to_send is >0 by design
                bits_to_send  <= bits_to_send - 1;
                shift_reg_out <= shift_reg_out(14 downto 0) & '0';
            elsif bits_to_send = 0 and params.data_flash_en = '1' then
                if params.single_byte = '0' then
                    bits_to_send  <= 16;
                else
                    bits_to_send  <= 8;
                end if;
                shift_reg_out <= params.data_to_flash;
            end if;
        end if;
    end process;

    -- construction of SPI_SCK and SPI_CSn
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                local_SCK <= '1';
                local_CSn <= '1';
            elsif half_per_clk = 0 then
                if bits_to_send > 0 then
                    if local_CSn = '1' then
                        local_CSn <= '0';
                        local_SCK <= '0';
                    else
                        local_CSn <= '0';
                        local_SCK <= not local_SCK;
                    end if;
                elsif params.instr_conted = '1' then
                    local_CSn <= '0';
                    local_SCK <= '0';
                else
                    local_CSn <= '1';
                    local_SCK <= '0';
                end if;
            end if;
        end if;
    end process;

    -- input shift register (and input data synchronization)
    process(clk)
    begin
        if rising_edge(clk) then
            synced_MISO <= spi_MISO;    -- this is to get a synchronized version of MISO
            spi_falled  <= local_sck = '1' and half_per_clk = 0 ;
            if spi_falled then          -- we just had a falling edge on spi_SCK
                                        -- this is to retreived the value of MISO just before the falling edge of spi_SCK
                if params.single_byte = '0' then
                    shift_reg_in <= shift_reg_in(14 downto 0) & synced_MISO;
                else
                    shift_reg_in <= x"00" & shift_reg_in(6 downto 0) & synced_MISO;
                end if;                
            end if;
        end if;
    end process;
    
    -- this is to send the data to calling module
    fbck.data_from_flash <= shift_reg_in;
    process(clk)
    begin
        if rising_edge(clk) then
            if spi_falled and bits_to_send = 0 then
                fbck.data_flash_en   <= '1';
                fbck.last_data       <= not params.instr_conted;
            else
                fbck.data_flash_en   <= '0';
                fbck.last_data       <= '0';
            end if;
        end if;
    end process;


end Behavioral;
