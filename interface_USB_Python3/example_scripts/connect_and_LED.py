#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script only tests basic communication with the TWIST system
"""

# this is only to access the module in the ../ dir
import sys
sys.path.append("../")


import TWIST_control as TC
import time

TWIST = TC.TWIST_instance(debug=True)

for _ in range(4):
    for i in range(7, 0, -1):
        TWIST.set_dbgLED(2**i)
        time.sleep(0.05)
    for i in range(7):
        TWIST.set_dbgLED(2**i)
        time.sleep(0.05)
TWIST.set_dbgLED(0)
    
    