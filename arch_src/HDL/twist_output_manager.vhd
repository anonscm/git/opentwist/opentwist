----------------------------------------------------------------------------------
-- twist_output_manager
--
-- this module gathers the outputs on all channels. It also handles how
-- different channels can be combined
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.Twist_arch_types.all;

entity twist_output_manager is
    port(
        clk              : in  std_logic;
        rst              : in  std_logic;
        params           : in  chans_params_t;
        usec_cnt         : in  integer range 0 to TWIST_BASE_CLK_FREQ_MHZ-1;
        channels         : in  Channel_Values_t;
        output_ctrl      : out Total_HW_Channs_t);

end twist_output_manager;

architecture Behavioral of twist_output_manager is

    signal comb_params   : chans_params_t;      -- parameter values used for odd numbered channels
    signal comb_channels : Channel_Values_t;   -- the value to output considering
                                               -- channel combination
    -- if channels are combined, we need to synchronise their local pwm counters.
    -- to do so, we reset them when the combination is aked for.
    signal old_combs     : std_logic_vector(TWIST_STIM_CHANNELS/2 - 1 downto 0);
    signal resets        : std_logic_vector(TWIST_STIM_CHANNELS/2 - 1 downto 0);

begin



individual_channels : for half_channum in 0 to TWIST_STIM_CHANNELS/2 - 1 generate


    -- with this process, we dispatch channel parameters and values...
    process(clk)
    begin
        if rising_edge(clk) then
            -- we always copy channel value and params, whether it be odd or even,
            -- this is to keep the same processing latency and value coherence.
            comb_channels(2*half_channum)   <= channels(2*half_channum);
            comb_params(2*half_channum)     <= params(2*half_channum);
            if params(2*half_channum).combined then
                -- even channel is marked 'combined', so we drop value and parameters
                -- for odd numbered channel and provide those of the even numbered
                -- channel to both
                comb_channels(2*half_channum+1) <= channels(2*half_channum);
                comb_params(2*half_channum+1)   <= params(2*half_channum);
            else
                comb_channels(2*half_channum+1) <= channels(2*half_channum+1);
                comb_params(2*half_channum+1)   <= params(2*half_channum+1);
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                -- with this, we have a one clock period delay between system reset
                -- and channel output delay. should not be too much trouble
                old_combs(half_channum) <= '0';
                resets(half_channum)    <= '1';
            elsif params(2*half_channum).combined then
                -- chan combination is set, so we mark it as set, if it wasn't
                -- alsready, it means this is the first time, and we reset the
                -- corresponding modules
                old_combs(half_channum) <= '1';
                resets(half_channum)    <= not old_combs(half_channum);
            else
                old_combs(half_channum) <= '0';
                resets(half_channum)    <= '0';
            end if;
        end if;
    end process;

    
    even_single_chan : entity work.twist_single_output_manager
        generic map(COMBINED_POSITIVE => True)
        port map(
            clk         => clk,
            rst         => resets(half_channum),
            params      => comb_params(2*half_channum),
            usec_cnt    => usec_cnt,
            channel     => comb_channels(2*half_channum),
            output_ctrl => output_ctrl(2*half_channum));

    odd_single_chan : entity work.twist_single_output_manager
        generic map(COMBINED_POSITIVE => False)
        port map(
            clk         => clk,
            rst         => resets(half_channum),
            params      => comb_params(2*half_channum+1),
            usec_cnt    => usec_cnt,
            channel     => comb_channels(2*half_channum+1),
            output_ctrl => output_ctrl(2*half_channum+1));




    end generate;

end Behavioral;
